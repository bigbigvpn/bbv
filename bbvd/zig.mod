id: q201ijmfb93zebe6ikdsf0njqiiw8yq2pggqzq761qmed2st
name: bbvd
main: src/main.zig
dev_dependencies:

  - src: git https://github.com/truemedian/wz
    name: wz
    main: src/main.zig
  - src: git https://github.com/Vexu/zuri
    name: zuri
    main: src/zuri.zig
  - src: git https://gitlab.com/luna/zig-mbedtls
    name: tls
    main: src/lib.zig

