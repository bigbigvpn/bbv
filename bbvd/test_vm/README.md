# the bbvd test vm

the collection of scripts here create a firecracker microvm with bbvd
loaded in (via docker building the images) so that you can easily test some
flows without having to mess with your _existing_ networking stack.

## dependencies

- docker, https://docker.com or your package manager's docker
- firecracker, https://github.com/firecracker-microvm/firecracker
  - its a statically linked single binary, no other setup required
- current user should be able to access /dev/kvm, if you want to run it as
  your own user.
  - `sudo setfacl -m u:$USER:rw /dev/kvm` (setfacl may not be in your system by default, void has `acl-progs`)

## setup

- INSERT a test vpn in your instance of bbv that the test vm can connect into
  and that won't be deleted if it doesn't connect (instead raising an error,
  it's okay).

```sql
INSERT INTO bbv_vpns (
    id,
    group_id,
    cloud_provider,
    cloud_region,
    cloud_id,
    created_at,
    ipv4_network,
    ipv6_network,
    hostname,
    summoner_id,
    auth_token,
    state,
    local_ipv4_address,
    local_ipv6_address,
    wireguard_private_key,
    wireguard_public_key
) values (
	'be13f003-77e8-4986-a356-1043d9df3c98',
	'1ab05cd2-035e-4f1e-84d1-f479999ec391',
	'hetzner',
	'nbg1',
	'this_is_a_test_vm',
	now() at time zone 'utc',
	'127.0.0.1',
	null,
	'this_is_a_test_vm_hostname',
	'b3b6cb8e-69c8-4a24-bc19-36628b36763d',
	'6446b22b7b462fa962fe72d9ba38bce6f040d7b290b283bfeb7959fedf158ae2',
	'waiting_confirmation',
	'172.16.0.1',
	'fd42:12:34::1',
	'GOKJ8h98jTzdJGa+R3vOYidr+ZgO4N65zwJpePx5pE4=',
	'/PECtE4gwYRUGKmcigfRScWyl3CC2QjQqYPDK8TkOmc='
);
```

# running

- make sure you have a reachable INTERNET address for the bbv webapp.
  this can be done either via services such as `ngrok` (paid) or `localhost.run` (cute)

```sh
cd bbvd/ # ensure we are in the bbvd folder

# build the daemon and load it with the variables we added to the SQL script
# so that the daemon can actually login

# either ws:// or wss:// urls work, but wss:// is preffered as that would be
# closest to production flows.
zig build && env \
    BBVD_URL=wss://REPLACE_WITH_YOUR_URL.localhost.run/api/dev/control/ws \
    BBVD_VPN_ID=be13f003-77e8-4986-a356-1043d9df3c98 \
    BBVD_VPN_TOKEN=6446b22b7b462fa962fe72d9ba38bce6f040d7b290b283bfeb7959fedf158ae2 \
    BBVD_WIREGUARD_KEY=CB9bSkGHWoWTmVLjezHaIZdntB2jp3n6wr8r6J9EHlY= \
    BBVD_WIREGUARD_PORT=6969 \
    BBVD_WIREGUARD_IP="172.16.0.1, fd42:12:34::1" \
    ./test_vm/test.sh \
    path/to/firecracker/binary
```
