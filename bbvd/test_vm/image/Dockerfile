FROM alpine:latest
RUN apk update
RUN apk add openrc util-linux openssh-server rsyslog wireguard-tools busybox-ifupdown busybox-extras busybox-initscripts strace vim iptables ip6tables

# the root user does not have a password in the default alpine docker image.
# so we force-set one here. ;)
RUN echo 'root:shitfuck!' | chpasswd

# make sure .ssh exists
RUN mkdir -p /root/.ssh

# add good sshd config perms
RUN echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config
RUN echo 'PermitTTY yes' >> /etc/ssh/sshd_config

# set ifaces for vm
COPY interfaces /etc/network/interfaces

# setup services to run
RUN rc-update add sysfs sysinit
RUN rc-update add devfs sysinit
RUN rc-update add dmesg sysinit
RUN rc-update add hwdrivers sysinit
RUN rc-update add mdev sysinit

RUN rc-update add bootmisc boot
RUN rc-update add loadkmap boot
RUN rc-update add modules boot
RUN rc-update add networking boot
RUN rc-update add sysctl boot
RUN rc-update add rsyslog boot
RUN rc-update add urandom boot
RUN rc-update add hostname boot

RUN rc-update add sshd default

# enable ssh ttys to exist (and so, ssh to exist properly)
COPY devpts.initd /etc/init.d/devpts
RUN rc-update add devpts sysinit

# setup tty on serial console
RUN ln -s agetty /etc/init.d/agetty.ttyS0
RUN echo ttyS0 > /etc/securetty
RUN rc-update add agetty.ttyS0 default

# copy bbvd over and start it on vm boot
COPY bbvd /usr/bin/bbvd
COPY bbvd.initd /etc/init.d/bbvd
RUN mkdir -p /var/log/bbvd
RUN rc-update add bbvd default
