#!/bin/sh

set -euxo

IMG_ID=$(docker build -q test_vm/image)
CONTAINER_ID=$(docker run -td "$IMG_ID" /bin/sh)

MOUNTDIR=./mnt
FS=./test_vm/rootfs.ext4

# create empty ext4 image
mkdir -p $MOUNTDIR
qemu-img create -f raw $FS 100M
mkfs.ext4 $FS
sudo mount $FS $MOUNTDIR

# copy entire container filesystem into the newly created image
sudo docker cp "$CONTAINER_ID:/" $MOUNTDIR

# remove .dockerenv so that the image isn't wrongly tagged as docker stuff
# (like openrc does)
sudo rm $MOUNTDIR/.dockerenv

# clean up
sudo umount $MOUNTDIR
sudo docker kill "$CONTAINER_ID"
