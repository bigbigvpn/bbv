#!/bin/sh
#
# start bbvd in a firecracker vm

set -eux

firecracker_path=$1

cat <<EOF > ./test_vm/image/bbvd.initd
#!/sbin/openrc-run

BBVD_URL="$BBVD_URL"
BBVD_VPN_ID="$BBVD_VPN_ID"
BBVD_VPN_TOKEN="$BBVD_VPN_TOKEN"
BBVD_WIREGUARD_KEY="$BBVD_WIREGUARD_KEY"
BBVD_WIREGUARD_PORT="$BBVD_WIREGUARD_PORT"
BBVD_WIREGUARD_IP="$BBVD_WIREGUARD_IP"
BBVD_WIREGUARD_PATH="/etc/wireguard/wg0.conf"

export BBVD_URL
export BBVD_VPN_ID
export BBVD_VPN_TOKEN
export BBVD_WIREGUARD_KEY
export BBVD_WIREGUARD_PORT
export BBVD_WIREGUARD_IP
export BBVD_WIREGUARD_PATH

command="/usr/bin/bbvd"
command_background="yes"
pidfile="/tmp/bbvd.pid"
output_log="/var/log/bbvd/output.log"
error_log="/var/log/bbvd/error.log"

EOF
chmod +x ./test_vm/image/bbvd.initd


# networking configurations
# 
#: the interface running in the host that firecracker binds to. this interface
#  is where we add all our funny rules.
TAP_DEV="fc-88-tap0"
#: network mask for the local network we're running
MASK_LONG="255.255.255.252"
MASK_SHORT="/30"

FC_IP="169.254.0.21"
TAP_IP="169.254.0.22"
# mac of the fake interface
FC_MAC="02:FC:00:00:00:05"

KERNEL_BOOT_ARGS="ro console=ttyS0 noapic reboot=k panic=1 pci=off nomodules random.trust_cpu=on"

cat <<EOF > test_vm/image/interfaces
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
    address ${FC_IP}
    netmask ${MASK_LONG}
    gateway ${TAP_IP}
EOF

# build image
cp ./zig-out/bin/bbvd ./test_vm/image/bbvd
./test_vm/image/build.sh

# start firecracker vm

# set up a tap network interface for the Firecracker VM to userspace
sudo ip link del "$TAP_DEV" 2> /dev/null || true
sudo ip tuntap add dev "$TAP_DEV" mode tap
sudo ip addr add "${TAP_IP}${MASK_SHORT}" dev "${TAP_DEV}"
sudo ip link set $TAP_DEV up

# sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
# sudo nft add table nat
# sudo nft 'add chain nat postrouting { type nat hook postrouting priority 100 ; }'
# sudo nft "add rule nat postrouting ip saddr ${TAP_IP}${MASK_SHORT} oif $TAP_DEV snat to 10.0.0.2/31"
# sudo nft 'add rule nat postrouting masquerade'

# make a configuration file
cat <<EOF > test_vm/vmconfig.json
{
  "boot-source": {
    "kernel_image_path": "/home/luna/Downloads/abc/linux/linux-5.12.6/vmlinux",
    "boot_args": "$KERNEL_BOOT_ARGS"
  },
  "drives": [
    {
      "drive_id": "rootfs",
      "path_on_host": "./test_vm/rootfs.ext4",
      "is_root_device": true,
      "is_read_only": false
    }
  ],
  "network-interfaces": [
      {
          "iface_id": "eth0",
          "guest_mac": "$FC_MAC",
          "host_dev_name": "$TAP_DEV"
      }
  ],
  "machine-config": {
    "vcpu_count": 2,
    "mem_size_mib": 1024,
    "ht_enabled": false
  }
}
EOF
$firecracker_path --no-api --config-file test_vm/vmconfig.json
