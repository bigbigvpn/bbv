const std = @import("std");

const Uri = @import("zuri").Uri;
const main = @import("main.zig");

const messages = @import("messages.zig");
const Message = messages.Message;

const wgfile = @import("wireguard_file.zig");
const WireguardFile = wgfile.WireguardFile;
const WireguardPeers = wgfile.WireguardPeers;

const ws = @import("websocket.zig");
const isWebsocketClient = ws.isWebsocketClient;
const CloseData = ws.CloseData;

const wz = @import("wz");
const zuri = @import("zuri");

const states = @import("state.zig");
const LoginError = states.LoginError;
const ClientState = states.ClientState;

const logger = std.log.scoped(.bbvd_log);

/// A test executor that doesn't run wireguard binaries, for testing.
pub const TestWireguardExecutor = struct {
    reloaded: bool = false,
    connected_peers: ConnectedPeers = .{
        .total_count = 0,
        .offline_count = 0,
    },
    all_client_timeout: bool = false,

    const Self = @This();

    pub fn reload(self: *Self) !void {
        self.reloaded = true;
        return;
    }

    const ConnectedPeers = struct {
        total_count: usize,
        offline_count: usize,
    };

    pub fn endpointCount(self: Self) !ConnectedPeers {
        return self.connected_peers;
    }

    pub fn allClientTimeout(self: Self) !bool {
        return self.all_client_timeout;
    }
};

/// A test saver that doesn't write to the filesystem, for testing.
pub const TestWireguardSaver = struct {
    saved: bool = false,

    const Self = @This();

    pub fn saveToPath(self: *Self, wg_file: *const WireguardFile, path: []const u8) !void {
        _ = wg_file;
        _ = path;
        self.saved = true;
    }
};

const EventList = std.ArrayList(wz.base.client.Event);
const StringList = std.ArrayList(u8);
const MessageList = std.ArrayList(Message);

/// A test websocket client that doesn't do i/o to the outside world, for testing.
pub const TestWebsocketClient = struct {
    allocator: *std.mem.Allocator,

    /// Holds the list of events to be given to an user of this client.
    ///
    /// Add events via pushClose or pushMessage
    event_list: EventList,

    /// Holds the JSON of a given Message added via pushMessage.
    event_string_buffer: StringList,

    /// Holds the Messages given by the user.
    message_output_list: MessageList,

    const Self = @This();

    pub fn init(allocator: *std.mem.Allocator) Self {
        return Self{
            .allocator = allocator,
            .event_list = EventList.init(allocator),
            .event_string_buffer = StringList.init(allocator),
            .message_output_list = MessageList.init(allocator),
        };
    }

    pub fn deinit(self: *const Self) void {
        self.event_list.deinit();
        self.event_string_buffer.deinit();
        self.message_output_list.deinit();
    }

    fn allocString(self: *Self, string: []const u8) ![]u8 {
        // TODO use arena instead of ArrayList(u8) due to reallocations
        const before_add = self.event_string_buffer.items.len;
        try self.event_string_buffer.appendSlice(string);
        const after_add = self.event_string_buffer.items.len;
        return self.event_string_buffer.items[before_add..after_add];
    }

    /// Push a message to the list.
    pub fn pushMessage(self: *Self, message: Message) !void {
        var buf: [0x1000]u8 = undefined;
        var buf_stream = std.io.FixedBufferStream([]u8){ .buffer = &buf, .pos = 0 };

        try std.json.stringify(message, .{}, buf_stream.writer());
        const end_pos = buf_stream.pos;

        const message_in_stack = buf[0..end_pos];
        const message_to_be_sent = try self.allocString(message_in_stack);

        logger.info("send message: {s}", .{message_to_be_sent});

        try self.event_list.append(.{ .header = .{
            .opcode = .Text,
            .length = message_to_be_sent.len,
        } });
        try self.event_list.append(.{ .chunk = .{ .data = message_to_be_sent } });
    }

    /// Push a close event to the list.
    pub fn pushClose(self: *Self, comptime close_code: u16, comptime reason: []const u8) !void {
        var data = CloseData{ .code = close_code, .reason = reason };
        var close_msg_buffer: [@sizeOf(u16) + reason.len]u8 = undefined;
        const len = data.toBuffer(&close_msg_buffer);
        const owned_close_data = try self.allocString(&close_msg_buffer);

        try self.event_list.append(.{ .header = .{
            .opcode = .Close,
            .length = len,
        } });

        try self.event_list.append(.{ .chunk = .{ .data = owned_close_data } });
    }

    // == interface functions ==
    // the user calls those functions, not us, the test

    pub fn next(self: *Self) !?wz.base.client.Event {
        // when testing mainLoop(), it is important for this function
        // to have bounded execution (via returning null), or else the test
        // would never stop
        if (self.event_list.items.len == 0) return null;

        return self.event_list.orderedRemove(0);
    }

    // TODO implement more websocket test methods

    pub fn executeHandshake(self: *Self, uri: zuri.Uri) !void {
        _ = self;
        _ = uri;
    }
    pub fn closeWithCode(self: *Self, code: u16) !void {
        _ = self;
        _ = code;
    }
    pub fn closeWithReason(self: *Self, comptime code: u16, comptime reason: []const u8) !void {
        _ = self;
        _ = code;
        _ = reason;
    }
    pub fn sendMessage(self: *Self, message: Message) !void {
        try self.message_output_list.append(message);
    }
};

comptime {
    std.debug.assert(isWebsocketClient(TestWebsocketClient));
}

const TestState = ClientState(
    TestWireguardExecutor,
    TestWebsocketClient,
    WireguardFile,
    TestWireguardSaver,
);

// Attempt to sync, add, and remove keys from a given state.
test "client state test syncing" {
    const allocator = std.testing.allocator;

    var test_objects = TestObjects.setupTest(allocator);
    defer test_objects.deinit();

    var state = test_objects.getState(allocator);

    var test_keys_list = std.ArrayList(messages.WireguardKey).init(allocator);
    defer test_keys_list.deinit();

    try test_keys_list.append(messages.WireguardKey{
        .user_id = "test",
        .wireguard_public_key = "testkey",
        .local_ipv4_address = "xxx",
        .local_ipv6_address = "xxx",
    });

    try state.handleCommand(Message.Command{
        .t = .sync_wireguard_keys,
        .v = .{ .sync_wireguard_keys = .{
            .keys = test_keys_list.items,
        } },
    });

    try std.testing.expectEqual(@as(u32, 1), test_objects.wg_file.peers.count());

    try test_keys_list.append(messages.WireguardKey{
        .user_id = "test",
        .wireguard_public_key = "testkey2",
        .local_ipv4_address = "xxx",
        .local_ipv6_address = "xxx",
    });

    try state.handleCommand(Message.Command{
        .t = .add_wireguard_keys,
        .v = .{ .sync_wireguard_keys = .{
            .keys = test_keys_list.items,
        } },
    });

    try std.testing.expectEqual(@as(u32, 2), test_objects.wg_file.peers.count());

    try state.handleCommand(Message.Command{
        .t = .remove_wireguard_keys,
        .v = .{ .sync_wireguard_keys = .{
            .keys = test_keys_list.items,
        } },
    });

    try std.testing.expectEqual(@as(u32, 0), test_objects.wg_file.peers.count());
}

const TestObjects = struct {
    wg_file: WireguardFile,
    executor: TestWireguardExecutor,
    client: TestWebsocketClient,
    saver: TestWireguardSaver,

    const Self = @This();

    pub fn setupTest(allocator: *std.mem.Allocator) Self {
        return .{
            .wg_file = WireguardFile.init(allocator, "10.0.0.1", "doctor sex", "6969", "enp1s0"),
            .executor = TestWireguardExecutor{},
            .saver = TestWireguardSaver{},
            .client = TestWebsocketClient.init(allocator),
        };
    }

    pub fn getState(self: *Self, allocator: *std.mem.Allocator) TestState {
        return TestState.init(
            allocator,
            &self.executor,
            &self.wg_file,
            &self.client,
            undefined,
            &self.saver,
        );
    }

    pub fn deinit(self: *Self) void {
        self.wg_file.deinit();
        self.client.deinit();
    }
};

// Test that the offline checking thread does correct state transitions
// given correct input.
test "offline check" {
    const allocator = std.testing.allocator;

    var test_objects = TestObjects.setupTest(allocator);
    defer test_objects.deinit();

    var state = test_objects.getState(allocator);

    var offline_since: ?i64 = null;

    // if we're online, keep offline_since as null

    test_objects.executor.connected_peers.total_count = 1;
    test_objects.executor.connected_peers.offline_count = 0;
    test_objects.executor.all_client_timeout = false;

    try state.offlineCheckThreadTick(&offline_since);
    try std.testing.expectEqual(@as(?i64, null), offline_since);

    // if we just got offline, set offline_since

    test_objects.executor.connected_peers.total_count = 1;
    test_objects.executor.connected_peers.offline_count = 1;
    test_objects.executor.all_client_timeout = true;

    try state.offlineCheckThreadTick(&offline_since);
    try std.testing.expect(offline_since != null);
    try std.testing.expect(offline_since.? > 0);

    // if we set offline since to an absurd amount (while still offline),
    // we must send a Notify message

    const max_val: i64 = std.time.timestamp() - std.math.maxInt(i32);
    offline_since = max_val;

    try state.offlineCheckThreadTick(&offline_since);
    try std.testing.expect(offline_since != null);
    // make sure we haven't mutated offline_since upon sending notify
    try std.testing.expectEqual(max_val, offline_since.?);

    try std.testing.expectEqual(@as(usize, 1), test_objects.client.message_output_list.items.len);
    const notify_message = test_objects.client.message_output_list.pop();
    try std.testing.expectEqual(messages.OpCode.notify, notify_message.op);
    try std.testing.expectEqual(messages.OpCode.notify, notify_message.d);
    try std.testing.expect(notify_message.d.notify.t == .no_clients);

    // if we go back to online, unset offline_since

    test_objects.executor.connected_peers.total_count = 1;
    test_objects.executor.connected_peers.offline_count = 0;
    test_objects.executor.all_client_timeout = false;

    try state.offlineCheckThreadTick(&offline_since);
    try std.testing.expect(offline_since == null);
}

// Test that we can login successfully.
test "login flow" {
    const allocator = std.testing.allocator;

    var test_objects = TestObjects.setupTest(allocator);
    defer test_objects.deinit();

    var state = test_objects.getState(allocator);

    try test_objects.client.pushMessage(Message{
        .op = .welcome,
        .d = .{ .welcome = null },
    });

    var uri = try Uri.parse("ws://localhost", true);
    try state.login(uri, "", "");

    try test_objects.client.pushClose(4000, "test reason");
    try std.testing.expectError(
        LoginError.LoginFailure,
        state.login(uri, "", ""),
    );

    try test_objects.client.pushClose(4001, "test reason");
    try std.testing.expectError(
        LoginError.InvalidCredentials,
        state.login(uri, "", ""),
    );
}

// Test that heartbeats generate output (heartbeat ack message)
// and side effects (thread is spawned).
test "heartbeats work" {
    const allocator = std.testing.allocator;

    var test_objects = TestObjects.setupTest(allocator);
    defer test_objects.deinit();

    var state = test_objects.getState(allocator);

    try test_objects.client.pushMessage(Message{
        .op = .heartbeat,
        .d = .{ .heartbeat = null },
    });

    try std.testing.expect(state.running_offline_check_thread == null);

    try state.mainLoop();

    try std.testing.expectEqual(@as(usize, 1), test_objects.client.message_output_list.items.len);

    // assert heartbeats spawn the offline checking thread
    try std.testing.expect(state.running_offline_check_thread != null);
}

// Attempt to sync, add, and remove keys but going through the mainLoop route,
// asserting that JSON parsing works on the state side.
test "commands work" {
    const allocator = std.testing.allocator;

    var test_objects = TestObjects.setupTest(allocator);
    defer test_objects.deinit();

    var state = test_objects.getState(allocator);

    var test_keys_list = std.ArrayList(messages.WireguardKey).init(allocator);
    defer test_keys_list.deinit();

    try test_keys_list.append(messages.WireguardKey{
        .user_id = "test",
        .wireguard_public_key = "testkey",
        .local_ipv4_address = "xxx",
        .local_ipv6_address = "xxx",
    });

    try test_objects.client.pushMessage(Message{
        .op = .command,
        .d = .{ .command = .{
            .t = .add_wireguard_keys,
            .v = .{ .sync_wireguard_keys = .{ .keys = test_keys_list.items } },
        } },
    });
    try state.mainLoop();

    try std.testing.expectEqual(@as(usize, 0), test_objects.client.message_output_list.items.len);
    try std.testing.expectEqual(@as(u32, 1), test_objects.wg_file.peers.count());

    // add 2nd key, assert both work

    try test_keys_list.append(messages.WireguardKey{
        .user_id = "test",
        .wireguard_public_key = "testkey2",
        .local_ipv4_address = "xxx",
        .local_ipv6_address = "xxx",
    });

    try test_objects.client.pushMessage(Message{
        .op = .command,
        .d = .{ .command = .{
            .t = .add_wireguard_keys,
            .v = .{ .sync_wireguard_keys = .{ .keys = test_keys_list.items } },
        } },
    });

    try state.mainLoop();

    try std.testing.expectEqual(@as(usize, 0), test_objects.client.message_output_list.items.len);
    try std.testing.expectEqual(@as(u32, 2), test_objects.wg_file.peers.count());

    // attempt to remove both, assert it worked

    try test_objects.client.pushMessage(Message{
        .op = .command,
        .d = .{ .command = .{
            .t = .remove_wireguard_keys,
            .v = .{ .sync_wireguard_keys = .{ .keys = test_keys_list.items } },
        } },
    });
    try state.mainLoop();

    try std.testing.expectEqual(@as(usize, 0), test_objects.client.message_output_list.items.len);
    try std.testing.expectEqual(@as(u32, 0), test_objects.wg_file.peers.count());
}
