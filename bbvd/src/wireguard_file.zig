const std = @import("std");

/// Represents a wireguard peer.
/// This is rendered as:
/// ```
/// [Peer]
/// # comment here about the peer
/// PublicKey = pubkey
/// AllowedIPs = ...
/// PersistentKeepalive = 20
/// ```
///
/// WireGuard® is a registered trademark of Jason A. Donenfeld.
pub const WireguardPeer = struct {
    allocator: *std.mem.Allocator,
    wireguard_public_key: []const u8,
    allowed_ips: []const u8,

    const Self = @This();

    pub fn deinit(self: *const Self) void {
        self.allocator.free(self.wireguard_public_key);
        self.allocator.free(self.allowed_ips);
    }
};

pub const WireguardPeers = std.StringHashMap(WireguardPeer);

/// Wireguard file helper.
///
/// With this, you can use operations such as add_key and render
/// this info into an []const u8 so it can be written to a file.
///
/// To actually reload the file, look at this:
/// https://wiki.archlinux.org/index.php/WireGuard#Reload_peer_(server)_configuration
/// '# wg syncconf ${WGNET} <(wg-quick strip ${WGNET})'
///
/// WireGuard® is a registered trademark of Jason A. Donenfeld.
pub const WireguardFile = struct {
    allocator: *std.mem.Allocator,
    address: []const u8,
    private_key: []const u8,
    listen_port: []const u8,
    network_interface: []const u8,
    peers: WireguardPeers,

    const Self = @This();

    pub fn init(
        allocator: *std.mem.Allocator,
        address: []const u8,
        private_key: []const u8,
        listen_port: []const u8,
        network_interface: []const u8,
    ) Self {
        return Self{
            .allocator = allocator,
            .address = address,
            .private_key = private_key,
            .listen_port = listen_port,
            .network_interface = network_interface,
            .peers = WireguardPeers.init(allocator),
        };
    }

    pub fn deinitPeers(self: *Self) void {
        var it = self.peers.iterator();
        while (it.next()) |entry| {
            entry.value_ptr.*.deinit();
        }
        self.peers.deinit();
    }

    pub fn clearPeers(self: *Self) void {
        self.deinitPeers();
        self.peers = WireguardPeers.init(self.allocator);
    }

    pub fn deinit(self: *Self) void {
        self.deinitPeers();
    }

    pub fn format(
        self: Self,
        comptime fmt: []const u8,
        _: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        if (fmt.len == 0 or comptime std.mem.eql(u8, fmt, "s")) {
            try std.fmt.format(
                writer,
                \\[Interface]
                \\Address = {s}
                \\PrivateKey = {s}
                \\ListenPort = {s}
                \\# setup network forwarding
                \\PostUp   = iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o {s} -j MASQUERADE; ip6tables -A FORWARD -i %i -j ACCEPT; ip6tables -A FORWARD -o %i -j ACCEPT; ip6tables -t nat -A POSTROUTING -o {s} -j MASQUERADE
                \\PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o {s} -j MASQUERADE; ip6tables -D FORWARD -i %i -j ACCEPT; ip6tables -D FORWARD -o %i -j ACCEPT; ip6tables -t nat -D POSTROUTING -o {s} -j MASQUERADE
                \\
            ,
                .{
                    self.address,
                    self.private_key,
                    self.listen_port,
                    self.network_interface,
                    self.network_interface,
                    self.network_interface,
                    self.network_interface,
                },
            );

            var it = self.peers.iterator();

            while (it.next()) |entry| {
                try std.fmt.format(writer,
                    \\
                    \\[Peer]
                    \\PublicKey = {s}
                    \\AllowedIPs = {s}
                    \\PersistentKeepalive = 20
                , .{
                    entry.value_ptr.*.wireguard_public_key,
                    entry.value_ptr.*.allowed_ips,
                });
            }
        } else {
            @compileError("Unknown format character: '" ++ fmt ++ "'");
        }
    }
};

test "wireguard file rendering" {
    var wg_file = WireguardFile.init(
        std.testing.allocator,
        "10.0.0.1",
        "doctor sex",
        "6969",
        "enp1s0",
    );
    defer wg_file.deinit();

    var peer = WireguardPeer{
        .allocator = std.testing.allocator,
        .wireguard_public_key = try std.testing.allocator.dupe(u8, "doctor sex 2"),
        .allowed_ips = try std.testing.allocator.dupe(u8, "10.0.0.2"),
    };
    try wg_file.peers.put(
        "doctor sex 2",
        peer,
    );
    var buffer: [0x1000]u8 = undefined;
    const output = try std.fmt.bufPrint(&buffer, "{s}", .{wg_file});

    try std.testing.expectEqualStrings(
        \\[Interface]
        \\Address = 10.0.0.1
        \\PrivateKey = doctor sex
        \\ListenPort = 6969
        \\# setup network forwarding
        \\PostUp   = iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o enp1s0 -j MASQUERADE; ip6tables -A FORWARD -i %i -j ACCEPT; ip6tables -A FORWARD -o %i -j ACCEPT; ip6tables -t nat -A POSTROUTING -o enp1s0 -j MASQUERADE
        \\PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o enp1s0 -j MASQUERADE; ip6tables -D FORWARD -i %i -j ACCEPT; ip6tables -D FORWARD -o %i -j ACCEPT; ip6tables -t nat -D POSTROUTING -o enp1s0 -j MASQUERADE
        \\
        \\[Peer]
        \\PublicKey = doctor sex 2
        \\AllowedIPs = 10.0.0.2
        \\PersistentKeepalive = 20
    , output);
}
