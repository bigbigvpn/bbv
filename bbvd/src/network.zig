const std = @import("std");

const log = std.log.scoped(.bbvd_network);

/// Find the default interface running in the system.
///
/// Caller owns returned memory.
pub fn findDefaultInterface(allocator: *std.mem.Allocator) ![]const u8 {
    var proc = try std.ChildProcess.init(
        &[_][]const u8{ "ip", "r" },
        allocator,
    );
    defer proc.deinit();

    proc.stdout_behavior = .Pipe;
    proc.stderr_behavior = .Inherit;

    try proc.spawn();

    const ip_r_data = try proc.stdout.?.readToEndAlloc(allocator, 1024);
    defer allocator.free(ip_r_data);

    const term_result = try proc.wait();

    switch (term_result) {
        .Exited => |exit_code| {
            if (exit_code != 0) {
                log.err(
                    "failed to run command. exited with code {d}",
                    .{exit_code},
                );

                return error.FailedToExecute;
            }
        },
        .Signal, .Stopped, .Unknown => |exit_code| {
            log.err(
                "failed to run command. exited by reason ({s}) with code {d}",
                .{
                    std.meta.tagName(std.meta.activeTag(term_result)),
                    exit_code,
                },
            );
            return error.FailedToExecute;
        },
    }

    var lines_it = std.mem.split(ip_r_data, "\n");
    while (lines_it.next()) |line| {
        if (!std.mem.containsAtLeast(u8, line, 1, "default via")) {
            continue;
        }

        var space_it = std.mem.split(line, " ");
        _ = space_it.next();
        _ = space_it.next();
        _ = space_it.next();
        _ = space_it.next();
        return try allocator.dupe(u8, space_it.next().?);
    }

    unreachable;
}

test "it works" {
    const val = try findDefaultInterface(std.testing.allocator);
    defer std.testing.allocator.free(val);

    try std.testing.expect(val.len < 10);

    // TODO validate given val is in 'ip a' output
}
