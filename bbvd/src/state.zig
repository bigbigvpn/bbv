const std = @import("std");

const zuri = @import("zuri");
const Uri = zuri.Uri;

const wgfile = @import("wireguard_file.zig");
const WireguardFile = wgfile.WireguardFile;
const WireguardPeer = wgfile.WireguardPeer;

const main = @import("main.zig");
const messages = @import("messages.zig");
const Message = messages.Message;

const ws = @import("websocket.zig");

const log = main.logger;

const OFFLINE_LIMIT = 60 * std.time.s_per_min; // offline for 1h = shutdown
const POLL_TIME = 45 * std.time.ns_per_s; // poll every 45s

const isWireguardExecutor = std.meta.trait.multiTrait(.{
    std.meta.trait.hasFn("reload"),
    std.meta.trait.hasFn("endpointCount"),
    std.meta.trait.hasFn("allClientTimeout"),
});

const isWireguardFile = std.meta.trait.multiTrait(.{
    std.meta.trait.hasField("peers"),
});

const isWireguardSaver = std.meta.trait.multiTrait(.{
    std.meta.trait.hasFn("saveToPath"),
});

pub const LoginError = error{
    HandshakeFailure,
    LoginFailure,
    InvalidCredentials,
};

fn createOwnedPeer(allocator: *std.mem.Allocator, key: messages.WireguardKey) !WireguardPeer {
    var buf: [1024]u8 = undefined;
    const concatenated_address = try std.fmt.bufPrint(
        &buf,
        "{s}, {s}",
        .{ key.local_ipv4_address, key.local_ipv6_address },
    );

    return WireguardPeer{
        .allocator = allocator,
        .wireguard_public_key = try allocator.dupe(u8, key.wireguard_public_key),
        .allowed_ips = try allocator.dupe(u8, concatenated_address),
    };
}

pub fn ClientState(
    comptime WireguardExecutor: type,
    comptime WebsocketClient: type,
    comptime WireguardFileImpl: type,
    comptime WireguardSaver: type,
) type {
    if (comptime !isWireguardExecutor(WireguardExecutor))
        @compileError("Given executor type does not follow interface");
    if (comptime !isWireguardFile(WireguardFileImpl))
        @compileError("Given executor type does not follow interface");
    if (comptime !isWireguardSaver(WireguardSaver))
        @compileError("Given executor type does not follow interface");

    return struct {
        allocator: *std.mem.Allocator,
        wg_file: *WireguardFileImpl,
        wg_file_path: []const u8,
        client: *WebsocketClient,
        executor: *WireguardExecutor,
        saver: *WireguardSaver,

        running_offline_check_thread: ?std.Thread = null,

        const Self = @This();

        pub fn init(
            allocator: *std.mem.Allocator,
            executor: *WireguardExecutor,
            wg_file: *WireguardFileImpl,
            client: *WebsocketClient,
            wg_file_path: []const u8,
            saver: *WireguardSaver,
        ) Self {
            return Self{
                .allocator = allocator,
                .wg_file = wg_file,
                .executor = executor,
                .client = client,
                .wg_file_path = wg_file_path,
                .saver = saver,
            };
        }

        pub fn deinit(self: *Self) void {
            _ = self;
        }

        pub fn maybeSpawnOfflineThread(self: *Self) !void {
            if (self.running_offline_check_thread == null) {
                log.info("spawning offline check thread", .{});

                self.running_offline_check_thread = try std.Thread.spawn(
                    .{},
                    Self.offlineCheckThread,
                    .{self},
                );
            }
        }

        fn offlineCheckThread(self: *Self) !void {
            self.offlineCheckThreadWrapped() catch |err| {
                log.err(
                    "an error happened in the offline check thread: {s}",
                    .{@errorName(err)},
                );
                self.running_offline_check_thread = null;
                return err;
            };
        }

        fn notifyNoClients(self: *Self) !void {
            try self.client.sendMessage(.{
                .op = .notify,
                .d = .{
                    .notify = .{
                        .t = .no_clients,
                        .v = .{ .no_clients = null },
                    },
                },
            });
        }

        pub fn offlineCheckThreadTick(self: *Self, offline_since: *?i64) !void {
            const counts = try self.executor.endpointCount();
            const all_timed_out = try self.executor.allClientTimeout();
            const currently_offline: bool =
                (counts.total_count == counts.offline_count) or all_timed_out;

            log.info(
                "peers: total {d} offline {d}, all timeout? {} currently offline? {}",
                .{
                    counts.total_count,
                    counts.offline_count,
                    all_timed_out,
                    currently_offline,
                },
            );

            log.info("since before? {d}", .{offline_since});

            if (currently_offline and offline_since.* == null) {
                offline_since.* = std.time.timestamp();
            } else if (!currently_offline) {
                // if we got online, reset the counter
                offline_since.* = null;
            }

            log.info("since after? {d}", .{offline_since});

            // if we're not actually offline, skip to the next tick
            if (offline_since.* == null) return;

            const offline_time_frame = std.time.timestamp() - offline_since.*.?;
            if (offline_time_frame > 0) {
                const offline_for: i64 = try std.math.divTrunc(i64, offline_time_frame, std.time.s_per_min);
                log.info("have been offline for {d}minutes", .{
                    offline_for,
                });
            }

            if (offline_time_frame > OFFLINE_LIMIT) {
                log.warn("i have been offline for too long!", .{});
                try self.notifyNoClients();
            }
        }

        fn offlineCheckThreadWrapped(self: *Self) !void {
            var offline_since: ?i64 = null;

            while (true) {
                std.time.sleep(POLL_TIME);
                try self.offlineCheckThreadTick(&offline_since);
            }
        }

        pub fn removeKey(self: *Self, key: messages.WireguardKey) void {
            const entry_opt = self.wg_file.peers.getEntry(key.wireguard_public_key);

            if (entry_opt) |entry| {
                const copy = entry.value_ptr.*;
                std.debug.assert(self.wg_file.peers.remove(key.wireguard_public_key));
                copy.deinit();
                log.info("removed peer: pubkey={s}", .{key.wireguard_public_key});

                const fetched =
                    self.wg_file.peers.get(key.wireguard_public_key);
                std.debug.assert(fetched == null);
            } else {
                log.err("peer not found for removal: pubkey={s} {d}", .{ key.wireguard_public_key, key.wireguard_public_key.len });
            }
        }

        pub fn addKey(self: *Self, key: messages.WireguardKey) !void {
            const owned_peer = try createOwnedPeer(self.wg_file.peers.allocator, key);
            log.info(
                "add peer: pubkey={s}",
                .{owned_peer.wireguard_public_key},
            );

            var old_val_opt = self.wg_file.peers.get(owned_peer.wireguard_public_key);
            if (old_val_opt) |old_val| {
                const copy = old_val;
                std.debug.assert(self.wg_file.peers.remove(owned_peer.wireguard_public_key));
                std.debug.assert(self.wg_file.peers.get(owned_peer.wireguard_public_key) == null);
                // free strings held by the old peer in the map
                copy.deinit();
            }

            try self.wg_file.peers.put(owned_peer.wireguard_public_key, owned_peer);

            const fetched =
                self.wg_file.peers.get(owned_peer.wireguard_public_key);
            std.debug.assert(fetched != null);
            std.debug.assert(std.mem.eql(u8, fetched.?.wireguard_public_key, owned_peer.wireguard_public_key));
        }

        pub fn handleCommand(self: *Self, command: Message.Command) !void {
            log.info("handling command {}", .{command.t});
            switch (command.t) {
                .sync_wireguard_keys => {
                    const data = command.v.sync_wireguard_keys;

                    // sync command means we need to delete the entire peers we hold
                    // then add the ones we're given.
                    //
                    // we approach this by cleaning the hashmap then put()'ing
                    // it all back.

                    self.wg_file.clearPeers();
                    try self.wg_file.peers.ensureCapacity(@as(u32, @intCast(data.keys.len)));

                    for (data.keys) |key| {
                        // we don't own the lifetime of the WireguardKey struct,
                        // so we need to dupe it with our own allocator.
                        const owned_peer = try createOwnedPeer(self.wg_file.peers.allocator, key);

                        log.info(
                            "received peer: ips={s} pubkey={s}",
                            .{ owned_peer.allowed_ips, key.wireguard_public_key },
                        );

                        self.wg_file.peers.putAssumeCapacity(owned_peer.wireguard_public_key, owned_peer);
                        const fetched =
                            self.wg_file.peers.get(owned_peer.wireguard_public_key);
                        std.debug.assert(fetched != null);
                        std.debug.assert(std.mem.eql(u8, fetched.?.wireguard_public_key, owned_peer.wireguard_public_key));
                    }

                    try self.saveWireguard();
                    try self.executor.reload();
                },
                .add_wireguard_keys => {
                    const data = command.v.sync_wireguard_keys;
                    for (data.keys) |key| {
                        try self.addKey(key);
                    }

                    try self.saveWireguard();
                    try self.executor.reload();
                },
                .remove_wireguard_keys => {
                    const data = command.v.sync_wireguard_keys;
                    for (data.keys) |key| {
                        self.removeKey(key);
                    }

                    try self.executor.reload();
                },
            }
        }

        fn saveWireguard(self: Self) !void {
            log.info("saving wireguard file", .{});
            try self.saver.saveToPath(self.wg_file, self.wg_file_path);
        }

        pub fn mainLoop(self: *Self) !void {
            // keep a thread running that waits for offline clients
            try self.maybeSpawnOfflineThread();

            while (try self.client.next()) |event| {
                switch (event.header.opcode) {
                    .Text => {
                        std.debug.assert(event.header.opcode == .Text);
                        const text_event = (try self.client.next()).?;
                        std.debug.assert(text_event == .chunk);
                        log.info(
                            "got message ({d} bytes): '{s}'",
                            .{ text_event.chunk.data.len, text_event.chunk.data },
                        );

                        var tokens = std.json.TokenStream.init(text_event.chunk.data);
                        const opts = std.json.ParseOptions{ .allocator = self.allocator };
                        var message = try std.json.parse(Message, &tokens, opts);
                        defer std.json.parseFree(Message, message, opts);

                        switch (message.op) {
                            .heartbeat => {
                                try self.client.sendMessage(Message{
                                    .op = .heartbeat_ack,
                                    .d = .{ .heartbeat_ack = null },
                                });
                                try self.maybeSpawnOfflineThread();
                            },

                            .command => self.handleCommand(message.d.command) catch |err| {
                                log.err("error handling command: {s}", .{@errorName(err)});
                                if (@errorReturnTrace()) |trace| {
                                    std.debug.dumpStackTrace(trace.*);
                                }
                            },

                            else => log.info("unexpected opcode: {}", .{message.op}),
                        }
                    },
                    .Close => {
                        const close_data = try ws.CloseData.fromNextEvent((try self.client.next()).?);
                        log.info(
                            "connection closed: code={d} reason='{s}'",
                            .{ close_data.code, close_data.reason },
                        );

                        return error.WebsocketClosed;
                    },
                    else => {
                        log.err("invalid websocket opcode {s}", .{@tagName(event.header.opcode)});
                        @panic("got invalid websocket opcode");
                    },
                }
            }
        }

        const LoginOptions = struct {
            log: bool = true,
        };

        /// Initiate a websocket connection given a stream, send a login message
        /// and expect a welcome message in the reply.
        pub fn login(
            self: *Self,
            uri: Uri,
            vpn_id: []const u8,
            vpn_token: []const u8,
        ) LoginError!void {
            log.info("handshaking websocket", .{});
            self.client.executeHandshake(uri) catch |err| {
                log.err("Failed to handshake: {s}", .{@errorName(err)});
                return error.HandshakeFailure;
            };

            log.info("finished handshake", .{});

            self.client.sendMessage(Message{
                .op = .login,
                .d = .{
                    .login = .{
                        .id = vpn_id,
                        .token = vpn_token,
                    },
                },
            }) catch |err| {
                log.warn("Failed to send login message: {s}", .{@errorName(err)});
                return error.LoginFailure;
            };

            const welcome_event = (self.client.next() catch |err| {
                log.warn("Failed to get welcome event: {s}", .{@errorName(err)});
                return error.LoginFailure;
            }).?;

            // we don't need to hold the welcome reply beyond our own stack.
            var welcome_parse_buffer: [0x1000]u8 = undefined;
            var fba = std.heap.FixedBufferAllocator.init(&welcome_parse_buffer);
            const allocator = &fba.allocator;

            var parser = std.json.Parser.init(allocator, false);
            defer parser.deinit();

            switch (welcome_event.header.opcode) {
                .Text => {
                    const welcome_text_event = (self.client.next() catch |err| {
                        log.warn("Failed to get welcome text event: {s}", .{@errorName(err)});
                        return error.LoginFailure;
                    }).?;
                    std.debug.assert(welcome_text_event == .chunk);

                    var tree = parser.parse(welcome_text_event.chunk.data) catch |err| {
                        log.warn("Failed to parse welcome text: {s}", .{@errorName(err)});
                        return error.LoginFailure;
                    };
                    parser.reset();
                    defer tree.deinit();

                    var root = tree.root.Object;

                    // TODO maybe some protocol versioning?
                    const opcode = std.meta.intToEnum(messages.OpCode, root.get("op").?.Integer) catch |err| {
                        log.warn("invalid opcode in control protocol. ({s})", .{@errorName(err)});
                        return error.LoginFailure;
                    };
                    if (opcode != .welcome) {
                        log.warn("unexpected opcode after login: {}", .{opcode});
                        return error.LoginFailure;
                    }

                    log.info("logged in!", .{});
                },
                .Close => {
                    const close_data = try ws.CloseData.fromNextEvent((self.client.next() catch |err| {
                        log.warn("failed to get close event: {s}", .{@errorName(err)});
                        return error.LoginFailure;
                    }).?);

                    log.warn(
                        "failed to login: code={d} reason={s}",
                        .{ close_data.code, close_data.reason },
                    );

                    return switch (close_data.code) {
                        4001 => error.InvalidCredentials,
                        else => error.LoginFailure,
                    };
                },
                else => {
                    log.warn("invalid message type: {d}", .{welcome_event.header.opcode});
                    @panic("invalid message type");
                },
            }
        }
    };
}
