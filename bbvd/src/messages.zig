const std = @import("std");

pub const OpCode = enum(u16) {
    login = 1,
    welcome = 2,
    heartbeat = 3,
    heartbeat_ack = 4,
    command = 5,
    notify = 6,

    pub fn jsonStringify(
        self: @This(),
        options: std.json.StringifyOptions,
        writer: anytype,
    ) @TypeOf(writer).Error!void {
        return std.json.stringify(@intFromEnum(self), options, writer);
    }
};

pub const CommandType = enum(u16) {
    /// Synchronize our wireguard key state with the given keys.
    sync_wireguard_keys = 0,
    add_wireguard_keys = 1,
    remove_wireguard_keys = 2,

    pub fn jsonStringify(
        self: @This(),
        options: std.json.StringifyOptions,
        writer: anytype,
    ) @TypeOf(writer).Error!void {
        return std.json.stringify(@intFromEnum(self), options, writer);
    }
};

pub const CommandValue = union(CommandType) {
    // We can only have add_ and remove_ if they become actually different
    sync_wireguard_keys: struct {
        keys: []WireguardKey,
    },
    add_wireguard_keys: ?struct {},
    remove_wireguard_keys: ?struct {},
    // add_wireguard_keys: struct {
    //     keys: []WireguardKey,
    // },
    // remove_wireguard_keys: struct {
    //     keys: []WireguardKey,
    // },
};

pub const NotifyType = enum(u16) {
    no_clients = 0,

    pub fn jsonStringify(
        self: @This(),
        options: std.json.StringifyOptions,
        writer: anytype,
    ) @TypeOf(writer).Error!void {
        return std.json.stringify(@intFromEnum(self), options, writer);
    }
};

pub const NotifyValue = union(NotifyType) {
    no_clients: ?struct {},
};

pub const WireguardKey = struct {
    user_id: []const u8,
    wireguard_public_key: []const u8,
    local_ipv4_address: []const u8,
    local_ipv6_address: []const u8,
};

pub const Message = struct {
    op: OpCode,
    d: union(OpCode) {
        login: Login,
        welcome: Welcome,
        heartbeat: Heartbeat,
        heartbeat_ack: HeartbeatAck,
        command: Command,
        notify: Notify,
    },

    pub const Login = struct {
        id: []const u8,
        token: []const u8,
    };

    pub const Welcome = ?struct {};

    pub const Heartbeat = ?struct {};

    pub const HeartbeatAck = ?struct {};

    pub const Command = struct {
        t: CommandType,
        v: CommandValue,
    };

    pub const Notify = struct {
        t: NotifyType,
        v: NotifyValue,
    };
};
