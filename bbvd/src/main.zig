const std = @import("std");
const Uri = @import("zuri").Uri;

const wgfile = @import("wireguard_file.zig");
const WireguardFile = wgfile.WireguardFile;
const WireguardPeer = wgfile.WireguardPeer;

const SystemWireguardExecutor = @import("wireguard_executor.zig").SystemWireguardExecutor;

const ws = @import("websocket.zig");
const WzClient = ws.WzClient;

const net_utils = @import("network.zig");
const tls = @import("tls");

const messages = @import("messages.zig");
const Message = messages.Message;

const ClientState = @import("state.zig").ClientState;

// always include the ssp helper functions
// look at build.zig for more details
comptime {
    _ = @import("ssp.zig");
}

pub const logger = std.log.scoped(.bbvd);
// https://github.com/ziglang/zig/pull/9193
// i am a great developer -lun-4
const log = logger;

fn tlsConnect(tls_ctx: *tls.Context, name: []const u8, port: u16) !void {
    try tls_ctx.connect(name, port);
    try tls_ctx.handshake();
    try tls_ctx.verify();
}

/// Connect to a given URI.
///
/// Returns an tls-or-plaintext stream (see tls.MaybeSecureInnerStream).
fn connectToUri(allocator: *std.mem.Allocator, uri: *Uri) error{
    InvalidURL,
    TlsContextFailure,
    OutOfMemory,
    ConnectFailure,
}!tls.MaybeSecureInnerStream {
    const is_ws = std.mem.eql(u8, uri.scheme, "ws");
    const is_wss = std.mem.eql(u8, uri.scheme, "wss");
    if (!(is_ws or is_wss)) {
        log.err("expected ws or wss, got '{s}'", .{uri.scheme});
        return error.InvalidURL;
    }

    const port: u16 = uri.port orelse @as(u16, if (is_wss) 443 else 80);

    var address_list: ?*std.net.AddressList = null;
    defer {
        // if we used dns, then address_list won't be null, and we can deinit it
        if (address_list != null) address_list.?.deinit();
    }

    var tls_ctx_opt: ?*tls.Context = null;
    errdefer {
        if (tls_ctx_opt) |tls_ctx| {
            allocator.destroy(tls_ctx);
        }
    }

    // we purposefully keep the Context struct allocatd in the heap, as
    // MaybeSecureInnerStream needs a *Context and we can not give a
    // pointer to stack memory.
    if (is_wss) {
        tls_ctx_opt = try allocator.create(tls.Context);
        tls_ctx_opt.?.* = tls.Context.init(allocator) catch |err| {
            log.err("failed to init tls context: {s}", .{@errorName(err)});
            return error.TlsContextFailure;
        };
    }

    const addr = switch (uri.host) {
        .name => |actual_name| blk: {
            if (tls_ctx_opt) |tls_ctx| {
                // if we get a name and we are going to tls, we MUST use the full
                // domain.
                tlsConnect(tls_ctx, actual_name, port) catch |err| {
                    log.err("failed to conenct tls socket: {s}", .{@errorName(err)});
                    return error.ConnectFailure;
                };
                return tls.MaybeSecureInnerStream{ .tls = tls_ctx };
            } else {
                // if its plaintext, we try to resolve the given domain
                // and connect to it
                address_list = std.net.getAddressList(allocator, actual_name, port) catch |err| {
                    log.err("failed to find plaintext socket ip address: {s}", .{@errorName(err)});
                    return error.ConnectFailure;
                };
                break :blk address_list.?.addrs[0];
            }
        },
        .ip => |*inner_addr| blk: {
            if (tls_ctx_opt != null) {
                // We do not want to disable domain validation
                log.err("Direct IP connection with TLS is not supported due to security concerns.", .{});
                return error.InvalidURL;
            }

            if (inner_addr.getPort() == 0)
                inner_addr.setPort(port);

            break :blk inner_addr.*;
        },
    };

    // TODO if we fail to connect at first, AND we used the address_list,
    // we can attempt to cycle through the other addresses in the list.
    return tls.MaybeSecureInnerStream{
        .plaintext = std.net.tcpConnectToAddress(addr) catch |err| {
            log.err("failed to connect plaintext socket: {s}", .{@errorName(err)});
            return error.ConnectFailure;
        },
    };
}

fn maybeSleep(current_connection_retry: usize) void {
    const seed = @as(u64, @truncate(@as(u128, @bitCast(std.time.nanoTimestamp()))));
    var r = std.rand.DefaultPrng.init(seed);

    // cap our retry calculations to 20 because we could overflow usize
    // if we go higher and higher.
    const actual_retry: usize = if (current_connection_retry > 20) 20 else current_connection_retry;
    const retry_sleep_target = std.math.pow(usize, 2, actual_retry);

    // random delay can be at most between 0 and 60s
    const sleep_ms = r.random.uintLessThan(usize, std.math.min(60000, retry_sleep_target));

    log.err(
        "connection failed. reconnecting in {d}ms (retry {d}, target {d}ms)",
        .{ sleep_ms, current_connection_retry, retry_sleep_target },
    );

    std.time.sleep(sleep_ms * std.time.ns_per_ms);
}

pub fn main() anyerror!void {
    var allocator_instance = std.heap.GeneralPurposeAllocator(.{}){};
    defer {
        _ = allocator_instance.deinit();
    }
    const allocator = &allocator_instance.allocator;

    // find ifaces, get env vars, etc.
    const default_interface = try net_utils.findDefaultInterface(allocator);
    defer allocator.free(default_interface);

    var wg_file = WireguardFile.init(
        allocator,
        std.os.getenv("BBVD_WIREGUARD_IP").?,
        std.os.getenv("BBVD_WIREGUARD_KEY").?,
        std.os.getenv("BBVD_WIREGUARD_PORT").?,
        default_interface,
    );
    defer wg_file.deinit();

    const wg_interface = std.os.getenv("BBVD_WIREGAURD_INTERFACE") orelse "wg0";
    const wg_quick_bin = std.os.getenv("BBVD_WIREGUARD_WG_QUICK_BIN") orelse "/usr/bin/wg-quick";
    const wg_bin = std.os.getenv("BBVD_WIREGUARD_WG_BIN") orelse "/usr/bin/wg";

    var system_wg = SystemWireguardExecutor.init(allocator, wg_interface, wg_bin, wg_quick_bin);
    try system_wg.assertBinariesExist();

    const uri_string = std.os.getenv("BBVD_URL") orelse return error.NoUrlProvided;
    var uri = try Uri.parse(uri_string, true);

    var current_connection_retry: usize = 0;
    while (true) {
        current_connection_retry += 1;

        var inner_stream = connectToUri(allocator, &uri) catch |err| switch (err) {
            error.InvalidURL => @panic("invalid url"),
            else => {
                log.err("error connecting: {s}", .{@errorName(err)});

                // the algorithm used for exponential backoff is the FullJitter algorithm
                //  (with custom modifications to make it work in bbvd's nature)
                //
                // this algorithm (and others) are described in here:
                // https://aws.amazon.com/blogs/architecture/exponential-backoff-and-jitter/

                maybeSleep(current_connection_retry);
                continue;
            },
        };

        var stream = tls.MaybeSecureStream.init(inner_stream);
        errdefer stream.deinit();

        // indeed, we can only handle 4kb websocket messages in the end. as things
        // scale (especially wireguard keys, as they contain a lot of data in json)
        // we may want to investigate bumping this limit.
        var wz_buffer: [0x1000]u8 = undefined;

        var wz_client: ws.WzClient = ws.wz.base.client.create(
            &wz_buffer,
            stream.reader(),
            stream.writer(),
        );

        var client = ws.Websocket{ .client = &wz_client };
        errdefer {
            if (wz_client.handshaken) {
                client.closeWithReason(4000, "had an error") catch {};
            }
        }
        log.info("connected and logged in!", .{});

        var saver = SystemWireguardSaver{};

        var state = SystemClientState.init(
            allocator,
            &system_wg,
            &wg_file,
            &client,
            std.os.getenv("BBVD_WIREGUARD_PATH") orelse "./wg0.conf",
            &saver,
        );
        defer state.deinit();

        const vpn_id = std.os.getenv("BBVD_VPN_ID").?;
        const vpn_token = std.os.getenv("BBVD_VPN_TOKEN").?;

        state.login(uri, vpn_id, vpn_token) catch |err| switch (err) {
            error.InvalidCredentials => {
                log.err("invalid login credentials provided ", .{});
                return;
            },
            else => {
                log.err("error logging in: {s}", .{@errorName(err)});
                maybeSleep(current_connection_retry);
                continue;
            },
        };

        current_connection_retry = 0;

        try state.mainLoop();
    }
}

const SystemClientState = ClientState(
    SystemWireguardExecutor,
    ws.Websocket,
    WireguardFile,
    SystemWireguardSaver,
);

const ConnectResult = struct {
    stream: tls.MaybeSecureStream,
    wz_client: WzClient,
};

const SystemWireguardSaver = struct {
    const Self = @This();
    pub fn saveToPath(self: *Self, wg_file: *const WireguardFile, path: []const u8) !void {
        _ = self;
        var file = try std.fs.cwd().createFile(path, .{});
        defer file.close();
        try file.writer().print("{s}", .{wg_file});
    }
};

test "everything" {
    _ = @import("test.zig");
    std.testing.refAllDecls(@This());
}
