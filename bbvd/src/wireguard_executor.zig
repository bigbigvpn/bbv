const std = @import("std");
const main = @import("main.zig");

const log = main.logger;

const MAX_TIMEOUT = 2 * 60; // amount of seconds that we will consider a client disconnected (2 minutes)

/// Caller owns returned memory.
fn runAndFetchStdout(
    args: [][]const u8,
    allocator: *std.mem.Allocator,
) ![]const u8 {
    var proc = try std.ChildProcess.init(args, allocator);
    defer proc.deinit();

    proc.stdout_behavior = .Pipe;
    proc.stderr_behavior = .Inherit;

    try proc.spawn();

    const stdout_data = try proc.stdout.?.reader().readAllAlloc(allocator, 32768);

    const result = try proc.wait();
    var exit_code: u32 = undefined;
    switch (result) {
        .Exited => |given_code| {
            exit_code = given_code;
        },
        .Signal, .Stopped, .Unknown => |given_code| {
            exit_code = given_code;
        },
    }
    if (exit_code != 0) {
        log.err("error running wireguard command: code {d}", .{exit_code});
        return error.WireguardRunFail;
    }

    return stdout_data;
}

pub const SystemWireguardExecutor = struct {
    allocator: *std.mem.Allocator,
    interface: []const u8,
    binary_path: []const u8,
    quick_binary_path: []const u8,

    const Self = @This();

    pub fn init(
        allocator: *std.mem.Allocator,
        interface: []const u8,
        binary_path: []const u8,
        quick_binary_path: []const u8,
    ) Self {
        return Self{
            .allocator = allocator,
            .interface = interface,
            .binary_path = binary_path,
            .quick_binary_path = quick_binary_path,
        };
    }

    pub fn assertBinariesExist(self: Self) !void {
        var wg_file = std.fs.cwd().openFile(
            self.binary_path,
            .{ .read = true, .write = false },
        ) catch |err| {
            switch (err) {
                error.FileNotFound => return error.WgNotFound,
                else => return err,
            }
        };
        defer wg_file.close();

        var wg_quick_file = std.fs.cwd().openFile(
            self.quick_binary_path,
            .{ .read = true, .write = false },
        ) catch |err| {
            switch (err) {
                error.FileNotFound => return error.WgQuickNotFound,
                else => return err,
            }
        };
        defer wg_quick_file.close();
    }

    /// Start a WireGuard® interface.
    ///
    /// Returns if the configured WireGuard® interface is already up.
    fn start(self: Self) !bool {
        var proc = try std.ChildProcess.init(&[_][]const u8{
            self.quick_binary_path,
            "up",
            self.interface,
        }, self.allocator);
        defer proc.deinit();

        proc.stdout_behavior = .Inherit;
        proc.stderr_behavior = .Inherit;

        const term_result = try proc.spawnAndWait();

        switch (term_result) {
            .Exited => |exit_code| {
                if (exit_code == 0) {
                    log.info("started wg", .{});
                    return true;
                } else {
                    log.err(
                        "failed to start. exited by reason ({s}) with code {d}. wg probably already up",
                        .{
                            std.meta.tagName(std.meta.activeTag(term_result)),
                            exit_code,
                        },
                    );
                    return false;
                }
            },
            .Signal, .Stopped, .Unknown => |exit_code| {
                log.err(
                    "failed to run wg command. exited by reason ({s}) with code {d}",
                    .{
                        std.meta.tagName(std.meta.activeTag(term_result)),
                        exit_code,
                    },
                );
                return false;
            },
        }
    }

    pub fn reload(self: Self) !void {
        log.info("reloading wireguard state", .{});
        // if we successfully started the interface, we don't need to reload it
        if (try self.start()) return;

        var shell_line_buf: [0x500]u8 = undefined;
        const shell_line = try std.fmt.bufPrint(
            &shell_line_buf,
            "({s} strip {s} > /tmp/bbvd-stripped-{s}.conf) && {s} syncconf {s} /tmp/bbvd-stripped-{s}.conf",
            .{
                self.quick_binary_path,
                self.interface,
                self.interface,
                self.binary_path,
                self.interface,
                self.interface,
            },
        );

        var proc = try std.ChildProcess.init(&[_][]const u8{
            "/bin/sh",
            "-c",
            shell_line,
        }, self.allocator);
        defer proc.deinit();

        proc.stdout_behavior = .Inherit;
        proc.stderr_behavior = .Inherit;

        const term_result = try proc.spawnAndWait();

        switch (term_result) {
            .Exited => |exit_code| {
                if (exit_code == 0) {
                    log.info("reloaded wg config successfully", .{});
                } else {
                    log.err(
                        "failed to run wg command. exited by reason ({s}) with code {d}",
                        .{
                            std.meta.tagName(std.meta.activeTag(term_result)),
                            exit_code,
                        },
                    );
                }
            },
            .Signal, .Stopped, .Unknown => |exit_code| {
                log.err(
                    "failed to run wg command. exited by reason ({s}) with code {d}",
                    .{
                        std.meta.tagName(std.meta.activeTag(term_result)),
                        exit_code,
                    },
                );
            },
        }
    }

    const ConnectedPeers = struct {
        total_count: usize,
        offline_count: usize,
    };

    pub fn endpointCount(self: Self) !ConnectedPeers {
        const stdout = try runAndFetchStdout(&[_][]const u8{
            self.binary_path,
            "show",
            self.interface,
            "endpoints",
        }, self.allocator);
        defer self.allocator.free(stdout);

        const total_count = std.mem.count(u8, stdout, "\n");
        const offline_count = std.mem.count(u8, stdout, "(none");

        return ConnectedPeers{
            .total_count = total_count,
            .offline_count = offline_count,
        };
    }

    /// Returns if all of the running clients have timed out.
    pub fn allClientTimeout(self: Self) !bool {
        const stdout = try runAndFetchStdout(
            &[_][]const u8{
                self.binary_path,
                "show",
                self.interface,
                "latest-handshakes",
            },
            self.allocator,
        );
        defer self.allocator.free(stdout);

        var it = std.mem.split(stdout, "\n");
        const now = std.time.timestamp();

        while (it.next()) |line| {
            if (line.len == 0) continue;

            // line comes in the format
            // <wg_key>\t<latest_handshake_unix_time>

            var tab_it = std.mem.split(line, "\t");

            _ = tab_it.next();
            const peer_last_handshake_str = tab_it.next().?;
            const peer_last_handshake = try std.fmt.parseInt(i64, peer_last_handshake_str, 10);

            const seconds_since_last_handshake = now - peer_last_handshake;
            if (seconds_since_last_handshake < MAX_TIMEOUT) {
                return false;
            }
        }

        return true;
    }
};
