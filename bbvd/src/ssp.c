#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>

int __printf_chk(int _flag, const char *fmt, ...) {
  int ret;
  va_list ap;
  va_start(ap, fmt);
  ret = vfprintf(stdout, fmt, ap);
  va_end(ap);
  return ret;
}

int __snprintf_chk(char *restrict s, size_t n, int _flag, size_t _slen,
                   const char *restrict fmt, ...) {
  int ret;
  va_list ap;
  va_start(ap, fmt);
  ret = vsnprintf(s, n, fmt, ap);
  va_end(ap);
  return ret;
}

int __vsnprintf_chk(char *restrict s, size_t n, int _flag, size_t _slen,
                    const char *restrict fmt, va_list ap) {
  return vsnprintf(s, n, fmt, ap);
}

size_t __fread_chk(void *restrict destv, size_t size, size_t nmemb, size_t _n,
                   FILE *restrict f) {
  return fread(destv, size, nmemb, f);
}

long int __fdelt_chk(long int d) { return d / NFDBITS; }
