// Websocket helpers

const std = @import("std");
const tls = @import("tls");
const zuri = @import("zuri");

pub const wz = @import("wz");

const messages = @import("messages.zig");
const Message = messages.Message;

const main = @import("main.zig");
const log = main.logger;

pub const isWebsocketClient = std.meta.trait.multiTrait(.{
    std.meta.trait.hasFn("next"),
    std.meta.trait.hasFn("executeHandshake"),
    std.meta.trait.hasFn("closeWithCode"),
    std.meta.trait.hasFn("closeWithReason"),
    std.meta.trait.hasFn("sendMessage"),
});

pub const WzClient = wz.base.client.BaseClient(
    tls.MaybeSecureStream.Reader,
    tls.MaybeSecureStream.Writer,
);

comptime {
    std.debug.assert(isWebsocketClient(Websocket));
}

pub const Websocket = struct {
    client: *WzClient,

    const Self = @This();

    pub fn next(self: *Self) !?wz.base.client.Event {
        return try self.client.next();
    }

    pub fn executeHandshake(self: *Self, uri: zuri.Uri) !void {
        try self.client.handshakeStart(uri.path);
        switch (uri.host) {
            .ip => |host_addr| try self.client.handshakeAddHeaderValueFormat("Host", "{}", .{host_addr}),
            .name => |str| try self.client.handshakeAddHeaderValueFormat("Host", "{s}", .{str}),
        }
        try self.client.handshakeFinishHeaders();
        _ = try self.client.handshakeAccept();
    }

    pub fn closeWithCode(self: *Self, code: u16) !void {
        var close_data = CloseData{ .code = code, .reason = "" };
        var close_msg_buffer: [2]u8 = undefined;
        _ = close_data.toBuffer(&close_msg_buffer);
        try self.client.writeHeader(.{ .opcode = .Close, .length = 2 });
        try self.client.writeChunk(&close_msg_buffer);
    }

    pub fn closeWithReason(self: *Self, comptime code: u16, comptime reason: []const u8) !void {
        var close_data = CloseData{ .code = code, .reason = reason };
        var close_msg_buffer: [@sizeOf(u16) + reason.len]u8 = undefined;
        const len = close_data.toBuffer(&close_msg_buffer);
        try self.client.writeHeader(.{ .opcode = .Close, .length = len });
        try self.client.writeChunk(close_msg_buffer[0..len]);
    }

    pub fn sendMessage(self: *Self, message: Message) !void {
        var buf: [0x1000]u8 = undefined;
        var buf_stream = std.io.FixedBufferStream([]u8){ .buffer = &buf, .pos = 0 };

        try std.json.stringify(message, .{}, buf_stream.writer());
        const end_pos = buf_stream.pos;

        log.info("send message: {s}", .{buf[0..end_pos]});

        try self.client.writeHeader(.{ .opcode = .Text, .length = end_pos });
        try self.client.writeChunk(buf[0..end_pos]);
    }
};

/// Representation of a websocket close message.
///
/// Errors in the Websocket protocol have a code and a reason.
pub const CloseData = struct {
    code: u16,
    reason: []const u8,

    const Self = @This();

    /// Create a CloseData struct out of full event data given by a websocket
    /// library.
    pub fn init(event_data: []const u8) Self {
        return Self{
            .code = std.mem.readIntBig(u16, event_data[0..2]),
            .reason = event_data[2..],
        };
    }

    /// Create a CloseData struct out of the next events' data from a
    /// wz client.
    pub fn fromNextEvent(close_event: wz.base.client.Event) !Self {
        std.debug.assert(close_event == .chunk);
        return CloseData.init(close_event.chunk.data);
    }

    /// 'write' the close data into a buffer.
    ///
    /// Returns the amount of bytes written into the buffer.
    pub fn toBuffer(self: Self, buffer: []u8) usize {
        std.mem.writeIntBig(u16, buffer[0..2], self.code);
        std.mem.copy(u8, buffer[2..], self.reason);
        return 2 + self.reason.len;
    }
};
