const std = @import("std");
const deps = @import("./deps.zig");

fn linkLibraries(step: anytype) void {
    deps.addAllTo(step);
    // step.addIncludeDir("/usr/include");
    // step.linkLibC();
    // step.addObjectFile("/usr/lib/libmbedtls.a");
    // step.addObjectFile("/usr/lib/libmbedcrypto.a");
    // step.addObjectFile("/usr/lib/libmbedx509.a");
}

pub fn build(b: *std.build.Builder) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{
        .default_target = .{
            .abi = .musl,
        },
    });

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const exe = b.addExecutable("bbvd", "src/main.zig");
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.install();

    const test_unit = b.addTest("src/main.zig");
    test_unit.setTarget(target);
    test_unit.setBuildMode(mode);

    // trying to build mbedtls with musl causes issues due to zig not providing
    // the relevant ssp symbols for this target.
    //
    // we copy the lib/std/special/ssp.zig file from the zig project into our
    // source tree so we can provide stack protection in important memory
    // functions such as memcpy and memmove.
    //
    // then another file, ssp.c, bypasses stack protection for the other
    // symbols mbedtls is linking against (printf, fread, etc.).
    //
    // we want to use musl so we can build a binary that works in any
    // linux distribution.
    exe.addCSourceFile("src/ssp.c", &[_][]const u8{});

    linkLibraries(exe);
    linkLibraries(test_unit);

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&test_unit.step);
}
