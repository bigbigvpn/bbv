# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only
import pytest

pytestmark = pytest.mark.asyncio


async def test_the_universe_is_ok():
    assert 1 == 1
