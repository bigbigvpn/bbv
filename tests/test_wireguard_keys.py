# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only
import pytest

from bbv.errors import ErrorCodes
from tests.util.validators import assert_error, assert_validation_error

pytestmark = pytest.mark.asyncio

TEST_KEY = "785dwAH+5piU3C8jlR878+j4QVmEdQ+1DX77JqpHuGs="


async def test_fetch_add_delete(client):
    resp = await client.get("/api/dev/user/@me/keys")
    assert resp.status_code == 200
    rjson = await resp.json
    assert isinstance(rjson["wireguard_keys"], list)

    # by default, empty
    assert not rjson["wireguard_keys"]

    resp = await client.put(
        "/api/dev/user/@me/keys",
        json={"wireguard_key": TEST_KEY, "password": client.password},
    )
    assert resp.status_code == 200
    rjson = await resp.json
    assert isinstance(rjson["wireguard_keys"], list)
    for key_object in rjson["wireguard_keys"]:
        if key_object["wireguard_public_key"] == TEST_KEY:
            assert isinstance(key_object["local_ipv4_address"], str)
            assert isinstance(key_object["local_ipv6_address"], str)
            break
    else:
        raise AssertionError("pubkey we added was not found")

    resp = await client.delete(
        "/api/dev/user/@me/keys",
        json={"wireguard_key": TEST_KEY, "password": client.password},
    )
    assert resp.status_code == 200
    rjson = await resp.json
    assert isinstance(rjson["wireguard_keys"], list)
    assert TEST_KEY not in [k["wireguard_public_key"] for k in rjson["wireguard_keys"]]


async def test_add_invalid_key(client):
    resp = await client.put(
        "/api/dev/user/@me/keys",
        json={"wireguard_key": "amogus", "password": client.password},
    )
    assert resp.status_code == 400
    rjson = await resp.json
    assert_error(rjson, ErrorCodes.INPUT_VALIDATION_FAIL)
    assert_validation_error(rjson, "wireguard_key", "value_error")
