# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only
import pytest


pytestmark = pytest.mark.asyncio


async def test_assets_fetch(client):
    group = await client.create_group()
    token = await client.create_cloud_token(group_id=group.id)
    vpn = await client.create_vpn(group_id=group.id, cloud_provider=token.provider)
    async with client.app.app_context():
        await client.app.running_vpns.on_incoming_vpn(vpn)

    resp = await client.get(
        "/api/dev/vpn_assets/bbvd.service",
        do_token=False,
        headers={"Authorization": f"VPN {vpn.id}:{vpn.auth_token}"},
    )

    assert resp.status_code == 200


async def test_assets_fetch_invalid_auth(client):
    # keep the same setup
    group = await client.create_group()
    token = await client.create_cloud_token(group_id=group.id)
    vpn = await client.create_vpn(group_id=group.id, cloud_provider=token.provider)
    async with client.app.app_context():
        await client.app.running_vpns.on_incoming_vpn(vpn)

    resp = await client.get(
        "/api/dev/vpn_assets/bbvd.service",
        do_token=False,
        headers={"Authorization": "VPN xxx:yyy"},
    )

    assert resp.status_code == 401
