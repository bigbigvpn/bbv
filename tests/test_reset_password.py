# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only
import re
import pytest

from tests.util.generators import username
from bbv.services import EmailQueue
from tests.util.mock import TestEmailState

# from tests.util.validators import assert_error
# from bbv.errors import ErrorCodes

pytestmark = pytest.mark.asyncio

EMAIL_REGEX = re.compile(r"token: '(.*)'")


async def test_reset_password(client):
    new_password = username()

    resp = await client.post(
        "/api/dev/auth/reset_password/start", json={"email": client.user.email}
    )
    assert resp.status_code == 200

    rjson = await resp.json

    email_job_id = rjson["job_id"]
    await EmailQueue.wait_job(email_job_id, timeout=10)

    # assert there's one email
    emails = TestEmailState.emails
    assert emails

    to_address, _subject, body = emails[-1]
    assert to_address == client.user.email

    match = EMAIL_REGEX.search(body)
    assert match
    token = match.group(1)
    assert token

    # trying to use this token is not allowed

    resp = await client.get(
        "/api/dev/user/@me",
        do_token=False,
        headers={"Authorization": token},
    )
    assert resp.status_code == 401

    resp = await client.post(
        "/api/dev/auth/reset_password/finish",
        json={"reset_token": token, "new_password": new_password},
    )
    assert resp.status_code == 204

    # Verify that the new password works for login
    resp = await client.post(
        "/api/dev/auth/login",
        json={
            "username": client.user.username,
            "password": new_password,
        },
        do_token=False,
    )
    assert resp.status_code == 200

    # so that other clients dont have wrong password
    client.set_test_user_password(new_password)


async def test_reset_password_incorrect_data(client):
    # TODO error codes for FailedAuth
    resp = await client.post("/api/dev/auth/reset_password/start", json={})
    assert resp.status_code == 400
    # rjson = await resp.json
    # assert_error(rjson, ErrorCodes.RESET_PASSWORD_INCORRECT_INPUT)

    resp = await client.post(
        "/api/dev/auth/reset_password/finish",
        json={"reset_token": "abcdef", "new_password": "abcabc"},
    )
    assert resp.status_code == 401
    # rjson = await resp.json
    # assert_error(rjson, ErrorCodes.RESET_PASSWORD_INVALID_TOKEN)
