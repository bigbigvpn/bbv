# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only
import pytest

from tests.util.generators import username, email
from bbv.models import User

pytestmark = pytest.mark.asyncio


async def test_register(client):
    password = username()
    resp = await client.post(
        "/api/dev/auth/register",
        json={
            "username": username(),
            "email": email(),
            "password": password,
        },
        do_token=False,
    )
    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["id"], str)
    assert isinstance(rjson["username"], str)
    assert isinstance(rjson["email"], str)
    assert isinstance(rjson["created_at"], str)

    async with client.app.app_context():
        user = await User.fetch(rjson["id"])
        assert user is not None
        client.add_resource(user)


async def test_register_bad_email(client):
    resp = await client.post(
        "/api/dev/auth/register",
        json={
            "username": username(),
            "email": username(),
            "password": username(),
        },
        do_token=False,
    )
    assert resp.status_code == 400


async def test_login(client):
    resp = await client.post(
        "/api/dev/auth/login",
        json={
            "username": client["username"],
            "password": client.password,
        },
        do_token=False,
    )
    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["token"], str)

    resp = await client.get(
        "/api/dev/user/@me",
        headers={"Authorization": rjson["token"]},
        do_token=False,
    )
    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["id"], str)
    assert isinstance(rjson["username"], str)
    assert isinstance(rjson["email"], str)
    assert isinstance(rjson["created_at"], str)


async def test_login_badpwd(client):
    resp = await client.post(
        "/api/dev/auth/login",
        json={
            "username": client["username"],
            "password": username(),
        },
        do_token=False,
    )
    assert resp.status_code == 401


async def test_login_baduser(client):
    resp = await client.post(
        "/api/dev/auth/login",
        json={
            "username": username(),
            "password": client.password,
        },
        do_token=False,
    )
    assert resp.status_code == 401
