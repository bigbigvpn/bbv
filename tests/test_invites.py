# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only
import pytest


from tests.util.generators import username
from tests.util.validators import assert_error
from bbv.errors import ErrorCodes

pytestmark = pytest.mark.asyncio


async def test_group_create_invite(client):
    group = await client.create_group(
        name=username(),
        superowner_id=client.user.id,
    )

    resp = await client.put(
        f"/api/dev/groups/{group.id}/invites",
        json={"max_uses": None, "expires_at": None},
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    invite_code = rjson["code"]
    assert isinstance(rjson["code"], str)
    assert isinstance(rjson["group_id"], str)
    assert isinstance(rjson["uses"], int)
    assert "max_uses" in rjson
    assert "expires_at" in rjson

    resp = await client.get(
        f"/api/dev/groups/{group.id}/invites",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["invites"], list)
    invitejson = rjson["invites"][0]
    assert isinstance(invitejson["code"], str)
    assert invitejson["code"] == invite_code
    assert isinstance(invitejson["group_id"], str)
    assert isinstance(invitejson["uses"], int)
    assert "max_uses" in invitejson
    assert "expires_at" in invitejson

    # test delete
    resp = await client.delete(
        f"/api/dev/groups/{group.id}/invites/{invite_code}",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["code"], str)
    assert rjson["code"] == invite_code

    # test list post delete
    resp = await client.get(
        f"/api/dev/groups/{group.id}/invites",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["invites"], list)
    # should be empty since we just created this test group
    assert not rjson["invites"]


async def test_group_invite_use(client):
    new_user = await client.create_user()
    group = await client.create_group()
    invite = await client.create_invite(group.id)
    new_user_token = await new_user.create_auth_token()

    resp = await client.post(
        f"/api/dev/invites/{invite.code}/use",
        headers={"authorization": new_user_token},
        do_token=False,
    )

    assert resp.status_code == 200
    rjson = await resp.json
    assert isinstance(rjson, dict)
    assert rjson["code"] == invite.code

    resp = await client.get(
        f"/api/dev/groups/{group.id}/members/list/0",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)

    assert isinstance(rjson["members"], list)
    for member in rjson["members"]:
        if member["user"]["id"] == str(new_user.id):
            break
    else:
        # user not found
        raise AssertionError()


async def test_unknown_invite_use(client):
    resp = await client.post(
        "/api/dev/invites/xxxyyyzzz/use",
    )

    assert resp.status_code == 404
    rjson = await resp.json
    assert_error(rjson, ErrorCodes.INVITE_NOT_FOUND)
