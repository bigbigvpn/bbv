# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
import logging
import ipaddress
from typing import List, Tuple
from datetime import datetime

import bbv.cloud
import bbv.services.mailer
from bbv.cloud.types import CloudRegion, CloudVPN

log = logging.getLogger(__name__)


class TestEmailState:
    emails: List[Tuple[str, str, str]] = []


def new_raw_send(_smtp, _cfg, to, subject, content):
    log.info("Sent mock email to=%r subject=%r content=%r", to, subject, content)
    TestEmailState.emails.append((to, subject, content))


bbv.services.mailer.raw_send_email = new_raw_send
bbv.services.mailer.raw_connect_smtp = lambda cfg: 1


class MockHetznerCloud:
    @staticmethod
    async def fetch_regions(_token: str):
        return [
            CloudRegion("BR", "br-sp-1"),
            CloudRegion("DE", "fsn1"),
        ]

    @staticmethod
    async def summon(
        token: str, cloud_region_id: str, hostname: str, cloud_init: str
    ) -> CloudVPN:
        regions = await MockHetznerCloud.fetch_regions(token)
        assert cloud_region_id in [r.cloud_internal_code for r in regions]
        await asyncio.sleep(0.5)
        return CloudVPN(
            server_cloud_id="dickbutt",
            ipv4_network=ipaddress.IPv4Network("1.1.1.1"),
            ipv6_network=ipaddress.IPv6Network("2606:4700:3032::6815:20d3"),
            created_at=datetime.utcnow(),
        )

    @staticmethod
    async def release(token: str, cloud_region_id: str, cloud_vpn_id: str) -> None:
        return


def get_mock_cloud_impl(provider):
    return MockHetznerCloud


bbv.cloud._actual_get_provider = get_mock_cloud_impl
