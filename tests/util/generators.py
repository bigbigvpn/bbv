# pawbs: powerdns ui for nyan of your business
# Copyright 2020, Nyan Of Your Business and the pawbs contributors
# SPDX-License-Identifier: AGPL-3.0-only

import secrets
import random

__all__ = [
    "token",
    "hexs",
    "username",
    "email",
]


def token() -> str:
    return secrets.token_urlsafe(random.randint(100, 300))


def hexs(len: int = 5) -> str:
    return secrets.token_hex(len)


def username() -> str:
    return hexs(6)


def email() -> str:
    name = hexs()
    return f"{name}@discordapp.io"


def zone() -> str:
    # zone tld is .local because .local is rfc
    return f"{hexs()}.local"
