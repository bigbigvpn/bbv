# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only


def assert_error(rjson, wanted_error_code):
    assert rjson["error"]
    assert rjson["error_code"] == wanted_error_code.value
    assert "message" in rjson


def assert_validation_error(rjson, field: str, error_type: str):
    errors = rjson["validation_errors"]
    found_error = False
    for error in errors:
        if field in error["location"] and error["type"] == error_type:
            found_error = True

    if not found_error:
        raise AssertionError(f"Field {field} did not have errors")
