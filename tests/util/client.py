# pawbs: powerdns ui for nyan of your business
# Copyright 2020, Nyan Of Your Business and the pawbs contributors
# SPDX-License-Identifier: AGPL-3.0-only

import secrets
from typing import Optional
from uuid import UUID, uuid4
from datetime import datetime
from ipaddress import IPv4Network

from bbv.models import User, Group, Invite, VPN, CloudToken
from bbv.auth import hash_password
from bbv.cloud import CloudProviderType
from bbv.cloud.types import CloudVPN
from tests.util.generators import hexs, email

__all__ = ["TestClient"]


def _wrapped_method(method: str):
    async def _method(self, *args, **kwargs):
        kwargs["headers"] = self._inject_auth(kwargs)
        verb = getattr(self.cli, method)
        return await verb(*args, **kwargs)

    _method.__name__ = method
    return _method


class TestClient:
    """Test client that wraps quart's TestClient and a test
    user and adds authorization headers to test requests."""

    def __init__(self, test_cli, test_userdata):
        self.cli = test_cli
        self.app = test_cli.app
        self._resources = []

        self._test_userdata = test_userdata

        self.user = test_userdata.user
        self.password = test_userdata.password
        self.token = test_userdata.token

    def __getitem__(self, key):
        return getattr(self.user, key)

    def _inject_auth(self, kwargs: dict) -> list:
        """Inject the test user's API key into the test request before
        passing the request on to the underlying TestClient."""
        headers = kwargs.get("headers", {})

        do_token = kwargs.get("do_token", True)

        try:
            kwargs.pop("do_token")
        except KeyError:
            pass

        if not do_token:
            return headers

        headers["authorization"] = self.token
        return headers

    get = _wrapped_method("get")
    post = _wrapped_method("post")
    put = _wrapped_method("put")
    patch = _wrapped_method("patch")
    delete = _wrapped_method("delete")
    head = _wrapped_method("head")

    def websocket(self, *args, **kwargs):
        return self.cli.websocket(*args, **kwargs)

    async def _create_resource(self, _classmethod, *args, **kwargs):
        async with self.app.app_context():
            resource = await _classmethod(*args, **kwargs)

        self._resources.append(resource)
        return resource

    def add_resource(self, resource) -> None:
        self._resources.append(resource)

    async def _reset_user(self):
        async with self.app.app_context():

            # next test clients will get a session-scoped test_userdata
            # instance. we modify it with a refreshed user

            self._test_userdata.user = await User.fetch(self.user.id)
            self._test_userdata.token = (
                await self._test_userdata.user.create_auth_token()
            )

    def set_test_user_password(self, new_password: str):
        self._test_userdata.password = new_password

    async def create_user(
        self,
        *,
        username: Optional[str] = None,
        password: Optional[str] = None,
        user_email: Optional[str] = None,
    ) -> User:
        username = username or hexs(6)
        password = password or hexs(6)
        user_email = user_email or email()

        async with self.app.app_context():
            password_hash = await hash_password(password)

            user_data = await User.create(
                username=username, email=user_email, password_hash=password_hash
            )
            user = await User.fetch(user_id=user_data.id)
            assert user is not None
            self.add_resource(user)

        return user

    async def create_group(
        self,
        *,
        name: Optional[str] = None,
        superowner_id: Optional[UUID] = None,
    ) -> Group:
        name = name or hexs(6)
        superowner_id = superowner_id or self.user.id

        async with self.app.app_context():
            group = await Group.create(name, superowner_id)
            assert group is not None
            self.add_resource(group)

        return group

    async def create_cloud_token(
        self,
        *,
        group_id: UUID,
        provider: CloudProviderType = CloudProviderType.HETZNER,
        token: str = "abcdef",
    ) -> CloudToken:
        async with self.app.app_context():
            token = await CloudToken.create(group_id, provider=provider, token=token)
            assert token is not None
            self.add_resource(token)

        return token

    async def create_vpn(
        self,
        *,
        group_id: UUID,
        vpn_id: Optional[UUID] = None,
        cloud_provider: CloudProviderType = CloudProviderType.HETZNER,
        cloud_region: str = "test_region",
        summoned_vpn_info: Optional[CloudVPN] = None,
        hostname="test_hostname",
        summoner_id: Optional[UUID] = None,
        vpn_token: Optional[str] = None,
        wireguard_private_key: str = "mock_privkey",
        wireguard_public_key: str = "mock_pubkey",
        wireguard_port: int = 53,
    ) -> VPN:
        summoner_id = summoner_id or self.user.id
        vpn_id = vpn_id or uuid4()
        summoned_vpn_info = summoned_vpn_info or CloudVPN(
            server_cloud_id="test_id",
            ipv4_network=IPv4Network("10.0.0.1/32"),
            ipv6_network=None,
            created_at=datetime.utcnow(),
        )
        vpn_token = vpn_token or secrets.token_urlsafe(32)

        async with self.app.app_context():
            vpn = await VPN.create(
                vpn_id,
                group_id,
                cloud_provider,
                cloud_region,
                summoned_vpn_info,
                hostname,
                summoner_id,
                vpn_token,
                wireguard_private_key,
                wireguard_public_key,
                wireguard_port,
            )
            assert vpn is not None

            # VPNs are not marked with add_resource, this is deliberate.
            #
            # Groups, when deleted, already delete their declared VPNs.
            #
            # attempting to register the vpn resource would lead to
            # double delete of the resource at cleanup()

        return vpn

    async def create_invite(
        self,
        group_id: UUID,
        *,
        max_uses: Optional[int] = None,
        expires_at: Optional[datetime] = None,
    ) -> User:
        async with self.app.app_context():
            invite = await Invite.create(
                group_id, max_uses=max_uses, expires_at=expires_at
            )
            assert invite is not None
            self.add_resource(invite)

        return invite

    async def cleanup(self):
        """Delete all allocated test resources.

        Resets the test user to a good initial state.
        """
        if self.user is not None:
            await self._reset_user()

        for resource in self._resources:
            async with self.app.app_context():
                await resource.delete()
