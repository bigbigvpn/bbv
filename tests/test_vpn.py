# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
import pytest

from bbv.models import CloudToken
from bbv.cloud import CloudProviderType
from bbv.errors import ErrorCodes
from tests.util.validators import assert_error

pytestmark = pytest.mark.asyncio


async def test_fetch_regions(client):
    group = await client.create_group()
    async with client.app.app_context():
        await CloudToken.create(
            group.id, provider=CloudProviderType.HETZNER, token="test"
        )

    resp = await client.get(
        f"/api/dev/groups/{group.id}/cloud_tokens",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    cloud_tokens = rjson["cloud_tokens"]
    assert isinstance(cloud_tokens, list)
    assert cloud_tokens

    resp = await client.get(f"/api/dev/groups/{group.id}/regions/BR")
    assert resp.status_code == 200
    rjson = await resp.json
    assert isinstance(rjson, dict)
    regions = rjson["regions"]
    assert isinstance(regions, list)
    assert regions
    region = regions[0]
    assert isinstance(region, dict)
    assert isinstance(region["cloud_provider"], str)
    assert isinstance(region["iso_country_code"], str)
    assert isinstance(region["cloud_region"], str)
    assert region["iso_country_code"] == "BR"


async def test_summon_vpn(client):
    group = await client.create_group()
    async with client.app.app_context():
        await CloudToken.create(
            group.id, provider=CloudProviderType.HETZNER, token="test"
        )

    resp = await client.get(f"/api/dev/groups/{group.id}/regions/BR")
    assert resp.status_code == 200
    rjson = await resp.json
    assert isinstance(rjson, dict)
    regions = rjson["regions"]
    assert isinstance(regions, list)
    assert regions
    region = regions[0]
    assert isinstance(region, dict)
    assert isinstance(region["cloud_provider"], str)
    assert isinstance(region["iso_country_code"], str)
    assert isinstance(region["cloud_region"], str)
    assert region["iso_country_code"] == "BR"

    region_internal_code = region["cloud_region"]

    # we try to spawn two at the same time in the same region,
    # only one of them should succeed.
    resp_coro1 = client.post(
        f"/api/dev/groups/{group.id}/servers",
        json={
            "cloud_provider": "hetzner",
            "cloud_region": region_internal_code,
            "wireguard_port": 53,
        },
    )
    resp_coro2 = client.post(
        f"/api/dev/groups/{group.id}/servers",
        json={
            "cloud_provider": "hetzner",
            "cloud_region": region_internal_code,
            "wireguard_port": 53,
        },
    )

    resp1, resp2 = await asyncio.gather(resp_coro1, resp_coro2)
    if resp1.status_code == 200:
        resp = resp1
        resp_failed = resp2
    elif resp2.status_code == 200:
        resp = resp2
        resp_failed = resp1
    else:
        raise AssertionError("Neither response succeeded")

    assert resp_failed.status_code == 400
    rjson_failed = await resp_failed.json
    assert_error(rjson_failed, ErrorCodes.VPN_RESOURCE_LOCKED)

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert rjson["vpn"]["cloud_region"] == region_internal_code
    assert rjson["vpn"]["hostname"]
    assert isinstance(rjson["vpn"]["local_ipv4_address"], str)
    assert isinstance(rjson["vpn"]["local_ipv6_address"], str)

    # test list works
    resp = await client.get(
        f"/api/dev/groups/{group.id}/servers",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson["summoned_vpns"], list)
    vpn = rjson["summoned_vpns"][0]
    assert isinstance(vpn, dict)
    vpn_id = vpn["id"]
    assert vpn["cloud_region"] == region_internal_code
    assert vpn["hostname"]

    # test single fetch works
    resp = await client.get(
        f"/api/dev/groups/{group.id}/servers/{vpn_id}",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson["vpn"], dict)
    assert rjson["vpn"]["id"] == vpn["id"]

    # test release works
    resp = await client.delete(
        f"/api/dev/groups/{group.id}/servers/{vpn_id}",
    )
    assert resp.status_code == 200

    rjson = await resp.json
    assert isinstance(rjson, dict)
    released_vpn = rjson["released_vpn"]
    assert isinstance(released_vpn, dict)
    assert released_vpn["id"] == vpn["id"]

    # test list works after releasing
    resp = await client.get(
        f"/api/dev/groups/{group.id}/servers",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson["summoned_vpns"], list)
    assert not rjson["summoned_vpns"]
