# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only
import pytest

from tests.util.generators import username
from tests.util.validators import assert_error
from bbv.errors import ErrorCodes

pytestmark = pytest.mark.asyncio


async def test_group_fetch_tokens(client):
    group = await client.create_group()

    resp = await client.get(
        f"/api/dev/groups/{group.id}/cloud_tokens",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    cloud_tokens = rjson["cloud_tokens"]
    assert isinstance(cloud_tokens, list)


def assert_token_exists(rjson, cloud_provider: str, value: str):
    for token in rjson["cloud_tokens"]:
        if token["provider"] == cloud_provider:
            assert token["value"] == value
            break
    else:
        raise AssertionError(f"no {cloud_provider!r} token found")


def assert_token_not_exists(rjson, cloud_provider: str):
    for token in rjson["cloud_tokens"]:
        if token["provider"] == cloud_provider:
            raise AssertionError(f"{cloud_provider!r} token found")


async def test_group_patch_tokens(client):
    group = await client.create_group()

    # test with random token value
    # TODO: can we validate cloud tokens per provider? (as in, validate that
    # a given hetzner token looks like a hetzner token)
    token_value = username()
    resp = await client.put(
        f"/api/dev/groups/{group.id}/cloud_tokens/digitalocean",
        json={"value": token_value},
    )
    assert resp.status_code == 200
    rjson = await resp.json
    assert_token_exists(rjson, "digitalocean", token_value)

    # test the newly set token appears on full fetch as well
    resp = await client.get(
        f"/api/dev/groups/{group.id}/cloud_tokens",
    )
    assert resp.status_code == 200
    rjson = await resp.json
    assert_token_exists(rjson, "digitalocean", token_value)

    # test removing it works
    resp = await client.delete(
        f"/api/dev/groups/{group.id}/cloud_tokens/digitalocean",
    )
    assert resp.status_code == 200
    rjson = await resp.json
    assert_token_not_exists(rjson, "digitalocean")

    # test newly removed token also doesnt appear when fetching
    resp = await client.get(
        f"/api/dev/groups/{group.id}/cloud_tokens",
    )
    assert resp.status_code == 200
    rjson = await resp.json
    assert_token_not_exists(rjson, "digitalocean")


async def test_invalid_cloud_provider(client):
    group = await client.create_group()

    # test invalid cloud provider does not work
    resp = await client.delete(
        f"/api/dev/groups/{group.id}/cloud_tokens/xxxxxxxx",
    )
    assert resp.status_code == 400
    rjson = await resp.json
    assert_error(rjson, ErrorCodes.INVALID_CLOUD_PROVIDER)
