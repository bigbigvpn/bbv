# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import json

import pytest

from bbv.enums.messages import OperationType, CloseCodes
from quart.testing.connections import WebsocketDisconnectError


pytestmark = pytest.mark.asyncio


async def _setup_vpn(client):
    group = await client.create_group()
    token = await client.create_cloud_token(group_id=group.id)
    vpn = await client.create_vpn(group_id=group.id, cloud_provider=token.provider)
    async with client.app.app_context():
        await client.app.running_vpns.on_incoming_vpn(vpn)

    return group, token, vpn


async def _recv_json(ws):
    frame = await ws.receive()
    return json.loads(frame)


async def _send_json(ws, data):
    frame = json.dumps(data)
    await ws.send(frame)


async def test_control(client):
    """Test if a conncetion can be done to the control websocket."""
    group, token, vpn = await _setup_vpn(client)
    async with client.websocket("/api/dev/control/ws") as ws:
        await _send_json(
            ws,
            {
                "op": OperationType.LOGIN.value,
                "d": {"id": str(vpn.id), "token": str(vpn.auth_token)},
            },
        )

        welcome = await _recv_json(ws)
        assert welcome["op"] == OperationType.WELCOME.value


async def test_control_invalid_login(client):
    group, token, vpn = await _setup_vpn(client)
    async with client.websocket("/api/dev/control/ws") as ws:
        await _send_json(
            ws,
            {
                "op": OperationType.LOGIN.value,
                "d": {"id": str(vpn.id), "token": "abcdef"},
            },
        )

        try:
            _ = await _recv_json(ws)
            raise AssertionError("websocket did not disconnect on invalid token")
        except WebsocketDisconnectError as exc:
            assert exc.args[0] == CloseCodes.FAILED_AUTH
