# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only
import pytest


from tests.util.generators import username
from bbv.models import Group

pytestmark = pytest.mark.asyncio


async def test_groups(client):
    resp = await client.post(
        "/api/dev/groups",
        json={"name": username()},
    )
    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["id"], str)
    assert isinstance(rjson["created_at"], str)
    assert isinstance(rjson["superowner_id"], str)
    assert isinstance(rjson["name"], str)

    async with client.app.app_context():
        group = await Group.fetch(rjson["id"])
        assert group is not None
        client.add_resource(group)

    # test single fetch

    resp = await client.get(f"/api/dev/groups/{group.id}")
    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["id"], str)
    assert rjson["id"] == str(group.id)
    assert isinstance(rjson["created_at"], str)
    assert isinstance(rjson["superowner_id"], str)
    assert isinstance(rjson["name"], str)

    # test list

    resp = await client.get("/api/dev/groups/list")
    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["groups"], list)
    groupjson = rjson["groups"][0]
    assert isinstance(groupjson, dict)
    assert isinstance(groupjson["id"], str)
    assert groupjson["id"] == str(group.id)
    assert isinstance(groupjson["created_at"], str)
    assert isinstance(groupjson["superowner_id"], str)
    assert isinstance(groupjson["name"], str)

    # test patch

    new_name = username()
    resp = await client.patch(f"/api/dev/groups/{group.id}", json={"name": new_name})
    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert rjson["name"] == new_name

    async with client.app.app_context():
        group = await Group.fetch(rjson["id"])
        assert group is not None
        assert group.name == new_name


async def test_group_delete(client):
    group = await client.create_group(
        name=username(),
        superowner_id=client.user.id,
    )
    resp = await client.delete(
        f"/api/dev/groups/{group.id}",
        json={"password": client.password},
    )
    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["id"], str)
    assert isinstance(rjson["created_at"], str)
    assert isinstance(rjson["superowner_id"], str)
    assert isinstance(rjson["name"], str)

    async with client.app.app_context():
        group = await Group.fetch(rjson["id"])
        assert group is None
