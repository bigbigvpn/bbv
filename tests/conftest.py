# pawbs: powerdns ui for nyan of your business
# Copyright 2020, Nyan Of Your Business and the pawbs contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
import os
import sys
from dataclasses import dataclass

import pytest

sys.path.append(os.getcwd())
from bbv import app as app_  # noqa: E402
from bbv.models import User  # noqa: E402
from bbv.auth import hash_password  # noqa: E402
from tests.util.client import TestClient  # noqa: E402
from tests.util.generators import hexs, username, email  # noqa: E402

# the mock module currently only has email. that is monkey patched in
# the email module. we just import, but don't use
import tests.util.mock  # noqa: F401, F402, E402


@pytest.fixture(name="event_loop", scope="session")
def event_loop_fixture():
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(name="app", scope="session")
async def app_fixture(event_loop):
    app_.loop = event_loop
    await app_.startup()
    yield app_
    async with app_.app_context():
        await app_.shutdown()


@pytest.fixture(name="test_cli")
def test_cli_fixture(app):
    return app.test_client()


@dataclass
class TestUserData:
    user: User
    password: str
    token: str


async def _user_fixture_setup():
    password = hexs(6)

    user = await User.create(
        username=username(),
        email=email(),
        password_hash=await hash_password(password),
    )

    token = await user.create_auth_token()
    return TestUserData(user, password, token)


async def _user_fixture_teardown(userdata: TestUserData):
    await userdata.user.delete()


@pytest.fixture(name="test_user", scope="session")
async def test_user_fixture(app):
    """Yield a randomly generated test user.

    As an optimization, the test user is set to be in session scope,
    the test client's cleanup() method then proceeds to reset the test user
    back to a wanted initial state, which is faster than creating/destroying
    the user on every single test.
    """
    async with app.app_context():
        userdata = await _user_fixture_setup()

    yield userdata

    async with app.app_context():
        await _user_fixture_teardown(userdata)


@pytest.fixture(name="client")
async def test_cli_user(test_cli, test_user):
    """Yield a TestClient instance that contains a randomly generated
    user."""
    client = TestClient(test_cli, test_user)
    yield client
    await client.cleanup()
