# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only
import re
import pytest

from tests.util.generators import username
from bbv.services import EmailQueue
from tests.util.mock import TestEmailState


pytestmark = pytest.mark.asyncio

EMAIL_REGEX = re.compile(r"token: '(.*)'")


async def test_delete_own_user(client):
    user_password = username()
    user = await client.create_user(password=user_password)

    resp = await client.post(
        "/api/dev/auth/login",
        json={
            "username": user.username,
            "password": user_password,
        },
        do_token=False,
    )
    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["token"], str)

    token = rjson["token"]

    resp = await client.delete(
        "/api/dev/user/@me/start",
        do_token=False,
        headers={"authorization": token},
    )

    assert resp.status_code == 200

    rjson = await resp.json

    email_job_id = rjson["job_id"]
    await EmailQueue.wait_job(email_job_id, timeout=10)

    # assert there's one email
    emails = TestEmailState.emails
    assert emails

    to_address, _subject, body = emails[-1]
    assert to_address == user.email

    match = EMAIL_REGEX.search(body)
    assert match
    token = match.group(1)
    assert token

    resp = await client.delete(
        "/api/dev/user/@me/finish",
        json={"delete_token": token},
    )
    assert resp.status_code == 204

    # Verify that account cannot be logged into
    resp = await client.post(
        "/api/dev/auth/login",
        json={
            "username": user.username,
            "password": user_password,
        },
        do_token=False,
    )
    assert resp.status_code == 401
