# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import uuid
import pytest

from tests.util.validators import assert_error
from bbv.errors import ErrorCodes

pytestmark = pytest.mark.asyncio


async def test_member_list(client):
    group = await client.create_group()
    resp = await client.get(f"/api/dev/groups/{group.id}/members/list/0")

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    members = rjson["members"]
    assert isinstance(rjson["members"], list)
    assert len(members) == 1

    first_member = members[0]
    assert first_member["user"]["id"] == str(client.user.id)


async def test_member_add(client):
    user = await client.create_user()
    group = await client.create_group()
    async with client.app.app_context():
        await group.add_member(user.id)

    resp = await client.get(
        f"/api/dev/groups/{group.id}/members/list/0",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    members = rjson["members"]
    assert isinstance(rjson["members"], list)
    assert len(members) == 2

    found_superowner = False
    found_new_user = False

    for member in members:
        member_id = member["user"]["id"]
        if member_id == str(client.user.id):
            found_superowner = True
        elif member_id == str(user.id):
            # new member, no perms
            assert not member["permissions"]
            found_new_user = True

    assert found_superowner and found_new_user


async def test_member_edit(client):
    user = await client.create_user()
    group = await client.create_group()
    async with client.app.app_context():
        await group.add_member(user.id)

    resp = await client.patch(
        f"/api/dev/groups/{group.id}/members/{user.id}", json={"permissions": ["owner"]}
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert "owner" in rjson["permissions"]

    # find wanted user in member list with new permission
    resp = await client.get(
        f"/api/dev/groups/{group.id}/members/list/0",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    members = rjson["members"]
    assert isinstance(members, list)
    for member in members:
        if member["user"]["id"] == str(user.id):
            assert "owner" in member["permissions"]
            break
    else:
        raise AssertionError(f"user not found {members!r}")


async def test_member_unknown_edit(client):
    group = await client.create_group()

    # test with a random id to get notfound
    random_id = uuid.uuid4()
    # i hope this assert isn't triggered
    assert random_id != client.user.id

    resp = await client.patch(
        f"/api/dev/groups/{group.id}/members/{random_id}",
        json={"permissions": []},
    )
    assert resp.status_code == 404
    rjson = await resp.json
    assert_error(rjson, ErrorCodes.MEMBER_NOT_FOUND)


async def test_member_delete(client):
    user = await client.create_user()
    group = await client.create_group()
    async with client.app.app_context():
        await group.add_member(user.id)

    resp = await client.delete(f"/api/dev/groups/{group.id}/members/{user.id}")

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert rjson["user"]["id"] == str(user.id)

    # test user cant be listed anymore
    resp = await client.get(
        f"/api/dev/groups/{group.id}/members/list/0",
    )

    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    members = rjson["members"]
    assert isinstance(members, list)
    for member in members:
        if member["user"]["id"] == str(user.id):
            raise AssertionError("deleted member was found on list")

    resp = await client.get(
        "/api/dev/groups/list",
        headers={"authorization": await user.create_auth_token()},
        do_token=False,
    )
    assert resp.status_code == 200
    rjson = await resp.json

    assert isinstance(rjson, dict)
    assert isinstance(rjson["groups"], list)
    for group in rjson["groups"]:
        if group["id"] == str(group.id):
            raise AssertionError("deleted member can still access group in list")
