-- START BBV SCHEMA

CREATE TABLE IF NOT EXISTS bbv_users (
    id UUID PRIMARY KEY,
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() AT TIME ZONE 'utc'),
    username TEXT UNIQUE,
    email TEXT UNIQUE,
    password_hash TEXT, --argon 2 babey
    CHECK (username = lower(username)) -- keep them case insensitive on db
);

CREATE TABLE IF NOT EXISTS bbv_queue_email (
    job_id UUID PRIMARY KEY,
    name TEXT UNIQUE,

    state BIGINT DEFAULT 0,
    errors TEXT DEFAULT '',
    inserted_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() AT TIME ZONE 'utc'),
    scheduled_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() AT TIME ZONE 'utc'),
    taken_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
    internal_state JSONB DEFAULT '{}',

    to_address TEXT NOT NULL,
    subject TEXT NOT NULL,
    body TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS bbv_groups (
    id UUID PRIMARY KEY,
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() AT TIME ZONE 'utc'),
    name TEXT NOT NULL,
    superowner_id UUID REFERENCES bbv_users (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS bbv_group_members (
    group_id UUID REFERENCES bbv_groups (id) ON DELETE CASCADE NOT NULL,
    user_id UUID REFERENCES bbv_users (id) ON DELETE CASCADE NOT NULL,
    PRIMARY KEY (group_id, user_id),
    permissions text[] NOT NULL DEFAULT '{}'
);

CREATE TABLE IF NOT EXISTS bbv_group_invites (
    code TEXT PRIMARY KEY,
    group_id UUID REFERENCES bbv_groups (id) ON DELETE CASCADE NOT NULL,
    uses INT NOT NULL DEFAULT 0,
    max_uses INT DEFAULT NULL,
    expires_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL
);


CREATE TABLE IF NOT EXISTS bbv_group_cloud_tokens (
    group_id UUID REFERENCES bbv_groups (id) ON DELETE CASCADE NOT NULL,
    provider TEXT NOT NULL,
    PRIMARY KEY (provider, group_id),
    value TEXT DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS bbv_user_wireguard_keys (
    user_id UUID REFERENCES bbv_users (id) ON DELETE CASCADE NOT NULL,
    wireguard_public_key TEXT,
    local_ipv4_address INET UNIQUE NOT NULL,
    local_ipv6_address INET UNIQUE NOT NULL,
    PRIMARY KEY (user_id, wireguard_public_key)
);

-- holds all running vpns in the system
CREATE TABLE IF NOT EXISTS bbv_vpns (
    id UUID PRIMARY KEY,
    group_id UUID REFERENCES bbv_groups (id) ON DELETE CASCADE NOT NULL,

    -- definitions given by user when summoning
    cloud_provider TEXT NOT NULL,
    cloud_region TEXT NOT NULL,

    UNIQUE (group_id, cloud_region),

    -- data given by cloud provider
    cloud_id TEXT NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    ipv4_network CIDR NOT NULL,
    ipv6_network CIDR DEFAULT NULL,

    -- given by us when summoning
    hostname TEXT NOT NULL,
    summoner_id UUID REFERENCES bbv_users (id) ON DELETE CASCADE NOT NULL,

    -- generated by us when summoning
    auth_token TEXT NOT NULL,
    state TEXT NOT NULL,
    local_ipv4_address INET NOT NULL,
    local_ipv6_address INET NOT NULL,

    -- TODO talk with ave about key material
    wireguard_private_key TEXT NOT NULL,
    wireguard_public_key TEXT NOT NULL,
    -- While ports are unsigned short, we can't use smallint
    -- as it is equivalent to a signed short
    wireguard_port INTEGER NOT NULL DEFAULT 53
);

-- END BBV SCHEMA
