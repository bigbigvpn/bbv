#!/bin/sh

# spawn download commands in parallel
curl "{{ asset_url }}/wg-quick" --output /usr/bin/wg-quick &
curl "{{ asset_url }}/wg" --output /usr/bin/wg &
curl "{{ asset_url }}/bbvd" --output /usr/bin/bbvd &
curl -H "Authorization: VPN {{ vpn_api_credentials }}" "{{ asset_url }}/bbvd.service" --output /lib/systemd/system/bbvd.service &

# wait for all spawned jobs to finish
wait $(jobs -p)

# setup binary permissions for the things we just downloaded
chmod +x /usr/bin/wg /usr/bin/wg-quick /usr/bin/bbvd

# wireguard won't make this folder by itself
mkdir -p /etc/wireguard

# by default, we set wireguard on 53, as its udp and DNS is also udp, which
# can make it easier to pass firewalls. systemd-resolved conflicts with that.
systemctl disable --now systemd-resolved

# put our own nameservers instead of the cloud providers'
rm -f /etc/resolv.conf
echo "nameserver 95.217.25.217" > /etc/resolv.conf

# important system properties for wireguard to work
echo "net.ipv4.ip_forward=1" > /etc/sysctl.d/10-bbv.conf
echo "net.ipv6.conf.all.forwarding=1" >> /etc/sysctl.d/10-bbv.conf
sysctl -p /etc/sysctl.d/10-bbv.conf

# TODO shadowsocks support
{% if deploy_type == 'shadowsocks' %}
wget https://github.com/shadowsocks/shadowsocks-rust/releases/download/v1.10.0/shadowsocks-v1.10.0.x86_64-unknown-linux-gnu.tar.xz -O /root/ss/shadowsocks.tar.xz
cd /root/ss && tar xf ./shadowsocks.tar.xz
{% endif %}

# boot bbvd
systemctl enable --now bbvd.service
