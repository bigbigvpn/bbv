# Contribution handbook

Thank you for your interest in contributing to bigbigvpn!

This document outlines the architecture of the project and some technical
decisions on it.

## System architecture

There are three separate computers running different components of BBV:

- The client, running either [bbv-cli](bbv-cli) or some other client that communicates via the BBV API
- The BBV server, running this source, providing the BBV API.
- The VPN server, running `bbvd`, that provides "command-and-control" style orchestration from the BBV server.
  - The VPN server does not permanently exist, it is created by the BBV server
    when the client asks for it.

[bbv-cli]: https://gitlab.com/bigbigvpn/bbv-cli

### BBV server

This is an application written in Python using the
[Quart](https://pgjones.gitlab.io/quart/) web framework. This framework was
chosen because of its proximity with the Flask web framework, providing async as
a first class citizen, and we have experience with writing it from other projects.

Passwords for accounts are stored with the Argon2 password hash, which also
provides salting.

### High level business flow

- Users can register to the BBV server and get an account.
- An account can hold many WireGuard® public keys. Each key gets assigned an
  IPv4 and IPv6 address pair that lives in the _private ranges_ of both
  address spaces.
- Accounts can create groups, or be invited to an existing group by an admin of
  such group.
- Groups can hold one "cloud token" which is an authentication token used to
  communicate with a cloud provider's API to create/destroy VPS'es.
- When an account wishes to summon a VPN, it does so via the BBV server API.
- The BBV server uses the cloud token to create a VPS and loads a cloud-init
  file containing the steps the newly created VPS should do to become a VPN.
  - The actual creation in the respective cloud providers' API can be seen
    in the `bbv/cloud/` folder tree. The code inside bbv MUST be generic
    across any cloud provider when this code lives outside of the
    `bbv/cloud` subpackage.
    - Implementations of such APIs must follow the CloudProvider interface,
      described in `bbv/cloud/provider.py`
- That script installs statically-linked builds of wireguard (courtesy of the
  mirrorbuilds project), sets relevant system configurations,
  downloads and runs bbvd.
- bbvd is a long-lived daemon that runs in the VPS and contains logic to,
  for example, get all the WireGuard® public keys, and its own WireGuard®
  keypair so it can generate a configuration file and reload it in
  WireGuard.
  - bbvd is written in the [Zig programming language](https://ziglang.org).
  - bbvd is statically linked with musl and mbedtls, so that we can have a
    production-ready TLS implementation.
    - [iguanaTLS](https://github.com/alexnask/iguanaTLS)
      was considered but it is not ready for the sensitive applications of BBV.
- We take this approach so that state changes that happen in the BBV server,
  such as adding or removing a member, or a user adding a new WireGuard®
  public key, can be synchronized back to all the running VPNs via bbvd.
  - The communication between the server and bbvd is done via a websocket,
    its implementation can be found in `bbv/bp/control.py`
