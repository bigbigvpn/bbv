# bbv

Server component for bigbigvpn

## setup

- wireguard must be installed in the system for key generation.
- postgresql (9.6 minimum)
- python (3.9+)
  - [poetry](https://python-poetry.org/)
- Download a statically linked "wg" and "wg-quick" to assets directory. These can be obtained from [artifacts of this project](https://gitlab.com/mirrorbuilds/wireguard-tools/-/jobs) (rename `linux.bash` to `wg-quick`).

### postgresql

```
postgres=# create user bbv with password '123';
CREATE ROLE
postgres=# create database bbv;
CREATE DATABASE
postgres=# grant all privileges on database bbv to bbv;
GRANT
```

### python

```
git clone ...
cd ...

# Install dependencies to a virtualenv with poetry
# ( https://python-poetry.org/ )
poetry install

# Enable poetry virtualenv
# you can also use 'poetry run' for cases like running bbv inside a
# systemd service.
poetry shell

# specifics of creating the database and user for it are deploy-specific.
cp config.example.toml config.toml

# arguments may be different to load the schema
psql -U bbv -f schema.sql

# prepare database for future migrations
./agnwrapper.py bootstrap

# run the webapp
hypercorn bbv --access-log - --bind 0.0.0.0:6900
```

## setup bbvd (bbv daemon)

the daemon runs on every running vpn and is used to control it (shutdown,
add users, remove users, etc)

requirements:

- zig master, https://ziglang.org/download/
- zigmod, https://github.com/nektro/zigmod

### building bbvd for development

```
cd bbvd
zigmod fetch
zig build
```

### linking bbvd as a bbv vpn asset ready for production

```
cd bbvd
zig build -Drelease-safe=true
cp zig-cache/bin/bbvd ../assets/
```

## development

### Creating a database migration

**You will both need to create the migration and update `schema.sql` to reflect
your changes.**

The need to keep the `schema.sql` file instead of making it a
migration itself is for ease of development. It is easier for any developer to
peek at the `schema.sql` file and have a full view of the database, instead of
having to go through N migrations to find out the final state of a table.

```
./agnwrapper.py create_migration name_for_migration_goes_here
```

### Running the unit tests

It is recommended to run the test suite on every change. CI is setup to do so
as well to guarantee the system is somewhat correct before deployment.

The tool we use to run the suite is [tox](https://tox.readthedocs.io/en/latest/),
which then runs things like formatting, linting and `pytest`.

```
tox
```
