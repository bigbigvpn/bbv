# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import asyncio
from typing import Tuple, Union

import asyncpg
import quart
from quart import jsonify
from quart_schema import (
    RequestSchemaValidationError,
    ResponseSchemaValidationError,
)
from pydantic import ValidationError
from tomlkit import parse
from argon2 import PasswordHasher
from violet import JobManager

from .errors import APIError, ErrorCodes, InputError, ServerError
from .utils import toml_to_native

from .bp import (
    index,
    auth,
    user,
    group,
    invite,
    member,
    cloud_token,
    vpn,
    control,
    assets,
)
from .services.mailer import EmailQueue
from .locks import LockManager
from .running_vpns import RunningVPNManager
from .api_docs import ApiDocs
from .templates import TemplateManager


log = logging.getLogger(__name__)
ROOT_PREFIXES = ("/api/dev",)


def create_app() -> quart.Quart:
    app = quart.Quart(__name__)
    with open("./config.toml", "r") as config_file:
        app.cfg = parse(config_file.read())

    logging.basicConfig(level="INFO")

    ApiDocs(app)
    return app


def setup_blueprints(app: quart.Quart) -> None:
    # use None to load the blueprint under /
    # use an empty string to load the blueprint under /api
    # use a non-empty string to load the blueprint under /api<your string>
    blueprint_list = [
        (index, None),
        # (cors, ""),
        (auth, "/auth"),
        (user, "/user"),  # TODO: move to /users
        (group, "/groups"),
        (invite, "/"),
        (member, "/"),
        (cloud_token, "/groups"),
        (vpn, "/groups"),
        (control, "/control"),
        (assets, "/vpn_assets"),
        # (password, "/auth"),
        # (profile, "/profile"),
    ]

    for blueprint, api_prefix in blueprint_list:
        route_prefixes = [f'{root}{api_prefix or ""}' for root in ROOT_PREFIXES]

        if api_prefix is None:
            route_prefixes = [""]

        log.debug(
            "loading blueprint %r with prefixes %r", blueprint.name, route_prefixes
        )
        for route in route_prefixes:
            app.register_blueprint(blueprint, url_prefix=route)


app = create_app()


@app.before_serving
async def app_before_serving():
    app.loop = asyncio.get_event_loop()

    log.info("connecting to db")
    app.db = await asyncpg.create_pool(**toml_to_native(app.cfg["database"]))

    argon2_cfg = toml_to_native(app.cfg["argon2"])
    app.password_hasher = PasswordHasher(**argon2_cfg)

    app.sched = JobManager(db=app.db, context_function=app.app_context)
    app.sched.register_job_queue(EmailQueue)

    app.locks = LockManager()
    app.running_vpns = RunningVPNManager()
    app.templates = TemplateManager()
    await app.running_vpns.on_app_start()


@app.after_serving
async def close_db():
    """Close all database connections."""
    log.info("closing job manager")
    await app.sched.stop_all()

    log.info("closing db")
    await app.db.close()


@app.errorhandler(404)
async def handle_notfound(_err):
    return "Not Found", 404


@app.errorhandler(500)
def handle_exception(exception):
    """Handle any kind of exception."""
    status_code = 500

    try:
        status_code = exception.status_code
    except AttributeError:
        pass

    return (
        jsonify(
            {
                "error": True,
                "message": repr(exception),
            }
        ),
        status_code,
    )


def _wrap_err_in_json(err: APIError) -> Tuple[quart.wrappers.Response, int]:
    res = {
        "error": True,
        "error_code": err.get_code(),
        "message": err.get_message(),
    }
    extra = err.get_payload()
    assert "error" not in extra
    res.update(extra)
    return jsonify(res), err.status_code


@app.errorhandler(APIError)
def handle_api_error(err: APIError):
    """Handle any kind of application-level raised error."""
    log.warning(f"API error: {err!r}")

    return _wrap_err_in_json(err)


@app.errorhandler(RequestSchemaValidationError)
async def handle_request_validation_Error(err: RequestSchemaValidationError):
    orig: Union[TypeError, ValidationError] = err.validation_error

    if isinstance(orig, TypeError):
        message = repr(orig)

        # this is hacky, but works for now. blame pydantic.
        #
        # this works by matching text in the representation of the TypeError
        # so we can create more straightforward messages for api users
        if "must be a mapping, not NoneType" in message:
            return _wrap_err_in_json(InputError(ErrorCodes.MISSING_JSON_BODY, {}))

        if "got an unexpected keyword argument" in message:
            # extract the unexpected argument from message text
            parts = message.split("argument '")
            unexpected_keyword = parts[-1][:-3]
            return _wrap_err_in_json(
                InputError(
                    ErrorCodes.UNEXPECTED_FIELD,
                    {"unexpected_field": unexpected_keyword},
                )
            )

        return _wrap_err_in_json(
            InputError(
                ErrorCodes.INPUT_VALIDATION_TYPE_ERROR,
                {"error_message": repr(orig)},
            )
        )

    # orig must be pydantic.ValidationError
    errors = orig.errors()
    remapped_errors = []
    for error_object in errors:
        remapped_errors.append(
            {
                "location": error_object["loc"],
                "message": error_object["msg"],
                "type": error_object["type"],
            }
        )
    return _wrap_err_in_json(
        InputError(
            ErrorCodes.INPUT_VALIDATION_FAIL,
            {"validation_errors": remapped_errors},
        )
    )


@app.errorhandler(ResponseSchemaValidationError)
async def handle_response_validation_Error(err: ResponseSchemaValidationError):
    log.warning("response validation error: {err!r}")
    return _wrap_err_in_json(
        ServerError(
            ErrorCodes.INVALID_OUTPUT,
            {"validation_error": repr(err.validation_error)},
        )
    )


setup_blueprints(app)
