# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import secrets
import asyncio
import uuid
import logging
from typing import List

from quart import Blueprint, g, current_app as app
from quart_schema import validate_request, validate_response
from pydantic.dataclasses import dataclass

from bbv.models import CloudToken, VPN, VPNOut
from bbv.decorators import needs_clearance, Clearance
from bbv.cloud import CloudProviderType, get_cloud_provider_impl
from bbv.errors import BadRequest, ErrorCodes, VPNNotFound
from bbv.names import random_name

bp = Blueprint("vpn", __name__)
log = logging.getLogger(__name__)


@dataclass
class CloudRegionOut:
    cloud_provider: str
    cloud_region: str
    iso_country_code: str


@dataclass
class CloudRegionList:
    regions: List[CloudRegionOut]


@bp.route("/<uuid:group_id>/regions/<wanted_iso_country_code>", methods=["GET"])
@validate_response(CloudRegionList)
@needs_clearance(Clearance.MEMBER)
async def fetch_available_providers(group_id, wanted_iso_country_code: str):
    """Fetch all available cloud providers in a certain country."""
    group = g.member.group

    # fetch all cloud tokens, call out to each provider for regions, cross
    # reference with given country, and return data.

    tokens = await CloudToken.from_group(group.id)

    result = []
    for token in tokens:
        # get provider impl, fetch regions
        provider_impl = get_cloud_provider_impl(token.provider)

        # TODO cache this data per group
        regions = await provider_impl.fetch_regions(token.value)
        for region in regions:
            if region.iso_country_code != wanted_iso_country_code:
                continue

            result.append(
                CloudRegionOut(
                    cloud_provider=token.provider.value,
                    cloud_region=region.cloud_internal_code,
                    iso_country_code=region.iso_country_code,
                )
            )

    return {"regions": result}


@dataclass
class SpawnVPNData:
    cloud_provider: CloudProviderType
    cloud_region: str
    wireguard_port: int


async def generate_cloud_init(vpn_id: uuid.UUID, vpn_auth_token: str):
    web_url = app.cfg["bbv"]["web_url"]
    return app.templates.cloud_init.render(
        auth_token=vpn_auth_token,
        # TODO maybe change prefix?
        asset_url=f"{web_url}/api/dev/vpn_assets",
        vpn_api_credentials=f"{vpn_id!s}:{vpn_auth_token}",
    )


async def wg_keypair():
    wg_priv_key_process = await asyncio.create_subprocess_shell(
        "wg genkey",
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )

    stdout, stderr = await wg_priv_key_process.communicate()
    if wg_priv_key_process.returncode != 0:
        log.error(
            "failed to generate private key (code %d). out=%r\nerr=%r",
            wg_priv_key_process.returncode,
            stdout.decode(),
            stderr.decode(),
        )
        raise RuntimeError("failed to generate private key")
    wg_priv_key = stdout.decode().strip()

    wg_pubkey_process = await asyncio.create_subprocess_shell(
        "wg pubkey",
        stdin=asyncio.subprocess.PIPE,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )

    wg_pubkey_process.stdin.write(wg_priv_key.encode())
    await wg_pubkey_process.stdin.drain()
    wg_pubkey_process.stdin.write_eof()

    pub_stdout, pub_stderr = await wg_pubkey_process.communicate()
    assert wg_pubkey_process.returncode == 0
    wg_pub_key = pub_stdout.decode().strip()

    assert wg_priv_key != wg_pub_key
    return wg_priv_key, wg_pub_key


@dataclass
class SingleVPN:
    vpn: VPNOut


@bp.route("/<uuid:group_id>/servers", methods=["POST", "PUT"])
@validate_request(SpawnVPNData)
@validate_response(SingleVPN)
@needs_clearance(Clearance.MEMBER)
async def summon_vpn(group_id, *, data: SpawnVPNData):
    """Summon a VPN in the selected group and region."""
    group = g.member.group
    provider_impl = get_cloud_provider_impl(data.cloud_provider)
    token = await CloudToken.fetch(group.id, data.cloud_provider)
    if token is None:
        raise BadRequest(ErrorCodes.NO_TOKEN_FOR_PROVIDER)

    # if we already have a vpn, just use it (one vpn per group per cloud region)
    #
    # to keep the claim of 'one vpn per group per cloud region' we also have
    # an asyncio lock on it.
    vpn = await VPN.fetch_by_region(group.id, data.cloud_region)
    if vpn is None:
        hostname = random_name()
        hostname = f"bbv-{hostname}"

        lock = app.locks.get_vpn_spells(group.id, data.cloud_region)
        if lock.locked():
            raise BadRequest(ErrorCodes.VPN_RESOURCE_LOCKED)

        async with lock:
            vpn_id = uuid.uuid4()
            vpn_api_token = secrets.token_urlsafe(32)
            cloud_init = await generate_cloud_init(vpn_id, vpn_api_token)

            # TODO maybe move this to a template ephmeral thing. we don't
            # want to hold the privkey, but we have to for now.
            #
            # there are other concerns with privkey on the server, like
            # entropy on first boot, especially on a vps.

            wg_priv_key, wg_pub_key = await wg_keypair()

            summoned_vpn_info = await provider_impl.summon(
                token.value, data.cloud_region, hostname, cloud_init
            )
            vpn = await VPN.create(
                vpn_id,
                group.id,
                data.cloud_provider,
                data.cloud_region,
                summoned_vpn_info,
                hostname,
                g.user.id,
                vpn_api_token,
                wg_priv_key,
                wg_pub_key,
                data.wireguard_port,
            )
            await app.running_vpns.on_incoming_vpn(vpn)

    assert vpn is not None
    return {"vpn": vpn.to_dict()}


@dataclass
class SummonedVPNsOut:
    summoned_vpns: List[VPNOut]


@bp.route("/<uuid:group_id>/servers", methods=["GET"])
@validate_response(SummonedVPNsOut)
@needs_clearance(Clearance.MEMBER)
async def list_summoned_vpns(group_id):
    """Get all currently summoned VPNs."""
    group = g.member.group
    all_summoned_vpns = await VPN.fetch_many_by_group(group.id)
    return {"summoned_vpns": [vpn.to_dict() for vpn in all_summoned_vpns]}


@bp.route("/<uuid:group_id>/servers/<uuid:server_id>", methods=["GET"])
@validate_response(SingleVPN)
@needs_clearance(Clearance.MEMBER)
async def fetch_single_vpn(group_id, server_id):
    """Get a VPN."""
    vpn = await VPN.fetch(server_id)
    if vpn.group_id != group_id:
        raise VPNNotFound()

    return {"vpn": vpn.to_dict()}


@dataclass
class ReleaseVPNOutput:
    released_vpn: VPNOut


@bp.route("/<uuid:group_id>/servers/<uuid:summoned_vpn_id>", methods=["DELETE"])
@validate_response(ReleaseVPNOutput)
@needs_clearance(Clearance.MEMBER)
async def release_vpn(group_id, summoned_vpn_id):
    """Release a VPN."""
    group = g.member.group
    vpn = await VPN.fetch(summoned_vpn_id)
    if vpn is None or vpn.group_id != group.id:
        raise VPNNotFound()

    await vpn.delete()

    return {"released_vpn": vpn.to_dict()}
