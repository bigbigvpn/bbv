# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only


from quart import Blueprint
from quart_schema import hide

bp = Blueprint("index", __name__)


@bp.route("/", methods=["GET"])
@hide
async def index_route():
    return "nyanya"
