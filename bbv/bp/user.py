# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from base64 import b64decode
from quart import Blueprint, g
from quart_schema import validate_request, validate_response
from pydantic.dataclasses import dataclass
from pydantic import field_validator

from bbv.models import UserOut
from bbv.auth import TokenType
from bbv.models import User, Group
from bbv.services import EmailQueue

from bbv.decorators import needs_clearance, Clearance


bp = Blueprint("user", __name__)


@bp.route("/@me", methods=["GET"])
@validate_response(UserOut, 200)
@needs_clearance(Clearance.USER)
async def get_self_user():
    return g.user.to_dict()


@bp.route("/@me/start", methods=["DELETE"])
@needs_clearance(Clearance.USER)
async def delete_self_start():
    delete_account_token = await g.user.create_delete_account_token()

    body = f"""
You have requested an account deletion.

token: '{delete_account_token}'

This token is valid for one hour.
"""

    job_id = await EmailQueue.submit(g.user.email, "account deletion request", body)
    return {"job_id": str(job_id)}, 200


@dataclass
class DeleteAccountFinishInput:
    delete_token: str


@bp.route("/@me/finish", methods=["DELETE"])
@validate_request(DeleteAccountFinishInput)
@needs_clearance(Clearance.USER)
async def delete_self_finish(data: DeleteAccountFinishInput):
    user = await User.accept_token(data.delete_token, TokenType.DeleteAccount)
    await user.delete()
    return "", 204


@bp.route("/@me/keys", methods=["GET"])
@needs_clearance(Clearance.USER)
async def fetch_all_keys_from_user():
    """Fetch all WireGuard® public keys for the current user.

    WireGuard® is a registered trademark of Jason A. Donenfeld.
    """

    return {"wireguard_keys": await g.user.fetch_wireguard_keys()}


@dataclass
class AddKeyInput:
    wireguard_key: str
    password: str

    @field_validator("wireguard_key", mode="after")
    def wireguard_key_must_be_base64(cls, value: str):
        try:
            b64decode(value)
        except Exception as exc:
            raise ValueError(f"Invalid wireguard key (must be base64) ({exc!r})")
        return value


@bp.route("/@me/keys", methods=["PUT"])
@validate_request(AddKeyInput)
@needs_clearance(Clearance.USER)
async def add_key(data: AddKeyInput):
    """Add a WireGuard® public key for the current user.

    WireGuard® is a registered trademark of Jason A. Donenfeld.
    """
    await g.user.validate_password(data.password)
    newly_added_key = await g.user.add_wireguard_key(data.wireguard_key)

    # after adding, dispatch sync to every vpn this user is a member of
    #
    # TODO this behavior is implemented in the blueprint, but it can't
    # be a method of g.user, because we would create circular imports.
    groups = await Group.fetch_groups(g.user.id)
    for group in groups:
        await group.dispatch_add_keys([newly_added_key])

    return {"wireguard_keys": await g.user.fetch_wireguard_keys()}


@bp.route("/@me/keys", methods=["DELETE"])
@validate_request(AddKeyInput)
@needs_clearance(Clearance.USER)
async def delete_key(data: AddKeyInput):
    """Remove a WireGuard® public key for the current user.

    WireGuard® is a registered trademark of Jason A. Donenfeld.
    """
    await g.user.validate_password(data.password)
    newly_deleted_key = await g.user.delete_wireguard_key(data.wireguard_key)

    # after deleting, dispatch sync to every vpn this user is a member of
    #
    # TODO this behavior is implemented in the blueprint, but it can't
    # be a method of g.user, because we would create circular imports.
    groups = await Group.fetch_groups(g.user.id)
    for group in groups:
        await group.dispatch_remove_keys([newly_deleted_key])

    return {"wireguard_keys": await g.user.fetch_wireguard_keys()}
