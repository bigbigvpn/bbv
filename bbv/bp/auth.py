# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
from typing import Optional

from quart import Blueprint
from quart_schema import validate_request, validate_response
from pydantic.dataclasses import dataclass
from pydantic import EmailStr

from bbv.models import User, RegisterOut
from bbv.auth import (
    hash_password,
    TokenType,
)
from bbv.errors import FailedAuth, BadRequest, ErrorCodes
from bbv.services import EmailQueue

bp = Blueprint("auth", __name__)
log = logging.getLogger(__name__)


@dataclass
class RegisterPayload:
    username: str
    email: EmailStr
    password: str


# TODO add payloads for failures
@bp.route("/register", methods=["POST"])
@validate_request(RegisterPayload)
@validate_response(RegisterOut, 200)
async def register_new_user(data: RegisterPayload):
    user = await User.create(
        username=data.username,
        email=data.email,
        password_hash=await hash_password(data.password),
    )
    log.info("new user %r", user)
    token = await user.create_auth_token()
    return RegisterOut(**user.to_dict(), token=token)


@dataclass
class LoginInput:
    username: str
    password: str


@dataclass
class LoginOutput:
    token: str


@bp.route("/login", methods=["POST"])
@validate_request(LoginInput)
@validate_response(LoginOutput, 200)
async def login_user(data: LoginInput):
    user = await User.fetch_by(username=data.username)

    if user is None:
        log.info("user %r doesn't exist", data.username)
        raise FailedAuth("User or password invalid")

    await user.validate_password(data.password)
    token = await user.create_auth_token()
    return {"token": token}, 200


@dataclass
class ResetPasswordInput:
    """Either username OR email will attempt to find the user and email them
    about the reset password request"""

    username: Optional[str] = None
    email: Optional[str] = None


@bp.route("/reset_password/start", methods=["POST"])
@validate_request(ResetPasswordInput)
async def reset_password_start(data: ResetPasswordInput):
    if not (data.username or data.email):
        raise BadRequest(ErrorCodes.RESET_PASSWORD_INCORRECT_INPUT)

    user = await User.fetch_by(username=data.username, email=data.email)
    # TODO add validation when user is None, think about errors
    # and possible user enumeration attacks?
    assert user is not None

    reset_password_token = await user.create_reset_password_token()

    body = f"""
You have requested a password reset.

token: '{reset_password_token}'

This token is valid for one hour.
"""

    job_id = await EmailQueue.submit(user.email, "reset password request", body)
    return {"job_id": str(job_id)}, 200


@dataclass
class ResetPasswordFinishInput:
    reset_token: str
    new_password: str


@bp.route("/reset_password/finish", methods=["POST"])
@validate_request(ResetPasswordFinishInput)
async def reset_password_finish(data: ResetPasswordFinishInput):
    user = await User.accept_token(data.reset_token, TokenType.ResetPassword)
    await user.update_password(data.new_password)
    return "", 204
