# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from typing import List

from quart import Blueprint, g
from quart_schema import validate_request, validate_response
from pydantic.dataclasses import dataclass

from bbv.models import Group
from bbv.models.member import Permissions
from bbv.decorators import needs_clearance, Clearance

bp = Blueprint("group", __name__)


@dataclass
class CreateGroupInput:
    name: str


@bp.route("", methods=["POST"], strict_slashes=False)
@validate_request(CreateGroupInput)
@validate_response(Group, 200)
@needs_clearance(Clearance.USER)
async def create_group(data: CreateGroupInput):
    """Create a group."""
    group = await Group.create(data.name, g.user.id)
    return group.to_dict()


@bp.route("/<uuid:group_id>", methods=["GET"])
@validate_response(Group, 200)
@needs_clearance(Clearance.MEMBER)
async def fetch_group(group_id):
    """Fetch a single group."""
    return g.member.group.to_dict()


@dataclass
class EditGroupInput:
    name: str


@bp.route("/<uuid:group_id>", methods=["PATCH"])
@validate_request(EditGroupInput)
@validate_response(Group, 200)
@needs_clearance(Permissions.OWNER)
async def edit_group(group_id, *, data):
    """Edit a group."""

    await g.member.group.update(name=data.name)
    return g.member.group.to_dict()


@dataclass
class DeleteGroupInput:
    password: str


@bp.route("/<uuid:group_id>", methods=["DELETE"])
@validate_request(DeleteGroupInput)
@validate_response(Group, 200)
@needs_clearance(Clearance.SUPEROWNER)
async def delete_group(group_id, *, data):
    """Delete a group.

    All VPNs in the group will be destroyed.
    """
    await g.member.group.delete()
    return g.member.group.to_dict()


@dataclass
class ListGroupOutput:
    groups: List[Group]


@bp.route("/list", methods=["GET"])
@validate_response(ListGroupOutput, 200)
@needs_clearance(Clearance.USER)
async def list_groups():
    """List groups the current user is a member of."""
    groups = await Group.fetch_groups(g.user.id)
    return {"groups": [group.to_dict() for group in groups]}
