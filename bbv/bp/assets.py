# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from uuid import UUID
from pathlib import Path
from quart import Blueprint, current_app as app, send_file, request, g

from bbv.models import VPN
from bbv.errors import FailedAuth, BadRequest, ErrorCodes

bp = Blueprint("assets", __name__)


def needs_vpn_token(handler):
    async def new_request_handler(*args, **kwargs):
        try:
            auth_value = request.headers["Authorization"]
        except KeyError:
            raise BadRequest(ErrorCodes.MISSING_AUTH_HEADER)

        try:
            vpn_marker, wrapped_creds = auth_value.split(" ")
            assert vpn_marker == "VPN"
            given_vpn_id, given_vpn_token = wrapped_creds.split(":")
            given_vpn_id = UUID(given_vpn_id)
        except (IndexError, ValueError, AssertionError):
            raise FailedAuth("Invalid VPN credentials provided")

        vpn = await VPN.authenticate(given_vpn_id, given_vpn_token)

        g.vpn = vpn
        return await handler(*args, **kwargs)

    new_request_handler.__name__ = handler.__name__
    return new_request_handler


@bp.route("/bbvd.service", methods=["GET"])
@needs_vpn_token
async def get_bbvd_service_file():
    env_vars = []
    ws_url = app.cfg["bbv"]["ws_url"]
    env_vars.append(("BBVD_URL", ws_url))
    env_vars.append(("BBVD_VPN_ID", str(g.vpn.id)))
    env_vars.append(("BBVD_VPN_TOKEN", str(g.vpn.auth_token)))
    env_vars.append(("BBVD_WIREGUARD_KEY", g.vpn.wireguard_private_key))
    env_vars.append(("BBVD_WIREGUARD_PORT", g.vpn.wireguard_port))
    env_vars.append(("BBVD_WIREGUARD_PATH", "/etc/wireguard/wg0.conf"))
    env_vars.append(("BBVD_WIREGUARD_INTERFACE", "wg0"))
    env_vars.append(
        ("BBVD_WIREGUARD_IP", f"{g.vpn.local_ipv4_address}, {g.vpn.local_ipv6_address}")
    )

    return app.templates.bbvd_service.render(env=env_vars), 200


@bp.route("/<path:path>")
async def get_any_asset(path):
    if ".." in path:
        return "no", 404

    absolute_asset_path = Path.cwd() / Path(app.cfg["bbv"]["assets_folder"]) / path
    return await send_file(str(absolute_asset_path))
