# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
from uuid import UUID
from typing import List

from quart import Blueprint, g
from quart_schema import validate_request, validate_response
from pydantic.dataclasses import dataclass

from bbv.models import Member, MemberOut
from bbv.models.member import Permissions
from bbv.decorators import needs_clearance, Clearance
from bbv.errors import MemberNotFound

bp = Blueprint("member", __name__)
log = logging.getLogger(__name__)


@dataclass
class ListMembersOutput:
    members: List[MemberOut]


@bp.route("/groups/<uuid:group_id>/members/list/<int:page>", methods=["GET"])
@validate_response(ListMembersOutput, 200)
@needs_clearance(Permissions.OWNER)
async def list_members(group_id: UUID, page: int):
    # TODO implement pagination
    # TODO should <int:page> be the preffered pagination method

    members = await Member.from_group(g.member.group.id)
    return {"members": [m.to_dict() for m in members]}


@dataclass
class EditMemberInput:
    permissions: List[str]


@bp.route("/groups/<uuid:group_id>/members/<uuid:target_id>", methods=["PATCH"])
@validate_request(EditMemberInput)
@validate_response(MemberOut, 200)
@needs_clearance(Permissions.OWNER)
async def edit_member(group_id: UUID, target_id: UUID, *, data: EditMemberInput):
    if g.member.group.superowner_id == target_id:
        return "you are superowner, no need for perms", 400

    target = await Member.fetch(group_id, target_id)
    if target is None:
        raise MemberNotFound()

    await target.set_permissions(data.permissions)
    return target.to_dict()


@bp.route("/groups/<uuid:group_id>/members/@me", methods=["DELETE"])
@validate_response(MemberOut, 200)
@needs_clearance(Clearance.USER)
async def leave_group(group_id: UUID):
    target = await Member.fetch(group_id, g.user.id)
    if target is None:
        raise MemberNotFound()

    if g.member.group.superowner_id == target.user.id:
        # TODO data depedency error
        return "superowner can not leave group. set another", 400

    await target.delete()
    return target.to_dict()


@bp.route("/groups/<uuid:group_id>/members/<uuid:target_id>", methods=["DELETE"])
@validate_response(MemberOut, 200)
@needs_clearance(Permissions.OWNER)
async def delete_member(group_id: UUID, target_id: UUID):
    target = await Member.fetch(group_id, target_id)
    if target is None:
        raise MemberNotFound()

    if g.member.group.superowner_id == target_id:
        # TODO permission error
        return "superowner can not be removed", 400

    await target.delete()
    return target.to_dict()
