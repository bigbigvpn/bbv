# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only


from .index import bp as index
from .auth import bp as auth
from .user import bp as user
from .group import bp as group
from .invite import bp as invite
from .member import bp as member
from .cloud_token import bp as cloud_token
from .vpn import bp as vpn
from .control import bp as control
from .assets import bp as assets

__all__ = (
    "index",
    "auth",
    "user",
    "group",
    "invite",
    "member",
    "cloud_token",
    "vpn",
    "control",
    "assets",
)
