# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
from typing import List
from uuid import UUID

from quart import Blueprint, current_app as app

from quart_schema import validate_request, validate_response
from pydantic.dataclasses import dataclass

from bbv.models import CloudToken, CloudTokenOut
from bbv.cloud import CloudProviderType
from bbv.models.member import Permissions
from bbv.decorators import needs_clearance
from bbv.errors import BadRequest, ErrorCodes

bp = Blueprint("cloud_token", __name__)
log = logging.getLogger(__name__)


@dataclass
class CloudTokensWrapper:
    cloud_tokens: List[CloudTokenOut]

    @classmethod
    def from_db_list(cls, tokens: List[CloudToken]):
        return cls([tok.to_out() for tok in tokens])


@bp.route("/<uuid:group_id>/cloud_tokens", methods=["GET"])
@validate_response(CloudTokensWrapper, 200)
@needs_clearance(Permissions.CLOUD_TOKENS)
async def fetch_tokens(group_id: UUID):
    """Fetch all cloud tokens in a group."""
    tokens = await CloudToken.from_group(group_id)
    return CloudTokensWrapper.from_db_list(tokens)


@dataclass
class SingleCloudToken:
    value: str


@bp.route("/<uuid:group_id>/cloud_tokens/<cloud_provider>", methods=["PUT"])
@validate_request(SingleCloudToken)
@validate_response(CloudTokensWrapper, 200)
@needs_clearance(Permissions.CLOUD_TOKENS)
async def set_cloud_token(
    group_id: UUID, cloud_provider: str, *, data: SingleCloudToken
):
    try:
        cloud_provider_type = CloudProviderType(cloud_provider)
    except ValueError:
        raise BadRequest(ErrorCodes.INVALID_CLOUD_PROVIDER)

    # set by inserting or update existing one
    await app.db.execute(
        """
        INSERT INTO bbv_group_cloud_tokens
            (group_id, provider, value)
        VALUES
            ($1, $2, $3)
        ON CONFLICT
            ON CONSTRAINT bbv_group_cloud_tokens_pkey
        DO
          UPDATE
            SET value = $3
            WHERE
                bbv_group_cloud_tokens.group_id = $1
            AND bbv_group_cloud_tokens.provider = $2
          """,
        group_id,
        cloud_provider_type.value,
        data.value,
    )

    # refetch tokens
    refetched = await CloudToken.from_group(group_id)
    return CloudTokensWrapper.from_db_list(refetched)


@bp.route("/<uuid:group_id>/cloud_tokens/<cloud_provider>", methods=["DELETE"])
@validate_response(CloudTokensWrapper, 200)
@needs_clearance(Permissions.CLOUD_TOKENS)
async def unset_cloud_token(group_id: UUID, cloud_provider: str):
    try:
        cloud_provider_type = CloudProviderType(cloud_provider)
    except ValueError:
        raise BadRequest(ErrorCodes.INVALID_CLOUD_PROVIDER)

    # TODO when unsetting, how do we deal with the currently running vpns?
    # delete all of them?
    token_list = await CloudToken.from_group(group_id)
    for token in token_list:
        if token.provider == cloud_provider_type:
            await token.delete()
            break
    else:
        # if no tokens are found for that provider, don't do anything.
        pass

    # refetch tokens
    refetched = await CloudToken.from_group(group_id)
    return CloudTokensWrapper.from_db_list(refetched)
