# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
from uuid import UUID
from typing import Optional, List
from datetime import datetime

from quart import Blueprint, g
from quart_schema import validate_request, validate_response
from pydantic.dataclasses import dataclass

from bbv.models import Group, Invite
from bbv.models.invite import InviteTooMuchUses, InviteExpired
from bbv.errors import InviteNotFound

from bbv.models.member import Permissions
from bbv.decorators import needs_clearance, Clearance


bp = Blueprint("invite", __name__)
log = logging.getLogger(__name__)


@dataclass
class CreateInviteInput:
    max_uses: Optional[int]
    expires_at: Optional[datetime]


@bp.route("/groups/<uuid:group_id>/invites", methods=["PUT"])
@validate_request(CreateInviteInput)
@validate_response(Invite, 200)
@needs_clearance(Permissions.INVITE)
async def create_invite(group_id, *, data: CreateInviteInput):
    invite = await Invite.create(
        g.member.group.id,
        max_uses=data.max_uses,
        expires_at=data.expires_at,
    )

    return invite.to_dict()


@dataclass
class InviteListOutput:
    invites: List[Invite]


@bp.route("/groups/<uuid:group_id>/invites", methods=["GET"])
@validate_response(InviteListOutput, 200)
@needs_clearance(Permissions.INVITE)
async def list_invites(group_id):
    invites = await Invite.fetch_invites(g.member.group.id)
    return {"invites": [invite.to_dict() for invite in invites]}


@bp.route("/groups/<uuid:group_id>/invites/<invite_code>", methods=["DELETE"])
@validate_response(Invite, 200)
@needs_clearance(Permissions.INVITE)
async def delete_invite(group_id: UUID, invite_code: str):
    invite = await Invite.fetch(invite_code)
    if invite is None:
        raise InviteNotFound()

    if invite.group_id != g.member.group.id:
        raise InviteNotFound()

    await invite.delete()
    return invite.to_dict()


@bp.route("/invites/<invite_code>/use", methods=["POST"])
@validate_response(Invite, 200)
@needs_clearance(Clearance.USER)
async def use_invite(invite_code: str):
    invite = await Invite.fetch(invite_code)
    if invite is None:
        raise InviteNotFound()

    try:
        await invite.validate_itself()
    except (InviteTooMuchUses, InviteExpired):
        await invite.delete()
        log.warning("invite '%r' became invalid, deleting")

        # TODO reraise api error
        return "", 404

    group = await Group.fetch(invite.group_id)
    assert group is not None

    # TODO convert asyncpg UniqueViolationError to MemberAlreadyExists
    # then catch it here, return 400 later
    #
    # TODO member api (and member adding code)
    await group.add_member(g.user.id)

    old_uses = invite.uses
    await invite.use()
    assert invite.uses == old_uses + 1

    return invite.to_dict()
