# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from .types import CloudProviderType
from .provider import CloudProvider
from .hetzner import HetznerCloud
from .scaleway import ScalewayCloud


def _actual_get_provider(provider_type):
    return {
        # CloudProviderType.DIGITALOCEAN: DigitalOceanCloud,
        CloudProviderType.HETZNER: HetznerCloud,
        CloudProviderType.SCALEWAY: ScalewayCloud,
    }[provider_type]


def get_cloud_provider_impl(provider_type: CloudProviderType) -> CloudProvider:
    return _actual_get_provider(provider_type)


__all__ = (
    "CloudProviderType",
    "CloudProvider",
    "get_cloud_provider_impl",
)
