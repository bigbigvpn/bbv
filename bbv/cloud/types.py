# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from enum import Enum
from typing import Optional
from datetime import datetime
from ipaddress import IPv4Network, IPv6Network

from pydantic.dataclasses import dataclass


class CloudProviderType(Enum):
    DIGITALOCEAN = "digitalocean"
    HETZNER = "hetzner"
    SCALEWAY = "scaleway"


@dataclass
class CloudRegion:
    # e.g 'FR'
    iso_country_code: str

    # e.g fr-par-1
    cloud_internal_code: str


@dataclass
class CloudVPN:
    """A generic representation of a VPS created by a cloud provider."""

    #: Represents a possible id from the cloud api for things like support
    #  to that cloud provider (or, maybe, turning it off).
    server_cloud_id: str
    ipv4_network: IPv4Network
    ipv6_network: Optional[IPv6Network]
    #: used in future dev to calculate prices
    created_at: datetime
