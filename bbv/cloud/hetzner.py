# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import ipaddress
import functools
from typing import List

from quart import current_app as app

from hcloud import Client as HetznerClient
from hcloud.server_types.domain import ServerType
from hcloud.images.domain import Image
from hcloud.locations.domain import Location
from hcloud.locations.client import LocationsClient
from hcloud.ssh_keys.client import SSHKeysClient


from .types import CloudRegion, CloudVPN


class HetznerCloud:
    """This is an interface for the Hetzner Cloud API."""

    @staticmethod
    async def fetch_regions(token: str) -> List[CloudRegion]:
        client = HetznerClient(token=token)
        locations_client = LocationsClient(client)
        given_locations = await app.loop.run_in_executor(None, locations_client.get_all)
        return [
            CloudRegion(
                iso_country_code=location.country,
                cloud_internal_code=location.name,
            )
            for location in given_locations
        ]

    @staticmethod
    async def summon(
        token: str, cloud_region_id: str, hostname: str, cloud_init: str
    ) -> CloudVPN:
        client = HetznerClient(token=token)
        ssh_keys = await app.loop.run_in_executor(None, SSHKeysClient(client).get_all)
        create_call_wrapper = functools.partial(
            client.servers.create,
            name=hostname,
            server_type=ServerType(name="cx11"),
            image=Image(name="ubuntu-20.04"),
            location=Location(name=cloud_region_id),
            ssh_keys=ssh_keys,
            user_data=cloud_init,
        )

        response = await app.loop.run_in_executor(None, create_call_wrapper)

        server = response.server

        return CloudVPN(
            server_cloud_id=str(server.id),
            ipv4_network=ipaddress.IPv4Network(server.public_net.ipv4.ip),
            ipv6_network=ipaddress.IPv6Network(server.public_net.ipv6.ip),
            created_at=server.created,
        )

    @staticmethod
    async def release(token: str, _cloud_region_id: str, cloud_vpn_id: str) -> None:
        client = HetznerClient(token=token)

        fetch_call = functools.partial(client.servers.get_by_id, cloud_vpn_id)
        server = await app.loop.run_in_executor(None, fetch_call)
        await app.loop.run_in_executor(None, server.delete)
