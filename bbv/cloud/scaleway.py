# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
import ipaddress
from typing import List

import httpx
from datetime import datetime
from asyncstdlib import functools

from .types import CloudRegion, CloudVPN

API_BASE = "https://api.scaleway.com"


def _headers(token: str) -> dict:
    actual_token, _project_id = token.split(":")
    return {"X-Auth-Token": actual_token}


def _loc_prefix(location: str) -> str:
    return f"{API_BASE}/instance/v1/zones/{location}"


def _server_prefix(cloud_region_id: str, cloud_vpn_id: str) -> str:
    return f"{_loc_prefix(cloud_region_id)}/servers/{cloud_vpn_id}"


async def _server_action(
    client, cloud_region_id: str, cloud_vpn_id: str, action: str
) -> None:
    req = await client.post(
        f"{_server_prefix(cloud_region_id,cloud_vpn_id)}/action",
        json={"action": action},
    )
    assert req.status_code == 202


async def _add_cloud_init(
    client, cloud_region_id: str, cloud_vpn_id: str, cloud_init: str
) -> None:
    resp = await client.patch(
        f"{_server_prefix(cloud_region_id, cloud_vpn_id)}/user_data/cloud-init",
        headers={"Content-Type": "text/plain"},
        data=cloud_init,
    )
    assert resp.status_code == 204


@functools.lru_cache(maxsize=32)
async def _get_ubuntu_image_id(client, location: str) -> str:
    resp = await client.get(
        f"{_loc_prefix(location)}/images",
    )
    assert resp.status_code == 200
    rjson = resp.json()

    assert rjson is not None
    return next(
        img["id"]
        for img in rjson["images"]
        if img["name"].lower() == "ubuntu 20.04 focal fossa"
    )


class ScalewayCloud:
    """This is an interface for the Scaleway Cloud API."""

    @staticmethod
    async def fetch_regions(_token: str) -> List[CloudRegion]:
        SCALEWAY_REGIONS = {
            "FR": ["fr-par-1", "fr-par-2"],
            "NL": ["nl-ams-1"],
            "PL": ["pl-waw-1"],
        }

        results = []

        for country, cloud_region_id_list in SCALEWAY_REGIONS.items():
            for cloud_region_id in cloud_region_id_list:
                results.append(
                    CloudRegion(
                        iso_country_code=country,
                        cloud_internal_code=cloud_region_id,
                    )
                )

        return results

    @staticmethod
    async def summon(
        token: str, cloud_region_id: str, hostname: str, cloud_init: str
    ) -> CloudVPN:
        _auth, project_id = token.split(":")

        async with httpx.AsyncClient(
            headers=_headers(token),
            timeout=10.0,
        ) as client:
            image_id = await _get_ubuntu_image_id(client, cloud_region_id)
            url = f"{_loc_prefix(cloud_region_id)}/servers"
            spinup_data = {
                "name": hostname,
                "commercial_type": "DEV1-S",
                "project": project_id,
                "image": image_id,
                "enable_ipv6": True,
            }

            resp = await client.post(url, json=spinup_data)
            assert resp.status_code == 201
            scaleway_server = resp.json()
            server_id = scaleway_server["server"]["id"]

            await _add_cloud_init(client, cloud_region_id, server_id, cloud_init)
            await _server_action(client, cloud_region_id, server_id, "poweron")

            while not scaleway_server["server"]["public_ip"]:
                await asyncio.sleep(1)
                resp = await client.get(
                    f"{_server_prefix(cloud_region_id, server_id)}",
                )
                assert resp.status_code == 200
                scaleway_server = resp.json()

        created_at = datetime.strptime(
            scaleway_server["server"]["creation_date"], "%Y-%m-%dT%H:%M:%S.%f+00:00"
        )

        return CloudVPN(
            server_cloud_id=server_id,
            ipv4_network=ipaddress.IPv4Network(
                scaleway_server["server"]["public_ip"]["address"]
            ),
            ipv6_network=ipaddress.IPv6Network(
                scaleway_server["server"]["ipv6"]["address"]
            ),
            created_at=created_at,
        )

    async def release(token: str, cloud_region_id: str, cloud_vpn_id: str) -> None:
        async with httpx.AsyncClient(headers=_headers(token)) as client:
            await _server_action(client, cloud_region_id, cloud_vpn_id, "terminate")
