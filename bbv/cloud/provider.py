# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from typing import List

from .types import CloudRegion, CloudVPN


class CloudProvider:
    """This is an interface for cloud APIs"""

    @staticmethod
    async def fetch_regions(token: str) -> List[CloudRegion]:
        ...

    @staticmethod
    async def summon(
        token: str, cloud_region_id: str, hostname: str, cloud_init: str
    ) -> CloudVPN:
        ...

    @staticmethod
    async def release(token: str, cloud_region_id: str, cloud_vpn_id: str) -> None:
        ...
