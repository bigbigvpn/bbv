# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
import logging
from typing import Optional, Tuple
from dataclasses import dataclass

import smtplib
from email.message import EmailMessage
from email.utils import formatdate
from violet import JobQueue
from violet.fail_modes import Retry
from hail import Flake

from quart import current_app as app


log = logging.getLogger(__name__)


@dataclass
class EmailQueueContext:
    smtp: Optional[smtplib.SMTP] = None

    async def connect(self):
        log.info("Connecting to SMTP")
        self.smtp = await app.loop.run_in_executor(
            None,
            raw_connect_smtp,
            app.cfg,
        )

    async def close(self, *, wait: bool = True):
        if wait:
            log.debug("Waiting for SMTP inactivity...")
            await asyncio.sleep(120)

        if self.smtp is None:
            return

        log.info("Closing SMTP connection!")
        try:
            await app.loop.run_in_executor(None, self.smtp.quit)
        except smtplib.SMTPServerDisconnected as exc:
            log.error("We were already disconnected (%r), ignoring", exc)
        finally:
            self.smtp = None

    async def ensure(self):
        if self.smtp is None:
            await self.connect()

        app.sched.stop("destroy_smtp")
        app.sched.spawn(self.close, [], name="destroy_smtp")


def raw_send_email(ctx: EmailQueueContext, cfg, to: str, subject: str, content: str):
    assert ctx.smtp is not None
    msg = EmailMessage()

    msg.set_content(content)
    msg["Subject"] = subject
    msg["From"] = cfg["smtp"]["from"]
    msg["To"] = to

    # Not needed when https://github.com/python/cpython/pull/5176 is merged
    msg["Date"] = formatdate()

    log.debug("smtp send %r %r", cfg["smtp"]["server"], cfg["smtp"]["port"])
    ctx.smtp.send_message(msg)
    log.debug("smtp done")


def raw_connect_smtp(cfg):
    """Connect and authenticate to a SMTP server."""
    server = cfg["smtp"]["server"]
    log.info("Connecting to %r", server)
    smtp = smtplib.SMTP(server, port=cfg["smtp"]["port"])
    if cfg["smtp"]["starttls"]:
        log.debug("smtp starttls")
        smtp.starttls()

    log.debug("smtp login")
    smtp.login(cfg["smtp"]["username"], cfg["smtp"]["password"])

    return smtp


class EmailQueue(JobQueue):
    name = "bbv_queue_email"
    args = ("to_address", "subject", "body")

    workers = 1
    fail_mode = Retry(retry_max_attempts=20)

    # we bump from 1/1 to 5/30 so that we don't overload the database on
    # every second. email isn't super realtime (and in most cases,
    # we'll already be receiving the tasks in real time. poller actions
    # only happen in the case we miss it, such as a restart of the webapp)
    poller_takes = 5
    poller_seconds = 30.0

    @classmethod
    def map_persisted_row(cls, row) -> Tuple[str, str, str]:
        return row["to_address"], row["subject"], row["body"]

    @classmethod
    async def submit(cls, to_address: str, subject: str, body: str, **kwargs) -> Flake:
        subject = app.cfg["smtp"]["subject_prefix"] + subject
        return await cls._sched.raw_push(
            cls,
            (to_address, subject, body),
            **kwargs,
        )

    @classmethod
    async def setup(cls, ctx):
        try:
            app._email_context
        except AttributeError:
            app._email_context = EmailQueueContext()

    @classmethod
    async def handle(cls, ctx):
        to_address, subject, content = ctx.args
        email_ctx = app._email_context
        await email_ctx.ensure()

        # since we only declare a single worker, semaphores
        # are not needed.
        future = app.loop.run_in_executor(
            None,
            raw_send_email,
            email_ctx,
            app.cfg,
            to_address,
            subject,
            content,
        )

        try:
            await asyncio.wait_for(future, timeout=30)
        except smtplib.SMTPServerDisconnected as exc:
            log.error("Failed to send email. Disconnecting and re-raising")
            await email_ctx.destroy(wait=False)
            await asyncio.sleep(2)
            raise exc
