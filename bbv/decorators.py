# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import itsdangerous
from enum import Enum, auto
from typing import Union

from quart import request, g
from bbv.errors import FailedAuth, GroupNotFound, Forbidden, ErrorCodes

from bbv.models import User, Member
from bbv.models.member import Permissions, MemberHasNoPermission
from bbv.auth import verify_token, unsafe_parse_token, TokenType


async def _do_clearance_auth():
    try:
        token = request.args["token"]
    except KeyError:
        token = request.headers["Authorization"]

    if not token:
        raise FailedAuth("Invalid token (token not found)")

    # we parse the token first, extract the payload, validate it, and
    # use the user id in the payload to fetch the user's password hash,
    # which is then used as token secret key.
    token_data = unsafe_parse_token(token)
    assert token_data.v == 0

    if token_data.t != TokenType.User:
        raise FailedAuth(
            f"invalid token (invalid token type, expected User, got {token_data.t})"
        )

    token_user_id = token_data.d.user_id
    user = await User.fetch(token_user_id)
    if user is None:
        raise FailedAuth("Invalid token (user not found)")

    try:
        real_data = await verify_token(user.password_hash, token)
    except itsdangerous.exc.BadSignature:
        raise FailedAuth("Invalid token (bad signature)")
    except itsdangerous.exc.SignatureExpired:
        raise FailedAuth("Invalid token (signature expired)")

    assert real_data.d.user_id == token_data.d.user_id

    # after verification is done, we also have the entire user object, add
    # it to quart's global context var
    g.user = user


async def _do_clearance_member(*args, **kwargs):
    group_id = kwargs.get("group_id")
    assert group_id is not None

    member = await Member.fetch(group_id, g.user.id)
    if member is None:
        raise GroupNotFound()

    g.member = member


async def _do_clearance_member_perm(permission: Permissions):
    try:
        await g.member.has_permission(permission)
    except MemberHasNoPermission:
        raise Forbidden(ErrorCodes.MEMBER_NO_PERMISSION)


async def _do_clearance_member_superowner():
    if g.member.group.superowner_id != g.member.user.id:
        raise Forbidden(ErrorCodes.MEMBER_NO_SUPEROWNER)


class Clearance(Enum):
    USER = auto()
    MEMBER = auto()
    SUPEROWNER = auto()


def needs_clearance(clearance: Union[Permissions, Clearance]):
    def new_deco(handler):
        async def new_request_handler(*args, **kwargs):
            if clearance == Clearance.USER:
                await _do_clearance_auth()
            elif clearance == Clearance.MEMBER:
                await _do_clearance_auth()
                await _do_clearance_member(*args, **kwargs)
            elif clearance == Clearance.SUPEROWNER:
                await _do_clearance_auth()
                await _do_clearance_member(*args, **kwargs)
                await _do_clearance_member_superowner()
            else:
                # clearance is Permissions
                await _do_clearance_auth()
                await _do_clearance_member(*args, **kwargs)
                await _do_clearance_member_perm(clearance)

            return await handler(*args, **kwargs)

        new_request_handler.__name__ = handler.__name__
        return new_request_handler

    return new_deco
