# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from enum import IntEnum


class APIError(Exception):
    #: HTTP status code
    status_code = 500
    code = 0

    def get_payload(self):
        """Get specific payload that will be merged with the error object"""
        return {}

    def get_message(self) -> str:
        return self.args[0]

    def get_code(self) -> int:
        return self.code


class ErrorCodes(IntEnum):
    """Directory for API error codes.

    1XXX -> Resource not found errors
    2XXX -> Data dependency problems (needs a group with a token, or something else)
    3XXX -> Input errors (missing some value or required body)
    4XXX -> Server-side programatic errors (should definitely be reported)
    5XXX -> Runtime errors, independent of input data (resource locking)
    """

    GENERIC_NOT_FOUND = 1000
    USER_NOT_FOUND = 1001
    GROUP_NOT_FOUND = 1002
    MEMBER_NOT_FOUND = 1003
    INVITE_NOT_FOUND = 1004
    SUMMONED_VPN_NOT_FOUND = 1005
    VPN_NOT_FOUND = 1006

    NO_TOKEN_FOR_PROVIDER = 2001
    MEMBER_NO_PERMISSION = 2002
    MEMBER_NO_SUPEROWNER = 2003

    INPUT_VALIDATION_FAIL = 3000
    MISSING_AUTH_HEADER = 3001
    MISSING_JSON_BODY = 3002
    UNEXPECTED_FIELD = 3003
    INPUT_VALIDATION_TYPE_ERROR = 3004
    RESET_PASSWORD_INCORRECT_INPUT = 3005
    INVALID_CLOUD_PROVIDER = 3006

    INVALID_OUTPUT = 4001

    GENERIC_RUNTIME_ERROR = 5000
    VPN_RESOURCE_LOCKED = 5001


ERROR_MESSAGES = {
    ErrorCodes.GENERIC_NOT_FOUND: "Resource not found",
    ErrorCodes.USER_NOT_FOUND: "User not found",
    ErrorCodes.GROUP_NOT_FOUND: "Group not found",
    ErrorCodes.MEMBER_NOT_FOUND: "Member not found",
    ErrorCodes.INVITE_NOT_FOUND: "Invite not found",
    ErrorCodes.SUMMONED_VPN_NOT_FOUND: "Summoned VPN not found",
    ErrorCodes.VPN_NOT_FOUND: "VPN not found",
    # 2xxx
    ErrorCodes.NO_TOKEN_FOR_PROVIDER: "Invalid group/provider combo, does the group have a token for that provider?",
    ErrorCodes.MEMBER_NO_PERMISSION: "You do not have the necessary permission to execute this action",
    ErrorCodes.MEMBER_NO_SUPEROWNER: "You are not a superowner of the group",
    # 3xxx
    ErrorCodes.INPUT_VALIDATION_FAIL: "Input has failed to validate.",
    ErrorCodes.MISSING_AUTH_HEADER: "Missing Authorization header",
    ErrorCodes.MISSING_JSON_BODY: "Missing JSON body (Content-Type header MUST be set)",
    ErrorCodes.UNEXPECTED_FIELD: "Unexpected field found in object",
    ErrorCodes.RESET_PASSWORD_INCORRECT_INPUT: "Either an username or an email MUST be provided",
    ErrorCodes.INVALID_CLOUD_PROVIDER: "Given cloud provider does not exist",
    # 4xxx
    ErrorCodes.INVALID_OUTPUT: "Server sent invalid output",
    # 5xxx
    ErrorCodes.GENERIC_RUNTIME_ERROR: "Generic runtime error",
    ErrorCodes.VPN_RESOURCE_LOCKED: "VPN resource locked. Only one user can create a vpn in a group in a cloud region at any single moment.",
}


class APIErrorOnlyCodes(APIError):
    """An API error with an interface that only allows a value from ErrorCodes
    to be given on instantiation."""

    def __init__(self, error_code: ErrorCodes):
        super().__init__(error_code)

    def get_code(self) -> int:
        return self.args[0].value

    def get_message(self) -> str:
        return ERROR_MESSAGES[self.args[0]]


class APIErrorCodeAndPayload(APIError):
    def __init__(self, error_code: ErrorCodes, payload):
        super().__init__(error_code, payload)

    def get_payload(self):
        return self.args[1]

    def get_message(self) -> str:
        return ERROR_MESSAGES[self.args[0]]

    def get_code(self) -> int:
        return self.args[0].value


class InputError(APIErrorCodeAndPayload):
    status_code = 400


class ServerError(APIErrorOnlyCodes):
    status_code = 500


class BadRequest(APIErrorOnlyCodes):
    status_code = 400


class APIErrorConstMessage(APIError):
    def get_code(self) -> int:
        return self.code

    def get_message(self) -> str:
        return ERROR_MESSAGES[self.code]


class NotFound(APIErrorConstMessage):
    status_code = 404


class GroupNotFound(NotFound):
    code = ErrorCodes.GROUP_NOT_FOUND


class UserNotFound(NotFound):
    code = ErrorCodes.USER_NOT_FOUND


class MemberNotFound(NotFound):
    code = ErrorCodes.MEMBER_NOT_FOUND


class InviteNotFound(NotFound):
    code = ErrorCodes.INVITE_NOT_FOUND


class VPNNotFound(NotFound):
    code = ErrorCodes.VPN_NOT_FOUND


class FailedAuth(APIError):
    """Failed to authenticate.

    Invalid token, password, etc.
    """

    status_code = 401


class Forbidden(APIErrorOnlyCodes):
    """Given credentials can not access given resource."""

    status_code = 403
