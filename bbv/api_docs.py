# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from quart_schema import QuartSchema, hide


class ApiDocs(QuartSchema):
    """Class to help inject more information into the
    OpenAPI schema given by quart-schema"""

    ROUTE_PREFIXES_WITHOUT_AUTH = ("/api/dev/auth", "/api/dev/vpn_assets")

    def init_app(self, app):
        super().init_app(app)

        # override swagger-ui version with possibly updated one
        app.config[
            "QUART_SCHEMA_SWAGGER_JS_URL"
        ] = "https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.47.1/swagger-ui-bundle.js"
        app.config[
            "QUART_SCHEMA_SWAGGER_CSS_URL"
        ] = "https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.47.1/swagger-ui.min.css"

    @hide
    async def openapi(self) -> dict:
        """'mix-in' the OpenAPI data given by quart-schema and add our
        own entities on top of it."""
        openapi_data = await super().openapi()
        security_schemes = {}
        security_schemes["api_token"] = {
            "type": "apiKey",
            "description": "API token granted by the login route.",
            "name": "Authorization",
            "in": "header",
        }

        for path_key, path_value in openapi_data["paths"].items():
            for prefix in self.ROUTE_PREFIXES_WITHOUT_AUTH:
                if path_key.startswith(prefix):
                    break
            else:
                for method_value in path_value.values():
                    method_value["security"] = [{"api_token": []}]

        openapi_data["components"]["securitySchemes"] = security_schemes
        return openapi_data
