# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
from collections import defaultdict
from uuid import UUID


class LockManager:
    def __init__(self):
        self.vpn_spells = defaultdict(asyncio.Lock)

    def get_vpn_spells(self, group_id: UUID, cloud_region: str) -> asyncio.Lock:
        return self.vpn_spells[(group_id, cloud_region)]
