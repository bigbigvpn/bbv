# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from pathlib import Path
from quart import current_app as app
from jinja2 import Template


class TemplateManager:
    def __init__(self):
        self.templates = {}

    def get_template(self, path):
        template = self.templates.get(path)

        if template:
            return template

        with open(path) as fp:
            template = Template(fp.read())

        self.templates[path] = template
        return template

    @property
    def bbvd_service(self):
        path = Path.cwd() / Path(app.cfg["bbv"]["assets_folder"]) / "bbvd.service"
        return self.get_template(path)

    @property
    def cloud_init(self):
        return self.get_template(app.cfg["bbv"]["cloud_init_template"])
