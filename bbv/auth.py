# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import json

from enum import Enum
from typing import Union
from uuid import UUID

from quart import current_app as app
from itsdangerous import URLSafeTimedSerializer
from itsdangerous.encoding import base64_decode
from argon2.exceptions import VerifyMismatchError
from pydantic.dataclasses import dataclass


async def hash_password(data: str) -> str:
    """Hash a password in a separate thread and return the result."""
    return await app.loop.run_in_executor(
        None,
        app.password_hasher.hash,
        data,
    )


async def verify_password(password_hash: str, password: str) -> bool:
    """Return if a given password is valid, given the hash."""
    try:
        await app.loop.run_in_executor(
            None, app.password_hasher.verify, password_hash, password
        )
        return True
    except VerifyMismatchError:
        return False


class TokenType(Enum):
    #: Maps to a TokenUserPayload in the TokenData.d field
    User = "user"
    #: Maps to a TokenUserPayload in the TokenData.d field
    ResetPassword = "reset_password"
    #: Maps to a TokenUserPayload in the TokenData.d field
    DeleteAccount = "delete_account"


@dataclass
class TokenUserPayload:
    user_id: UUID

    def to_dict(self):
        return {"user_id": self.user_id.hex}


@dataclass
class TokenData:
    #: the version of this token. useful if we do breaking changes to authentication
    v: int
    t: TokenType
    # this is interesting https://github.com/samuelcolvin/pydantic/issues/619
    # i really want this so that Union can be discriminated by 't'
    #
    #: the data of this token. depends on 't', currently, since we only have
    #  a need to hold the user id in the token, the payload can only be
    #  TokenUserPayload.
    d: Union[TokenUserPayload]

    def to_dict(self):
        return {
            "v": self.v,
            "t": self.t.value,
            "d": self.d.to_dict(),
        }


def unsafe_parse_token(token: str) -> TokenData:
    """THIS IS AN UNSAFE FUNCTION THAT DOES NOT PROVIDE AUTHENTICATION.

    Extract the data of a token so that its possible to get the
    user id and afterwards, token key required to authenticate.
    """

    # token structure: <data in base64>.<timestamp>.<signature>
    parts = token.split(".")
    token_body_string = parts[0]

    token_body = json.loads(base64_decode(token_body_string))
    token_data = TokenData(**token_body)
    return token_data


HOUR = 3600


async def verify_token(secret_key, token: str) -> TokenData:
    signer = URLSafeTimedSerializer(secret_key)
    data = TokenData(**signer.loads(token))
    assert data.v == 0

    token_type = data.t

    if token_type == TokenType.User:
        age = 24 * HOUR
    elif token_type in [TokenType.ResetPassword, TokenType.DeleteAccount]:
        age = 1 * HOUR
    elif token_type == "api":  # TODO TokenType.API, api key mechanism, etc.
        # api keys live forever
        # we don't need to re-validate, just return it
        return data
    else:
        raise AssertionError(f"invalid token type ({token_type})")

    return TokenData(**signer.loads(token, max_age=age))
