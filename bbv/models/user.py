# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import uuid
import random
from typing import Optional, List, Union
from datetime import datetime

from pydantic.dataclasses import dataclass
from quart import current_app as app
from ipaddress import IPv4Address, IPv6Address, IPv4Network, IPv6Network

from bbv.errors import FailedAuth
from bbv.auth import (
    hash_password,
    verify_password,
    TokenData,
    TokenType,
    TokenUserPayload,
    unsafe_parse_token,
    verify_token,
)
from itsdangerous import URLSafeTimedSerializer

IPV4_LOCAL_NETWORK = IPv4Network("172.16.0.0/12")
IPV6_LOCAL_NETWORK = IPv6Network("fd00:b00b::/32")


def _random_ip_addr(network: Union[IPv4Network, IPv6Network]):
    min_addr = network.network_address
    max_addr = network._address_class(
        int(network.network_address) | (int(network.netmask) ^ int(network._ALL_ONES))
    )

    rand_addr_int = random.randint(int(min_addr), int(max_addr))
    rand_addr = network._address_class(rand_addr_int)

    assert rand_addr in network
    return rand_addr


@dataclass
class UserOut:
    id: uuid.UUID
    created_at: datetime
    username: str
    email: str


@dataclass
class RegisterOut:
    id: uuid.UUID
    created_at: datetime
    username: str
    email: str
    token: str


@dataclass
class WireguardKey:
    user_id: uuid.UUID
    wireguard_public_key: str
    local_ipv4_address: IPv4Address
    local_ipv6_address: IPv6Address

    def to_dict(self):
        return {
            "user_id": str(self.user_id),
            "wireguard_public_key": self.wireguard_public_key,
            "local_ipv4_address": str(self.local_ipv4_address),
            "local_ipv6_address": str(self.local_ipv6_address),
        }


@dataclass
class User:
    id: uuid.UUID
    created_at: datetime
    username: str
    email: str
    password_hash: str

    @classmethod
    def from_row(_cls, row: Optional[dict]) -> Optional["User"]:
        return User(**row) if row is not None else None

    @classmethod
    async def fetch(_cls, user_id: uuid.UUID) -> Optional["User"]:
        row = await app.db.fetchrow(
            """
            SELECT
                id, created_at, username, email, password_hash
            FROM bbv_users
            WHERE id = $1
            """,
            user_id,
        )

        return User.from_row(row)

    @classmethod
    async def fetch_by(
        _cls,
        *,
        username: Optional[str] = None,
        email: Optional[str] = None,
    ) -> Optional["User"]:
        assert username or email

        row = await app.db.fetchrow(
            """
            SELECT
                id, created_at, username, email, password_hash
            FROM bbv_users
            WHERE username = $1 OR email = $1
            """,
            username or email,
        )

        return User.from_row(row)

    @classmethod
    async def create(_cls, *, username: str, email: str, password_hash: str) -> "User":
        row = await app.db.fetchrow(
            """
            INSERT INTO bbv_users
                (id, username, email, password_hash)
            VALUES
                ($1, $2, $3, $4)
            RETURNING
                id, created_at, username, email, password_hash
            """,
            uuid.uuid4(),
            username,
            email,
            password_hash,
        )

        assert row is not None
        return User(**row)

    def to_dict(self):
        return {
            "id": str(self.id),
            "created_at": self.created_at.isoformat(),
            "username": self.username,
            "email": self.email,
        }

    async def validate_password(self, given_password: str) -> None:
        """Validate a given password to check if it is the correct one.

        This WILL update the user's password hash if needed. The need for it
        comes from changes in PasswordHasher parameters.

        Raises FailedAuth on bad passwords.
        """

        valid = await verify_password(self.password_hash, given_password)

        if not valid:
            raise FailedAuth("User or password invalid")

        if app.password_hasher.check_needs_rehash(self.password_hash):
            await self.update_password(given_password)

    async def update_password(self, new_password: str):
        new_password_hash = await hash_password(new_password)

        await app.db.execute(
            """
            UPDATE bbv_users
            SET password_hash=$1
            WHERE id=$2
            """,
            new_password_hash,
            self.id,
        )

        self.password_hash = new_password_hash

    async def delete(self):
        await app.db.fetchrow(
            """
            DELETE FROM bbv_users
            WHERE id = $1
            """,
            self.id,
        )

    async def _create_token(self, token_payload: TokenData):
        signer = URLSafeTimedSerializer(self.password_hash)
        return signer.dumps(token_payload.to_dict())

    async def create_auth_token(self) -> str:
        return await self._create_token(
            TokenData(
                v=0,
                t=TokenType.User,
                d=TokenUserPayload(user_id=self.id),
            )
        )

    async def create_reset_password_token(self) -> str:
        return await self._create_token(
            TokenData(
                v=0,
                t=TokenType.ResetPassword,
                d=TokenUserPayload(user_id=self.id),
            )
        )

    async def create_delete_account_token(self) -> str:
        return await self._create_token(
            TokenData(
                v=0,
                t=TokenType.DeleteAccount,
                d=TokenUserPayload(user_id=self.id),
            )
        )

    async def fetch_wireguard_keys(self) -> List[WireguardKey]:
        return [
            WireguardKey(**r)
            for r in await app.db.fetch(
                """
                SELECT
                    user_id, wireguard_public_key,
                    local_ipv4_address, local_ipv6_address
                FROM bbv_user_wireguard_keys
                WHERE user_id = $1
                """,
                self.id,
            )
        ]

    @staticmethod
    async def generate_local_ip_pair():
        selected_v4, selected_v6 = None, None
        for _ in range(10):
            if selected_v4 is None:
                rand_v4 = _random_ip_addr(IPV4_LOCAL_NETWORK)
                if not await app.db.fetchval(
                    """
                    SELECT
                        EXISTS(
                            SELECT user_id FROM bbv_user_wireguard_keys
                            WHERE bbv_user_wireguard_keys.local_ipv4_address = $1
                        )
                        OR
                        EXISTS(
                            SELECT id FROM bbv_vpns WHERE
                            bbv_vpns.local_ipv4_address = $1
                        )
                    """,
                    rand_v4,
                ):
                    selected_v4 = rand_v4
            if selected_v6 is None:
                rand_v6 = _random_ip_addr(IPV6_LOCAL_NETWORK)
                if not await app.db.fetchval(
                    """
                    SELECT
                        EXISTS(
                            SELECT user_id FROM bbv_user_wireguard_keys
                            WHERE bbv_user_wireguard_keys.local_ipv6_address = $1
                        )
                        OR
                        EXISTS(
                            SELECT id FROM bbv_vpns WHERE
                            bbv_vpns.local_ipv6_address = $1
                        )
                    """,
                    rand_v6,
                ):
                    selected_v6 = rand_v6
            if selected_v4 and selected_v6:
                return selected_v4, selected_v6
        raise AssertionError(
            f"Failed to generate local IPv4 and IPv6 addresses ({selected_v4!r}, {selected_v6!r})"
        )

    async def add_wireguard_key(self, key: str) -> WireguardKey:
        ipv4_address, ipv6_address = await self.generate_local_ip_pair()
        await app.db.execute(
            """
            INSERT INTO bbv_user_wireguard_keys
                (user_id, wireguard_public_key,
                 local_ipv4_address, local_ipv6_address)
            VALUES ($1, $2, $3, $4)
            """,
            self.id,
            key,
            ipv4_address,
            ipv6_address,
        )
        return WireguardKey(
            user_id=self.id,
            wireguard_public_key=key,
            local_ipv4_address=ipv4_address,
            local_ipv6_address=ipv6_address,
        )

    async def delete_wireguard_key(self, key: str) -> None:
        row = await app.db.fetchrow(
            """
            DELETE FROM bbv_user_wireguard_keys
            WHERE user_id = $1 AND wireguard_public_key = $2
            RETURNING (
                user_id, wireguard_public_key,
                local_ipv4_address, local_ipv6_address
            )
            """,
            self.id,
            key,
        )
        return WireguardKey(*row["row"])

    @classmethod
    async def accept_token(cls, token: str, wanted_token_type: TokenType) -> "User":
        """Accept a token. Raises FailedAuth on invalid token."""
        try:
            token_unsafe_data = unsafe_parse_token(token)
        except UnicodeDecodeError:
            raise FailedAuth("Invalid token format")

        if token_unsafe_data.v != 0:
            raise FailedAuth("Invalid token format (invalid version)")

        if token_unsafe_data.t != wanted_token_type:
            raise FailedAuth("Invalid token format (invalid type)")

        assert token_unsafe_data.v == 0
        assert token_unsafe_data.t == wanted_token_type

        user = await cls.fetch(token_unsafe_data.d.user_id)
        if user is None:
            raise FailedAuth("Invalid token format (invalid user id)")

        token_data = await verify_token(user.password_hash, token)
        assert token_data.d.user_id == token_unsafe_data.d.user_id

        return user
