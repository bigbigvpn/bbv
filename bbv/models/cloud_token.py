# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only


from uuid import UUID
from typing import Optional, List

from pydantic.dataclasses import dataclass
from pydantic import Field

from quart import current_app as app
from bbv.cloud import CloudProviderType


@dataclass
class CloudTokenOut:
    provider: CloudProviderType = Field(description="The cloud provider")
    value: Optional[str] = Field(
        description="The value of the token in that cloud provider"
    )


@dataclass
class CloudToken:
    group_id: UUID
    provider: CloudProviderType
    value: Optional[str]

    @classmethod
    def from_row(cls, row) -> Optional["CloudToken"]:
        if row is None:
            return None

        return cls(
            group_id=row["group_id"],
            provider=CloudProviderType(row["provider"]),
            value=row["value"],
        )

    def to_out(self):
        return CloudTokenOut(provider=self.provider, value=self.value)

    @classmethod
    async def fetch(
        cls, group_id: UUID, provider: CloudProviderType
    ) -> Optional["CloudToken"]:
        row = await app.db.fetchrow(
            """
            SELECT group_id, provider, value
            FROM bbv_group_cloud_tokens
            WHERE group_id = $1 AND provider = $2
            """,
            group_id,
            provider.value,
        )

        return cls.from_row(row)

    @classmethod
    async def create(
        cls, group_id: UUID, *, provider: CloudProviderType, token: Optional[str] = None
    ) -> "CloudToken":

        row = await app.db.fetchrow(
            """
            INSERT INTO bbv_group_cloud_tokens
                (group_id, provider, value)
            VALUES
                ($1, $2, $3)
            RETURNING
                group_id, provider, value
            """,
            group_id,
            provider.value,
            token,
        )

        assert row is not None
        return cls.from_row(row)

    @classmethod
    async def from_group(cls, group_id: UUID) -> List["CloudToken"]:
        rows = await app.db.fetch(
            """
            SELECT
                group_id, provider, value
            FROM
                bbv_group_cloud_tokens
            WHERE
                group_id = $1
            """,
            group_id,
        )

        return [cls(**row) for row in rows]

    @staticmethod
    def list_to_dict(tokens: List["CloudToken"]) -> dict:
        all_tokens = {token.provider: token.value for token in tokens}
        return {
            "cloud_tokens": {
                provider.value: all_tokens.get(provider)
                for provider in CloudProviderType
            }
        }

    async def delete(self):
        await app.db.fetchrow(
            """
            DELETE FROM bbv_group_cloud_tokens
            WHERE group_id = $1 AND provider = $2
            """,
            self.group_id,
            self.provider.value,
        )
