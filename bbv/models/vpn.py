# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
from uuid import UUID
from datetime import datetime
from typing import Optional, List
from ipaddress import IPv4Network, IPv6Network, IPv4Address, IPv6Address
from enum import Enum

import asyncpg
from pydantic.dataclasses import dataclass
from quart import current_app as app

from bbv.cloud import CloudProviderType, get_cloud_provider_impl
from bbv.cloud.types import CloudVPN
from bbv.models import User, CloudToken
from bbv.errors import FailedAuth

log = logging.getLogger(__name__)


def optional(type_function, value):
    return None if value is None else type_function(value)


class VPNState(Enum):
    #: we either:
    #   - created a vpn right now
    #   - are booting up, and updated all vpns in SUMMONED to WAITING_CONNECTION
    #  on this state, wait for the vpn to connect with a set timeout, if it
    #  does not connect, vpn must be destroyed
    WAITING_CONNECTION = "waiting_connection"

    #: the vpn has connected to the control websocket
    SUMMONED = "summoned"


# everything except group_id and auth_token
@dataclass
class VPNOut:
    id: UUID
    cloud_provider: CloudProviderType
    cloud_region: str
    cloud_id: str
    created_at: datetime
    ipv4_network: str
    ipv6_network: Optional[str]
    hostname: str
    summoner_id: UUID
    state: VPNState
    local_ipv4_address: str
    local_ipv6_address: str
    wireguard_public_key: str
    wireguard_port: int


@dataclass
class VPN:
    id: UUID
    group_id: UUID
    cloud_provider: CloudProviderType
    cloud_region: str
    cloud_id: str
    created_at: datetime
    ipv4_network: IPv4Network
    ipv6_network: Optional[IPv6Network]
    hostname: str
    summoner_id: UUID
    auth_token: str
    state: VPNState
    local_ipv4_address: IPv4Address
    local_ipv6_address: IPv6Address

    wireguard_public_key: str
    wireguard_private_key: str
    wireguard_port: int

    def __repr__(self):
        return f"<VPN id={self.id!s} hostname={self.hostname!r}>"

    @classmethod
    def from_row(cls, row: Optional[dict]) -> Optional["VPN"]:
        return cls(**row) if row is not None else None

    @classmethod
    async def fetch(cls, vpn_id: UUID) -> Optional["VPN"]:
        row = await app.db.fetchrow(
            """
            SELECT
                id, group_id, cloud_provider, cloud_region, cloud_id,
                created_at, ipv4_network, ipv6_network, hostname,
                summoner_id, auth_token, state,
                local_ipv4_address, local_ipv6_address,
                wireguard_private_key, wireguard_public_key,
                wireguard_port
            FROM bbv_vpns
            WHERE id = $1
            """,
            vpn_id,
        )

        return cls.from_row(row)

    @classmethod
    async def fetch_all_in_state(cls, state: VPNState) -> List["VPN"]:
        rows = await app.db.fetch(
            """
            SELECT
                id, group_id, cloud_provider, cloud_region, cloud_id,
                created_at, ipv4_network, ipv6_network, hostname,
                summoner_id, auth_token, state,
                local_ipv4_address, local_ipv6_address,
                wireguard_private_key, wireguard_public_key,
                wireguard_port
            FROM bbv_vpns
            WHERE state = $1
            """,
            state.value,
        )

        return [cls.from_row(row) for row in rows]

    @classmethod
    def iterate_all(
        cls,
        conn: asyncpg.Connection,
    ) -> asyncpg.cursor.CursorFactory:
        return conn.cursor(
            """
            SELECT
                id, group_id, cloud_provider, cloud_region, cloud_id,
                created_at, ipv4_network, ipv6_network, hostname,
                summoner_id, auth_token, state,
                local_ipv4_address, local_ipv6_address,
                wireguard_private_key, wireguard_public_key,
                wireguard_port
            FROM bbv_vpns
            """
        )

    @classmethod
    async def fetch_by_region(cls, group_id: str, cloud_region: str):
        row = await app.db.fetchrow(
            """
            SELECT
                id, group_id, cloud_provider, cloud_region, cloud_id,
                created_at, ipv4_network, ipv6_network, hostname,
                summoner_id, auth_token, state,
                local_ipv4_address, local_ipv6_address,
                wireguard_private_key, wireguard_public_key,
                wireguard_port
            FROM bbv_vpns
            WHERE group_id = $1 AND cloud_region = $2
            """,
            group_id,
            cloud_region,
        )

        return cls.from_row(row)

    @classmethod
    async def create(
        cls,
        vpn_id: UUID,
        group_id: UUID,
        cloud_provider: CloudProviderType,
        cloud_region: str,
        summoned_vpn_info: CloudVPN,
        hostname: str,
        summoner_id: UUID,
        vpn_token: str,
        wireguard_private_key: str,
        wireguard_public_key: str,
        wireguard_port: int,
    ) -> "VPN":
        # TODO: this is a very bad hack to coerce a given offset-aware datetime
        # back into unaware (which would be equivalent to UTC+0).
        #
        # we should use pytz.
        #
        # Hetzner returns an offset-aware datetime of UTC+0, so we just strip
        # that information off by doing the strftime + utcfromtimestamp
        # method.
        original_created_at = summoned_vpn_info.created_at
        original_created_at_as_timestamp = float(original_created_at.strftime("%s"))
        created_at = datetime.utcfromtimestamp(original_created_at_as_timestamp)

        # TODO move those vars to parameters as well
        # ... and also maybe move all of those params into a dataclass
        # for ease of use?
        v4_addr, v6_addr = await User.generate_local_ip_pair()

        row = await app.db.fetchrow(
            """
            INSERT INTO bbv_vpns
                (id, group_id, cloud_provider, cloud_region, cloud_id,
                 created_at, ipv4_network, ipv6_network, hostname,
                 summoner_id, auth_token, state, local_ipv4_address,
                 local_ipv6_address, wireguard_private_key, wireguard_public_key,
                 wireguard_port)
            VALUES
                (
                $1, $2, $3, $4, $5,
                $6, $7, $8, $9,
                $10, $11, $12, $13,
                $14, $15, $16, $17
                )
            RETURNING
                id, group_id, cloud_provider, cloud_region, cloud_id,
                created_at, ipv4_network, ipv6_network, hostname,
                summoner_id, auth_token, state, local_ipv4_address,
                local_ipv6_address, wireguard_private_key, wireguard_public_key,
                wireguard_port
            """,
            vpn_id,
            group_id,
            cloud_provider.value,
            cloud_region,
            summoned_vpn_info.server_cloud_id,
            created_at,
            summoned_vpn_info.ipv4_network,
            summoned_vpn_info.ipv6_network,
            hostname,
            summoner_id,
            vpn_token,
            VPNState.WAITING_CONNECTION.value,
            v4_addr,
            v6_addr,
            wireguard_private_key,
            wireguard_public_key,
            wireguard_port,
        )

        assert row is not None
        return cls(**row)

    def to_dict(self):
        return {
            "id": str(self.id),
            "cloud_provider": self.cloud_provider.value,
            "cloud_region": self.cloud_region,
            "cloud_id": self.cloud_id,
            "created_at": self.created_at.isoformat(),
            "ipv4_network": str(self.ipv4_network),
            "ipv6_network": optional(str, self.ipv6_network),
            "hostname": self.hostname,
            "summoner_id": str(self.summoner_id),
            "state": self.state.value,
            "local_ipv4_address": str(self.local_ipv4_address),
            "local_ipv6_address": str(self.local_ipv6_address),
            "wireguard_public_key": self.wireguard_public_key,
            "wireguard_port": self.wireguard_port,
        }

    async def update_state(self, new_state: VPNState):
        log.info(
            "VPN %s %s updating state %s -> %s",
            self.id,
            self.hostname,
            self.state.value,
            new_state.value,
        )
        await app.db.execute(
            """
            UPDATE bbv_vpns
            SET state = $1
            WHERE id = $2
            """,
            new_state.value,
            self.id,
        )

        self.state = new_state

    async def delete(self):
        token = await CloudToken.fetch(self.group_id, self.cloud_provider)
        assert token is not None
        provider_impl = get_cloud_provider_impl(self.cloud_provider)
        await provider_impl.release(token.value, self.cloud_region, self.cloud_id)
        await app.db.fetchrow(
            """
            DELETE FROM bbv_vpns
            WHERE id = $1
            """,
            self.id,
        )

        await app.running_vpns.on_released_vpn(self.id)

    @classmethod
    async def fetch_many_by_group(cls, group_id: UUID) -> List["VPN"]:
        rows = await app.db.fetch(
            """
            SELECT
                id, group_id, cloud_provider, cloud_region, cloud_id,
                created_at, ipv4_network, ipv6_network, hostname,
                summoner_id, auth_token, state, local_ipv4_address,
                local_ipv6_address,
                wireguard_private_key, wireguard_public_key,
                wireguard_port
            FROM
                bbv_vpns
            WHERE
                bbv_vpns.group_id = $1
            """,
            group_id,
        )

        return [cls(**row) for row in rows]

    @classmethod
    async def authenticate(cls, vpn_id: UUID, given_token: str) -> "VPN":
        """Authenticate a VPN"""
        try:
            vpn = await cls.fetch(vpn_id)
        except ValueError:
            raise FailedAuth("Invalid VPN credentials provided")

        if vpn is None:
            raise FailedAuth("Invalid VPN credentials provided")

        if vpn.auth_token != given_token:
            raise FailedAuth("Invalid VPN credentials provided")

        return vpn
