# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from uuid import UUID, uuid4
from datetime import datetime
from typing import Optional, List

from bbv.models import VPN, User
from bbv.models.user import WireguardKey
from pydantic.dataclasses import dataclass
from quart import current_app as app
from bbv.enums.messages import CommandType, RunningVPNMessageType


@dataclass
class Group:
    id: UUID
    created_at: datetime
    superowner_id: UUID
    name: str

    @classmethod
    def from_row(cls, row: Optional[dict]) -> Optional["Group"]:
        return cls(**row) if row is not None else None

    @classmethod
    async def fetch(cls, group_id: UUID) -> Optional["Group"]:
        row = await app.db.fetchrow(
            """
            SELECT
                id, created_at, superowner_id, name
            FROM bbv_groups
            WHERE id = $1
            """,
            group_id,
        )

        return cls.from_row(row)

    @classmethod
    async def create(cls, name: str, superowner_id: UUID) -> "Group":
        row = await app.db.fetchrow(
            """
            INSERT INTO bbv_groups
                (id, name, superowner_id)
            VALUES
                ($1, $2, $3)
            RETURNING
                id, created_at, name, superowner_id
            """,
            uuid4(),
            name,
            superowner_id,
        )

        # add superowner as member so that methods like fetch_groups
        # work without needing expensive joins
        await app.db.execute(
            """
            INSERT INTO bbv_group_members
                (group_id, user_id)
            VALUES
                ($1, $2)
            """,
            row["id"],
            superowner_id,
        )

        assert row is not None
        return cls(**row)

    def to_dict(self):
        return {
            "id": str(self.id),
            "created_at": self.created_at.isoformat(),
            "name": self.name,
            "superowner_id": str(self.superowner_id),
        }

    async def update(self, *, name: str):
        await app.db.execute(
            """
            UPDATE bbv_groups
            SET name=$1
            WHERE id=$2
            """,
            name,
            self.id,
        )

        self.name = name

    async def delete(self):
        vpns = await VPN.fetch_many_by_group(self.id)
        for vpn in vpns:
            await vpn.delete()
        await app.db.fetchrow(
            """
            DELETE FROM bbv_groups
            WHERE id = $1
            """,
            self.id,
        )

    @classmethod
    async def fetch_groups(cls, user_id: UUID) -> List["Group"]:
        # TODO: maybe add bbv_group_members.permisisons here
        #  thinking about some newfangled frontend that would show
        #  if you're an owner of the group or not, etc.
        rows = await app.db.fetch(
            """
            SELECT
                bbv_groups.id, bbv_groups.created_at,
                bbv_groups.name, bbv_groups.superowner_id
            FROM
                bbv_group_members
            JOIN bbv_groups
              ON bbv_groups.id = bbv_group_members.group_id
            WHERE
                bbv_group_members.user_id = $1
            """,
            user_id,
        )

        return [cls(**row) for row in rows]

    async def _dispatch_command_to_running_vpns(self, data):
        vpns = await VPN.fetch_many_by_group(self.id)
        vpn_ids = [v.id for v in vpns]
        await app.running_vpns.dispatch(vpn_ids, RunningVPNMessageType.COMMAND, data)

    async def dispatch_add_keys(self, keys):
        await self._dispatch_command_to_running_vpns(
            (
                CommandType.ADD_WIREGUARD_KEYS,
                {"keys": [k.to_dict() for k in keys]},
            ),
        )

    async def dispatch_remove_keys(self, keys):
        await self._dispatch_command_to_running_vpns(
            (
                CommandType.REMOVE_WIREGUARD_KEYS,
                {"keys": [k.to_dict() for k in keys]},
            ),
        )

    async def add_member(self, user_id: UUID) -> None:
        await app.db.fetchrow(
            """
            INSERT INTO bbv_group_members
                (user_id, group_id)
            VALUES
                ($1, $2)
            """,
            user_id,
            self.id,
        )

        user = await User.fetch(user_id)
        assert user is not None
        keys = await user.fetch_wireguard_keys()
        await self.dispatch_add_keys(keys)

    async def fetch_all_wireguard_keys(self) -> List[WireguardKey]:
        return [
            WireguardKey(**r)
            for r in await app.db.fetch(
                """
            SELECT
                bbv_user_wireguard_keys.user_id, wireguard_public_key,
                local_ipv4_address, local_ipv6_address
            FROM bbv_user_wireguard_keys
            JOIN
                bbv_group_members
                ON bbv_group_members.user_id = bbv_user_wireguard_keys.user_id
            WHERE
                bbv_group_members.group_id = $1
            """,
                self.id,
            )
        ]
