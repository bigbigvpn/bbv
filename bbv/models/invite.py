# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only


import random
import string

from uuid import UUID
from datetime import datetime
from typing import Optional, List

from pydantic.dataclasses import dataclass
from quart import current_app as app


ALPHABET = string.ascii_lowercase + string.digits


# Those are not API errors on purpose.
class InviteTooMuchUses(Exception):
    pass


class InviteExpired(Exception):
    pass


@dataclass
class Invite:
    code: str
    group_id: UUID
    uses: int
    max_uses: Optional[int]
    expires_at: Optional[datetime]

    @classmethod
    def from_row(cls, row: Optional[dict]) -> Optional["Invite"]:
        return cls(**row) if row is not None else None

    @classmethod
    async def fetch(cls, code: str) -> Optional["Invite"]:
        row = await app.db.fetchrow(
            """
            SELECT
                code, group_id, uses, max_uses, expires_at
            FROM bbv_group_invites
            WHERE code = $1
            """,
            code,
        )

        return cls.from_row(row)

    @classmethod
    async def create(
        cls,
        group_id: UUID,
        *,
        max_uses: Optional[int] = None,
        expires_at: Optional[datetime] = None
    ) -> "Invite":
        # TODO: in the future, consider some human-readable invite codes
        # maybe using some english dictionary wordgen.
        code = "".join([random.choice(ALPHABET) for _ in range(25)])

        row = await app.db.fetchrow(
            """
            INSERT INTO bbv_group_invites
                (code, group_id, max_uses, expires_at)
            VALUES
                ($1, $2, $3, $4)
            RETURNING
                code, group_id, uses, max_uses, expires_at
            """,
            code,
            group_id,
            max_uses,
            expires_at,
        )

        assert row is not None
        return cls(**row)

    def to_dict(self):
        return {
            "code": self.code,
            "group_id": str(self.group_id),
            "uses": self.uses,
            "max_uses": self.max_uses,
            "expires_at": self.expires_at.isoformat()
            if self.expires_at is not None
            else None,
        }

    @classmethod
    async def fetch_groups(cls, group_id: UUID) -> List["Invite"]:
        rows = await app.db.fetch(
            """
            SELECT
                bbv_groups.code, bbv_groups.group_id,
                bbv_groups.uses, bbv_groups.max_uses,
                bbv_groups.expires_at
            FROM
                bbv_group_invites
            JOIN bbv_groups
              ON bbv_groups.id = bbv_group_invites.group_id
            WHERE
                bbv_group_invites.group_id = $1
            """,
            group_id,
        )

        return [cls(**row) for row in rows]

    async def validate_itself(self):
        if self.max_uses and self.uses > self.max_uses:
            raise InviteTooMuchUses()

        now = datetime.utcnow()
        if self.expires_at and now > self.expires_at:
            raise InviteExpired()

    async def use(self):
        self.uses = await app.db.fetchval(
            """
            UPDATE bbv_group_invites
            SET uses = uses + 1
            WHERE code = $1
            RETURNING uses
            """,
            self.code,
        )

    @classmethod
    async def fetch_invites(cls, group_id: UUID) -> List["Invite"]:
        rows = await app.db.fetch(
            """
            SELECT
                code, group_id, uses, max_uses, expires_at
            FROM
                bbv_group_invites
            WHERE
                group_id = $1
            """,
            group_id,
        )

        return [cls(**row) for row in rows]

    async def delete(self):
        await app.db.fetchrow(
            """
            DELETE FROM bbv_group_invites
            WHERE code = $1
            """,
            self.code,
        )
