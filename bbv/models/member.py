# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from enum import Enum
from uuid import UUID
from typing import List, Optional
from datetime import datetime

from quart import current_app as app
from pydantic.dataclasses import dataclass
from bbv.models import Group, User


class Permissions(Enum):
    OWNER = "owner"
    INVITE = "invite"
    CLOUD_TOKENS = "cloud_tokens"


class MemberHasNoPermission(Exception):
    pass


# Members are used in the Member API and so
# it's basically UserOut without email.
@dataclass
class MemberUser:
    id: UUID
    created_at: datetime
    username: str


@dataclass
class MemberOut:
    user: MemberUser
    permissions: List[str]


@dataclass
class Member:
    group: Group
    user: MemberUser
    permissions: List[str]

    @classmethod
    async def fetch(cls, group_id: UUID, user_id: UUID) -> Optional["Member"]:
        group = await Group.fetch(group_id)
        assert group is not None

        row = await app.db.fetchrow(
            """
            SELECT
                bbv_users.id, bbv_users.created_at,
                bbv_users.username, bbv_group_members.permissions
            FROM
                bbv_group_members
            JOIN bbv_users
              ON bbv_users.id = bbv_group_members.user_id
            WHERE
                bbv_group_members.group_id = $1
            AND bbv_group_members.user_id = $2
            """,
            group_id,
            user_id,
        )

        if row is None:
            return None

        member_user = MemberUser(
            id=row["id"], created_at=row["created_at"], username=row["username"]
        )
        # TODO permissions datatype?
        return cls(group=group, user=member_user, permissions=row["permissions"])

    async def set_permissions(self, new_permissions: List[str]):
        await app.db.execute(
            """
            UPDATE bbv_group_members
            SET permissions = $3
            WHERE user_id = $1 AND group_id = $2
            """,
            self.user.id,
            self.group.id,
            new_permissions,
        )
        self.permissions = new_permissions

    def to_dict(self):
        return {
            "user": {
                "id": str(self.user.id),
                "created_at": self.user.created_at.isoformat(),
                "username": self.user.username,
            },
            "permissions": self.permissions,
        }

    async def delete(self):
        await app.db.execute(
            """
            DELETE FROM bbv_group_members
            WHERE group_id = $1 AND user_id = $2
            """,
            self.group.id,
            self.user.id,
        )

        user = await User.fetch(self.user.id)
        assert user is not None
        keys = await user.fetch_wireguard_keys()
        await self.group.dispatch_remove_keys(keys)

    @classmethod
    async def from_group(_cls, group_id: UUID) -> List["Member"]:
        # This can be changed to a join inside the main query and it would
        # remove the need for this separate fetch().
        group = await Group.fetch(group_id)
        assert group is not None

        rows = await app.db.fetch(
            """
            SELECT
                bbv_users.id, bbv_users.created_at,
                bbv_users.username, bbv_group_members.permissions
            FROM
                bbv_group_members
            JOIN bbv_users
              ON bbv_users.id = bbv_group_members.user_id
            WHERE
                bbv_group_members.group_id = $1
            """,
            group_id,
        )

        result = []
        for row in rows:
            member_user = MemberUser(
                id=row["id"], created_at=row["created_at"], username=row["username"]
            )
            # TODO proper permissions datatype
            result.append(
                Member(
                    group=group,
                    user=member_user,
                    permissions=row["permissions"],
                )
            )

        return result

    async def has_permission(self, permission: Permissions) -> bool:
        if self.user.id == self.group.superowner_id:
            return

        if Permissions.OWNER.value in self.permissions:
            return

        if permission.value not in self.permissions:
            raise MemberHasNoPermission()
