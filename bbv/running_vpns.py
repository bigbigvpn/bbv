# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import asyncio
from typing import Any, Optional, List, Dict
from uuid import UUID
from dataclasses import dataclass

from quart import current_app as app

from bbv.models import VPN
from bbv.models.vpn import VPNState
from bbv.enums.messages import RunningVPNMessageType

log = logging.getLogger(__name__)


async def _wait_for_connection(vpn_id: UUID):
    """Wait a set time (currently 10 minutes) and destroy the VPN.

    This function is usually wrapped in an asyncio task, so that when the VPN
    connects, the task is cancelled, and our waiting stops.
    """
    await asyncio.sleep(10 * 60)
    vpn = await VPN.fetch(vpn_id)
    if vpn is None:
        return
    log.info("vpn %r took too long to connect, deleting", vpn_id)
    await vpn.delete()


@dataclass
class RunningVPNState:
    """Represents the state of a vpn.

    it may be disconnected and have a wait_task representing the asyncio task
    waiting for a connection until its destroyed,

    or it may be connected and have an asyncio queue others can dispatch
    events to!

    a vpn registered in this subsystem can only be in one of the two states.

    the setter methods in this function are the preffered way to mutate
    the state of this object. the methods assert that an invalid state is not
    being reached.
    """

    wait_task: Optional[asyncio.Task]
    queue: Optional[asyncio.Queue]

    def set_wait_task(self, task):
        assert self.queue is None
        self.wait_task = task

    def set_queue(self, queue):
        assert self.wait_task is None
        self.queue = queue

    def maybe_destroy_task(self):
        if self.wait_task is not None:
            self.wait_task.cancel()

    def maybe_destroy_queue(self):
        if self.queue is not None:
            # drain the queue so that we keep garbage collection happy
            count = 0
            while not self.queue.empty():
                self.queue.get_nowait()
                count += 1
            log.info("there were %d messages in queue", count)


def wait_connection_task_name(vpn_id):
    return f"wait_connection:{vpn_id}"


class RunningVPNManager:
    """holds the real-time state information about a vpn.

    the column bbv_vpns.state handles the persistence of state in a vpn,
    which is important, but the database shall not worry about the
    side effects of that state in the system.

    this class takes care of that.

    side-effects of a VPNState:
     - WAITING_CONNECTION means that we have an asyncio.Task waiting for connection
     - SUMMONED means that we have an asyncio.Queue ready to take messages

    methods prefixed with "on_" mean events in the lifecycle of bbv,
    called by specific pieces of code in blueprint code.
    """

    def __init__(self):
        self.vpns: Dict[UUID, RunningVPNState] = {}

    async def on_app_start(self):
        """Called on webapp startup.

        Fetches all VPNs and sets their state to WAITING_CONNECTION.

        Since we're starting up, we can assume that the old process of bbv
        was fully killed and all the currently existing vpns lost their
        connections.

        The happy path is that everyone reconnects.
        """

        log.info("event: on_app_start")

        count = 0

        async with app.db.acquire() as conn:
            async with conn.transaction():
                async for vpn_row in VPN.iterate_all(conn):
                    vpn = VPN.from_row(vpn_row)
                    await self.on_incoming_vpn(vpn)
                    count += 1

        log.info("on_app_start: waiting for %d vpns", count)

    async def on_incoming_vpn(self, vpn: VPN):
        """Called when a new vpn comes to the system, either because it was
        just created, or because it already exists and we're preloading it
        from on_app_start().
        """
        log.info("event: on_new_vpn %s", vpn.id)

        await vpn.update_state(VPNState.WAITING_CONNECTION)
        wait_task = app.sched.spawn(
            _wait_for_connection,
            [vpn.id],
            name=wait_connection_task_name(vpn.id),
        )

        # the given vpn MUST be a new one. if we already heard of it we must
        # not continue, as that is an invalid state.
        assert vpn.id not in self.vpns
        self.vpns[vpn.id] = RunningVPNState(wait_task, None)

    async def on_connected_vpn(self, vpn: VPN):
        """Called when a vpn has successfully connected to bbv."""
        log.info("event: on_connected_vpn %s", vpn.id)

        # the vpn must already exist (as it came from on_incoming_vpn)
        #
        # note: if its reconnecting, it would have gone through on_incoming_vpn
        # regardless.
        assert vpn.id in self.vpns

        await vpn.update_state(VPNState.SUMMONED)

        # transitioning from "waiting" to "ready for messages"
        #
        # which means destroying the waiting task, and setting up the queue
        app.sched.stop(wait_connection_task_name(vpn.id))
        self.vpns[vpn.id].wait_task = None
        self.vpns[vpn.id].set_queue(asyncio.Queue())

    async def on_disconnected_vpn(self, vpn: VPN):
        log.info("event: on_disconnected_vpn %s", vpn.id)

        # we need to destroy the given queue for the vpn, and setup the
        # "waiting for connection" task
        #
        # one valid worry about dropping the state is that we also destroy
        # the queue and it might have messages in it
        #
        # for now, we don't care about that because the messages are mostly
        # about key state updates. when a vpn connects, a full resync
        # is done in the connection.
        try:
            state = self.vpns.pop(vpn.id)
        except KeyError:
            # if no state is found (e.g when reconnecting, where we generate
            # two disconnection events), just ignore it
            return
        state.maybe_destroy_task()
        await self.on_incoming_vpn(vpn)

    async def on_released_vpn(self, vpn_id: UUID):
        """Called when a VPN is deleted from the database.

        This can be called when:
         - A VPN took too long to connect.
         - Manual action (VPN manual release, Group deletion).

        In either of those cases, the VPN can either be on
        WAITING_CONNECTION or SUMMONED. We account for both cases when releasing
        resources.

        """
        log.info("event: on_deleted_vpn %s", vpn_id)
        state = self.vpns.pop(vpn_id)

        # on_released_vpn is called via VPN.delete which can be called when:
        #  - it took too long to connect
        #  - someone manually did it (either singular, or via a group being deleted)
        #
        # we ensure

        # ensure the task waiting on a possible connection does not
        # become a zombie
        state.maybe_destroy_task()
        state.maybe_destroy_queue()

    async def dispatch(
        self,
        vpn_ids: List[UUID],
        dispatch_type: RunningVPNMessageType,
        data: Any,
    ) -> None:
        """Dispatch an event to a list of vpn ids.

        Only actually dispatches to VPNs in the SUMMONED state.
        """
        for vpn_id in vpn_ids:
            running_state = self.vpns[vpn_id]
            if running_state.queue is not None:
                await running_state.queue.put((dispatch_type, data))
