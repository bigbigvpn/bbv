# bbv: Server component for bigbigvpn
# Copyright 2021, Team bigbigvpn and bbv contributors
# SPDX-License-Identifier: AGPL-3.0-only

from enum import Enum


class RunningVPNMessageType(Enum):
    COMMAND = 0


class CommandType(Enum):
    SYNC_WIREGUARD_KEYS = 0
    ADD_WIREGUARD_KEYS = 1
    REMOVE_WIREGUARD_KEYS = 2
    SHUTDOWN = 3


class NotifyType(Enum):
    NO_CLIENTS = 0


class OperationType(Enum):
    LOGIN = 1
    WELCOME = 2
    HEARTBEAT = 3
    HEARTBEAT_ACK = 4
    COMMAND = 5
    NOTIFY = 6


class CloseCodes:
    ERROR = 4000
    FAILED_AUTH = 4001
    HEARTBEAT_EXPIRE = 4002
    INVALID_JSON = 4003
    INVALID_MESSAGE = 4004
